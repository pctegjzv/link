/**
 * Created by liuwei on 2018/3/15.
 */

import { SecretPage } from '../../page_objects/config/secret.page';
import { CommonKubectl } from '../../utility/common.kubectl';
import { CommonMethod } from '../../utility/common.method';

describe('L1:配置字典更新功能', () => {
  const secretPage = new SecretPage();
  const namespace = CommonMethod.random_generate_testData();
  const label = CommonMethod.random_generate_testData();

  const label_newkey = CommonMethod.random_generate_testData();
  const label_newvalue = CommonMethod.random_generate_testData();
  const annotations_newkey = CommonMethod.random_generate_testData();
  const annotations_newvalue = CommonMethod.random_generate_testData();

  const name = CommonMethod.random_generate_testData();
  // 根据【名称，命名空间， 类型】唯一确定资源列表的一行
  const rowkey = [name];

  beforeAll(() => {
    secretPage.navigateTo();
    this.testdata = CommonKubectl.createResource(
      'secret.yaml',
      {
        '${NAME}': name,
        '${NAMESPACE}': namespace,
        '${LABEL}': label,
      },
      'L1.secretUpdate.e2e-spec',
    );
  });

  afterAll(() => {
    // 删除测试数据
    CommonKubectl.deleteResourceByYmal(this.testdata);
  });

  it('L1:AldK8S-134: 单击左导航配置／保密字典 -- 进入保密列表页--选择一个保密字典，单击操作列，选择更新标签--验证更新标签功能正确', () => {
    secretPage.resourceTable.clickOperationButtonByRow(rowkey, '更新标签');
    secretPage.resourcelabel.newValue(label_newkey, label_newvalue);
    secretPage.resourcelabel.clickUpdate();
    expect(secretPage.toastsuccess.getText()).toBe('更新成功');
    expect(secretPage.resourceTable.getCellLabel(rowkey, '名称')).toContain(
      `${label_newkey}: ${label_newvalue}`,
    );
  });

  it('L1:AldK8S-135: 单击左导航配置／保密字典 -- 进入保密字典列表页--选择一个保密字典，单击操作列，选择更新注解--验证更新注解功能正确', () => {
    secretPage.resourceTable.clickOperationButtonByRow(rowkey, '更新注解');
    secretPage.resourceAnnotations.newValue(
      annotations_newkey,
      annotations_newvalue,
    );
    secretPage.resourceAnnotations.clickUpdate();
    expect(secretPage.toastsuccess.getText()).toBe('更新成功');
  });

  it('L1:AldK8S-136: 单击左导航配置／保密字典 -- 进入保密字典列表页--选择一个保密字典，单击操作列，选择更新--验证更新功能正确', () => {
    secretPage.resourceTable.clickResourceNameByRow(rowkey);
    // TODO:
  });

  it('L1:AldK8S-137: 单击左导航配置／保密字典 -- 进入保密字典列表页--选择一个保密字典，单击操作列，选择删除--验证删除功能正确', () => {
    secretPage.operation.select('删除');
    expect(secretPage.confirmDialog.confirmTitle.getText()).toBe(
      `确定删除 保密字典 ${name} 吗?`,
    );
    secretPage.confirmDialog.clickConfirm();
    expect(secretPage.toastsuccess.getText()).toBe('删除成功');
    secretPage.searchBox.search(name);
    expect(secretPage.resourceTable.noResult.getText()).toBe('无保密字典');
  });
});
