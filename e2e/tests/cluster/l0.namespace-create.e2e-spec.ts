import { NamespacePage } from '../../page_objects/cluster/namespace.page';
import { CommonKubectl } from '../../utility/common.kubectl';
import { CommonMethod } from '../../utility/common.method';

describe('L0:命名空间创建功能', () => {
  const namespacePage = new NamespacePage();
  const label = CommonMethod.random_generate_testData();
  const name = CommonMethod.random_generate_testData();
  // 根据【名称，命名空间， 类型】唯一确定资源列表的一行
  const rowkey = [name];

  beforeAll(() => {
    namespacePage.navigateTo();
    this.testdata = CommonMethod.generateStringYaml(
      'namespace.yaml',
      {
        '${NAME}': name,
        '${LABEL}': label,
      },
      'l0.namespace-create.e2e-spec',
    );
  });

  afterAll(() => {
    // 清除测试数据
    CommonKubectl.execKubectlCommand(`kubectl delete namespaces ${name}`);
  });
  it('AldK8S-208:L0: 单击集群 / 命名空间--进入命名空间列表--点击创建命名空间--填写yaml--点击创建--验证命名空间创建成功', () => {
    namespacePage.getButtonByText('创建命名空间').click();
    namespacePage.addnamespace(name);
    namespacePage.okelement.click();
    // 验证namespace 创建成功
    namespacePage.searchBox.search(name);
    expect(namespacePage.resourceTable.getRowCount()).toBe(1);
  });
});
