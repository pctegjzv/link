/**
 * Created by liuwei on 2018/3/15.
 */
import { browser, by, element } from 'protractor';

import { AddVolumePage } from '../../page_objects/computer/addvolumes.page';
import { DeploymentPage } from '../../page_objects/computer/deployment.page';
import { CommonApi } from '../../utility/common.api';
import { CommonKubectl } from '../../utility/common.kubectl';
import { CommonMethod } from '../../utility/common.method';
import { CommonPage } from '../../utility/common.page';
import { k8s_type_deployments } from '../../utility/resource.type.k8s';

describe('L1:部署更新功能的验证', () => {
  const deploymentPage = new DeploymentPage();
  const addVolumePage = new AddVolumePage();
  const namespace = CommonMethod.random_generate_testData();
  const label = CommonMethod.random_generate_testData();
  const label_newkey = CommonMethod.random_generate_testData();
  const label_newvalue = CommonMethod.random_generate_testData();
  const annotations_newkey = CommonMethod.random_generate_testData();
  const annotations_newvalue = CommonMethod.random_generate_testData();
  const name = CommonMethod.random_generate_testData();
  const configmap = name;

  // 根据【名称，类型】唯一确定资源列表的一行
  const rowkey = [name];

  beforeAll(() => {
    deploymentPage.navigateTo();

    // 创建一个Deployment
    this.testdata = CommonKubectl.createResource(
      'deployment.yaml',
      {
        '${NAME}': name,
        '${NAMESPACE}': namespace,
        '${LABEL}': label,
      },
      'l1.deployment-detail-info.e2e-spec',
    );
    // 选择新创建的namespace
  });

  afterAll(() => {
    // 删除测试数据
    CommonKubectl.deleteResourceByYmal(this.testdata);
  });

  it('L1:AldK8S-43: 单击左导航计算／部署 -- 进入Deployment 列表页--单击一个Deployment名称--进入详情页--验证显示信息正确', () => {
    deploymentPage.clickLeftNavByText('部署');
    deploymentPage.resourceTable.getCell('状态', rowkey).then(cell => {
      CommonPage.waitElementTextChangeTo(cell, '1 / 1');
      cell.getText().then(text => {
        console.log(text);
      });
    });
    deploymentPage.resourceTable.clickResourceNameByRow(rowkey);
    // 验证详情页基本信息部分字段值是否正确
    const resourceJson = CommonApi.getResourceJSON(
      name,
      k8s_type_deployments,
      namespace,
    );

    // 验证详情页基本信息页
    expect(
      deploymentPage.resourceBasicInfo.basicInfo
        .getElementByText('名称')
        .getText(),
    ).toBe(resourceJson.metadata.name);

    expect(
      deploymentPage.resourceBasicInfo.basicInfo
        .getElementByText('命名空间')
        .getText(),
    ).toBe(resourceJson.metadata.namespace);

    expect(
      deploymentPage.resourceBasicInfo.basicInfo
        .getElementByText('标签')
        .getText(),
    ).toContain(resourceJson.metadata.labels.deploy);

    expect(
      deploymentPage.resourceBasicInfo.basicInfo
        .getElementByText('注解')
        .getText(),
    ).toContain(Object.keys(resourceJson.metadata.annotations)[0]);

    expect(
      deploymentPage.resourceBasicInfo.basicInfo
        .getElementByText('选择器')
        .getText(),
    ).toContain(
      resourceJson.spec.selector.matchLabels[
        Object.keys(resourceJson.spec.selector.matchLabels)[0]
      ],
    );

    expect(
      deploymentPage.resourceBasicInfo.basicInfo
        .getElementByText('更新策略')
        .getText(),
    ).toContain(resourceJson.spec.strategy.type);

    expect(
      deploymentPage.resourceBasicInfo.basicInfo
        .getElementByText('滚动更新策略')
        .getText(),
    ).toContain(resourceJson.spec.strategy.rollingUpdate.maxSurge);

    expect(
      deploymentPage.resourceBasicInfo.basicInfo
        .getElementByText('最小准备时间')
        .getText(),
    ).toBe('0');
    expect(
      deploymentPage.resourceBasicInfo.basicInfo
        .getElementByText('历史版本限制')
        .getText(),
    ).toBe('2');

    expect(deploymentPage.detailPage.podList.getRowCount()).toBe(1);
    expect(
      deploymentPage.detailPage.podList.getRow(rowkey).getText(),
    ).toContain(resourceJson.metadata.name);
  });

  it('L1:AldK8S-51: 单击左导航计算／部署 -- 进入Deployment 列表页--单击一个Deployment名称--进入详情页--单击日志tab--验证日志显示正确', () => {
    deploymentPage.clickLeftNavByText('部署');
    deploymentPage.namespace.select(namespace);

    deploymentPage.resourceTable.getCell('状态', rowkey).then(elem => {
      return CommonPage.waitElementTextChangeTo(elem, '1 / 1');
    });
    deploymentPage.resourceTable.clickResourceNameByRow(rowkey);
    deploymentPage.resourceBasicInfo.resourceInfoTab.clickTabByText('日志');
    deploymentPage.resourceBasicInfo.logviewer.getToolbarButton('查找').click();
    deploymentPage.resourceBasicInfo.logviewer.waitLogDisplay();
    deploymentPage.resourceBasicInfo.logviewer.finder_inputbox.input('hehe');
    deploymentPage.resourceBasicInfo.logviewer.resultfinder
      .getText()
      .then(result => {
        expect(result.includes('无结果')).toBeFalsy();
      });
  });

  it('L1:AldK8S-44: 单击左导航计算／部署 -- 进入Deployment 列表页--单击一个Deployment名称--进入详情页--单击操作--选择更新标签--验证更新标签成功', () => {
    deploymentPage.clickLeftNavByText('部署');
    // 定位到deployment 所在行，单击操作按钮，选择【更新标签】
    deploymentPage.resourceTable.clickOperationButtonByRow(rowkey, '更新标签');

    deploymentPage.resourcelabel.newValue(label_newkey, label_newvalue);
    deploymentPage.resourcelabel.clickUpdate();
    deploymentPage.resourceTable.clickResourceNameByRow(rowkey);
    deploymentPage.resourceBasicInfo.resourceInfoTab.clickTabByText('基本信息');
    // 验证标签修改正确
    deploymentPage.resourceBasicInfo.basicInfo
      .getElementByText('标签')
      .getText()
      .then(function(labelText) {
        expect(labelText).toContain(label_newvalue);
      });

    // 验证详情页基本信息页
    expect(
      deploymentPage.resourceBasicInfo.basicInfo
        .getElementByText('名称')
        .getText(),
    ).toBe(name);
  });

  it('L1:AldK8S-45: 单击左导航计算／部署 -- 进入Deployment 列表页--单击一个Deployment名称--进入详情页--单击操作--选择更新注解--验证更新注解成功', () => {
    deploymentPage.operation.select('更新注解');
    CommonPage.waitElementPresent(
      deploymentPage.resourceAnnotations.buttonUpdate,
    );
    deploymentPage.resourceAnnotations.newValue(
      annotations_newkey,
      annotations_newvalue,
    );
    deploymentPage.resourceAnnotations.clickUpdate();
    deploymentPage.resourceBasicInfo.resourceInfoTab.clickTabByText('事件');
    deploymentPage.resourceBasicInfo.resourceInfoTab.clickTabByText('基本信息');
    // 验证注解修改正确
    deploymentPage.resourceBasicInfo.basicInfo
      .getElementByText('注解')
      .getText()
      .then(function(labelText) {
        expect(labelText).toContain(annotations_newvalue);
      });
  });

  it('L1:AldK8S-47: 单击左导航计算／部署 -- 进入Deployment 列表页--单击一个Deployment名称--进入详情页--单击操作--选择扩缩容--验证更新成功', () => {
    deploymentPage.operation.select('扩缩容');
    CommonPage.waitElementPresent(deploymentPage.resourceScale.buttonConfirm);
    deploymentPage.resourceScale.inputScale('2');
    deploymentPage.resourceScale.clickConfirm();
    deploymentPage.resourceBasicInfo.resourceInfoTab.clickTabByText('事件');
    deploymentPage.resourceBasicInfo.resourceInfoTab.clickTabByText('基本信息');
    browser.driver
      .wait(() => {
        deploymentPage.resourceBasicInfo.resourceInfoTab.clickTabByText('事件');
        deploymentPage.resourceBasicInfo.resourceInfoTab.clickTabByText(
          '基本信息',
        );
        return deploymentPage.detailPage.podList.getRowCount().then(count => {
          return count === 2;
        });
      }, 120000)
      .then(
        () => true,
        err => {
          console.warn('扩容失败: [' + err + ']');
          return false;
        },
      );
    // 验证注解修改正确
    expect(deploymentPage.detailPage.podList.getRowCount()).toBe(2);
  });

  it('L3:AldK8S-52: 单击左导航计算／部署 -- 进入Deployment 列表页--单击一个Deployment名称--进入详情页--单击事件 tab--验证事件显示正确', () => {
    deploymentPage.resourceBasicInfo.resourceInfoTab.clickTabByText('事件');

    // 验证事件的数量大于0
    deploymentPage.detailPage.eventList.getRowCount().then(rowcount => {
      expect(rowcount).toBeGreaterThan(0);
    });
  });

  it('L1:AldK8S-50: 单击左导航计算／部署 -- 进入Deployment 列表页--单击一个Deployment名称--进入详情页--单击配置管理 tab--验证配置管理显示正确', () => {
    deploymentPage.resourceBasicInfo.resourceInfoTab.clickTabByText('配置管理');

    // 验证Pod 详情页【配置管理】表格部分，名称值显示正确
    deploymentPage.detailPage.configmapList
      .getCell('值', [configmap])
      .then(elem => {
        elem.getText().then(text => {
          expect(text).toContain(configmap);
        });
      });
  });

  it('L1:AldK8S-49: 单击左导航计算／部署 -- 进入Deployment 列表页--单击一个Deployment名称--进入详情页--单击YAML tab--验证显示成功', () => {
    deploymentPage.resourceBasicInfo.resourceInfoTab.clickTabByText('YAML');
    deploymentPage.detailPage.yamlEditor.getYamlValue().then(textyaml => {
      // 验证YAML 显示正确
      expect(CommonMethod.parseYaml(textyaml).metadata.name).toBe(name);
    });
  });

  it('L1:AldK8S-46: 单击左导航计算／部署 -- 进入Deployment 列表页--单击一个Deployment名称--进入详情页--单击操作--选择更新Deployment--验证更新成功', () => {
    deploymentPage.operation.select('更新');

    deploymentPage.selectText('YAML');

    deploymentPage.detailPage.yamlEditor.getYamlValue().then(ymalvalue => {
      deploymentPage.detailPage.yamlEditor.setYamlValue(
        String(ymalvalue).replace(label_newkey, label),
      );
    });
    deploymentPage.getButtonByText('更新').click();
    CommonPage.clickLeftNavByText('部署');
    deploymentPage.namespace.select(namespace);
    deploymentPage.resourceTable.clickResourceNameByRow(rowkey);
    // 验证标签修改正确
    deploymentPage.resourceBasicInfo.basicInfo
      .getElementByText('标签')
      .getText()
      .then(function(labelText) {
        expect(labelText).toContain(label);
      });
  });
  it('AldK8S-191:L1:单击左导航计算/部署--进入Deployment列表--点击已有的Deployment的操作--点击更新--通过UI更新--验证更新成功', () => {
    deploymentPage.operation.select('更新');
    deploymentPage.clickAddVolume();
    CommonPage.waitElementDisplay(addVolumePage.cancelButton);
    addVolumePage.addVolume('updateVolume', '主机路径', 'test/hostpath');
    deploymentPage.getButtonByText('更新').click();
    CommonPage.clickLeftNavByText('部署');
    deploymentPage.namespace.select(namespace);
    deploymentPage.resourceTable.clickResourceNameByRow(rowkey);
    deploymentPage.resourceBasicInfo.resourceInfoTab.clickTabByText('YAML');
    // 验证修改成功
    deploymentPage.detailPage.yamlEditor.getYamlValue().then(ymalvalue => {
      expect(CommonMethod.parseYaml(ymalvalue).metadata.name).toBe(name);
    });
  });

  it('L1:AldK8S-48: 单击左导航计算／部署 -- 进入Deployment 列表页--单击一个Deployment名称--进入详情页--单击操作--选择删除--验证删除成功', () => {
    CommonPage.clickLeftNavByText('部署');
    deploymentPage.namespace.select(namespace);
    deploymentPage.resourceTable.clickResourceNameByRow(rowkey);
    deploymentPage.operation.select('删除');
    deploymentPage.confirmDialog.clickConfirm();
    expect(deploymentPage.toastsuccess.getText()).toBe('删除成功');
    CommonPage.clickLeftNavByText('部署');
    deploymentPage.namespace.select(namespace);

    // 验证删除功能正确
    expect(deploymentPage.resourceTable.noResult.getText()).toBe('无部署');
  });
});
