/**
 * Created by liuwei on 2018/3/15.
 */
import { browser, by, element } from 'protractor';

import { DeploymentPage } from '../../page_objects/computer/deployment.page';
import { CommonApi } from '../../utility/common.api';
import { CommonKubectl } from '../../utility/common.kubectl';
import { CommonMethod } from '../../utility/common.method';
import { CommonPage } from '../../utility/common.page';
import { k8s_type_deployments } from '../../utility/resource.type.k8s';

describe('L1:部署配置管理功能的验证', () => {
  const deploymentPage = new DeploymentPage();
  const label_newkey = CommonMethod.random_generate_testData();
  const label_newvalue = CommonMethod.random_generate_testData();
  const name = CommonMethod.random_generate_testData();
  const configmap = name;

  // 根据【名称，命名空间， 类型】唯一确定资源列表的一行
  const rowkey = [name];

  beforeAll(() => {
    deploymentPage.navigateTo();
    // 创建一个Deployment
    this.testdata = CommonKubectl.createResource(
      'deployment.yaml',
      {
        '${NAME}': name,
        '${NAMESPACE}': name,
        '${LABEL}': name,
      },
      'l2.deployment-detailconfig.e2e-spec',
    );
    // 选择新创建的namespace
    deploymentPage.resourceTable.clickResourceNameByRow([name]);
    CommonPage.waitProgressbarNotDisplay();
  });

  afterAll(() => {
    // 删除测试数据
    CommonKubectl.deleteResourceByYmal(this.testdata);
  });
  it('AldK8S-261:L1:单击左导航计算/部署--进入部署列表--点击部署名称--进入详情页面--点击配置管理--点击环境变量后的蓝色笔--添加与引用环境变量--验证环境变量与引用的', () => {
    deploymentPage.resourceBasicInfo.resourceInfoTab.clickTabByText('配置管理');
    deploymentPage.clickBasicPencil('环境变量');
    deploymentPage
      .getAlaudaInputTable()
      .fillinTablenolabel([label_newkey, label_newvalue, '0']);
    deploymentPage.clickconfirmButton();
    // 验证Pod 详情页【配置管理】表格部分，名称值显示正确
    deploymentPage.detailPage.configmapList
      .getCell('名称', [label_newkey])
      .then(elem => {
        elem.getText().then(text => {
          expect(text).toBe(label_newkey);
        });
      });
    deploymentPage.detailPage.configmapList
      .getCell('值', [label_newvalue])
      .then(elem => {
        elem.getText().then(text => {
          expect(text).toBe(label_newvalue);
        });
      });
  });
  it('AldK8S-262:L1:单击左导航计算/部署进入部署列表--点击部署名称进入详情页面--点击配置管理--点击配置引用后的蓝色笔--添加配引用--验证引用成功', () => {
    deploymentPage.navigateTo();
    deploymentPage.resourceTable.clickResourceNameByRow([name]);
    CommonPage.waitProgressbarNotDisplay();
    deploymentPage.resourceBasicInfo.resourceInfoTab.clickTabByText('配置管理');

    deploymentPage.clickBasicPencil('配置引用');
    deploymentPage.getmuitiSelect().select(name);
    deploymentPage.clickconfirmButton();
    browser.sleep(1000);
    // 验证Pod 详情页【配置管理】表格部分，名称值显示正确
    deploymentPage.detailPage.configmapList
      .getRow([name])
      .getText()
      .then(text => {
        expect(text).toContain(name);
      });
  });
});
