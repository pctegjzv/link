import { browser, element } from 'protractor';

import { AddVolumePage } from '../../page_objects/computer/addvolumes.page';
import { DeploymentPage } from '../../page_objects/computer/deployment.page';
import { CommonApi } from '../../utility/common.api';
import { CommonKubectl } from '../../utility/common.kubectl';
import { CommonMethod } from '../../utility/common.method';
import { CommonPage } from '../../utility/common.page';
import { k8s_type_namespaces } from '../../utility/resource.type.k8s';

describe('L0:部署创建功能通过UI', () => {
  const deploymentPage = new DeploymentPage();
  const addVolumePage = new AddVolumePage();
  const namespace = CommonMethod.random_generate_testData();
  const label = CommonMethod.random_generate_testData();
  const name = CommonMethod.random_generate_testData();
  const name1 = CommonMethod.random_generate_testData();
  const name2 = CommonMethod.random_generate_testData();
  const name3 = CommonMethod.random_generate_testData();
  const name4 = CommonMethod.random_generate_testData();
  const name5 = CommonMethod.random_generate_testData();
  const name6 = CommonMethod.random_generate_testData();

  // 根据【名称，命名空间， 类型】唯一确定资源列表的一行

  beforeAll(() => {
    deploymentPage.navigateTo();
    // 创建一个Deployment前先创建一个命名空间和一个配置文件
    this.testdata = CommonKubectl.createResource(
      'deploymentprapare.yaml',
      {
        '${NAME}': name,
        '${NAMESPACE}': namespace,
        '${LABEL}': label,
      },
      'L0.DeploymentCreate.e2e-spec',
    );
  });
  beforeEach(() => {
    deploymentPage.navigateTo();
  });
  afterAll(() => {
    // 删除测试数据
    CommonKubectl.execKubectlCommand(`kubectl delete pv ${name}`);
    CommonKubectl.execKubectlCommand(`kubectl delete StorageClass ${name}`);
    CommonApi.deleteResource(namespace, k8s_type_namespaces, namespace);
  });

  it('AldK8S-189:L0: 单击左导航计算 / 部署 -- 进入deploment列表--单击创建部署 -- 点击右上角表单 -- 输入合法的参数 -- 点击创建 -- 验证创建成功', () => {
    deploymentPage.getButtonByText('创建部署').click();
    CommonPage.waitProgressbarNotDisplay();
    // UI创建Deployment
    deploymentPage.fillinBasical(namespace, name4, '1', 'RollingUpdate');
    deploymentPage.clickCreateButton();
    // 验证创建成功后，有成功的toast 提示
    // expect(deploymentPage.toastsuccess.message.isPresent()).toBeTruthy();
    expect(deploymentPage.toastsuccess.getText()).toContain('创建成功');
    // 进入列表页面
    deploymentPage.navigateTo();
    expect(deploymentPage.getButtonByText('创建').isPresent()).toBeFalsy();
    // 验证Deployment运行情况
    CommonPage.waitElementPresent(
      deploymentPage.resourceTable.getCell('名称', [name4]),
    );
    deploymentPage.resourceTable.getCell('状态', [name4]).then(elem => {
      CommonPage.waitElementTextChangeTo(elem, '1 / 1');
      expect(elem.getText()).toBe('1 / 1');
    });
  });

  it('AldK8S-192:L1: 单击左导航计算 / 部署 -- 进入deployment列表 -- 单击创建部署 -- 填写命令echo 其他参数正确 -- 点击创建验证命令已执行', () => {
    deploymentPage.getButtonByText('创建部署').click();
    CommonPage.waitProgressbarNotDisplay();
    // UI创建Deployment
    deploymentPage.fillinBasical(namespace, name1, '1', 'RollingUpdate');
    deploymentPage.fillinContainerAdvanced('', 'echo hello');
    deploymentPage.clickCreateButton();
    // 验证创建成功后，有成功的toast 提示
    // expect(deploymentPage.toastsuccess.message.isPresent()).toBeTruthy();
    expect(deploymentPage.toastsuccess.getText()).toContain('创建成功');
    // 进入列表页面
    deploymentPage.resourceBasicInfo.resourceInfoTab.clickTabByText('日志');
    deploymentPage.resourceBasicInfo.logviewer.getToolbarButton('查找').click();
    deploymentPage.resourceBasicInfo.logviewer.waitLogDisplay('echo hello');
    deploymentPage.resourceBasicInfo.logviewer.finder_inputbox.input(
      'echo hello',
    );
    deploymentPage.resourceBasicInfo.logviewer.resultfinder
      .getText()
      .then(result => {
        expect(result.includes('无结果')).toBeFalsy();
      });
    deploymentPage.navigateTo();
    expect(deploymentPage.getButtonByText('创建').isPresent()).toBeFalsy();
    // 验证Deployment运行情况
    deploymentPage.resourceTable.getCell('状态', [name1]).then(elem => {
      expect(elem.getText()).toBe('0 / 1');
    });
  });
  it('AldK8S-194:L1: 单击左导航计算/部署--进入deploment列表--单击创建部署--点击右上角表单--创建两个容器输入合法的参数--点击创建--验证创建成功', () => {
    deploymentPage.getButtonByText('创建部署').click();
    CommonPage.waitProgressbarNotDisplay();
    // UI创建Deployment
    deploymentPage.fillinBasical(namespace, name3, '1', 'RollingUpdate');
    deploymentPage.fillincontainer(name, 'nginx:alpine', '1');
    deploymentPage.clickAddContaineButton();
    deploymentPage.fillincontainer(
      name1,
      'index.alauda.cn/alaudaorg/qaimages:volumetest',
      '1',
    );
    deploymentPage.clickCreateButton();
    // 验证创建成功后，有成功的toast 提示
    // expect(deploymentPage.toastsuccess.message.isPresent()).toBeTruthy();
    expect(deploymentPage.toastsuccess.getText()).toContain('创建成功');
    // 进入列表页面

    deploymentPage.navigateTo();
    expect(deploymentPage.getButtonByText('创建').isPresent()).toBeFalsy();
    // 验证Deployment运行情况
    CommonPage.waitElementPresent(
      deploymentPage.resourceTable.getCell('名称', [name3]),
    );
    deploymentPage.resourceTable.getCell('状态', [name3]).then(elem => {
      CommonPage.waitElementTextChangeTo(elem, '1 / 1');
      expect(elem.getText()).toBe('1 / 1');
    });
  });

  it('AldK8S-195:L1: 单击左导航的计算/部署--进入deployment列表--单击创建部署--点击右上角表单--填写资源限制其他参数正确--点击创建--验证创建成功', () => {
    deploymentPage.getButtonByText('创建部署').click();
    CommonPage.waitProgressbarNotDisplay();
    // UI创建Deployment
    deploymentPage.fillinBasical(namespace, name, '1', 'RollingUpdate');
    // delete
    browser.sleep(10000);

    deploymentPage.fillincontainer(
      name,
      'index.alauda.cn/alaudaorg/qaimages:volumetest',
      '1',
      '1',
    );
    deploymentPage.clickCreateButton();
    // 验证创建成功后，有成功的toast 提示
    // expect(deploymentPage.toastsuccess.message.isPresent()).toBeTruthy();
    expect(deploymentPage.toastsuccess.getText()).toContain('创建成功');
    // 进入列表页面
    deploymentPage.navigateTo();
    expect(deploymentPage.getButtonByText('创建').isPresent()).toBeFalsy();
    // 验证Deployment运行情况
    CommonPage.waitElementPresent(
      deploymentPage.resourceTable.getCell('名称', [name]),
    );
    deploymentPage.resourceTable.getCell('状态', [name]).then(elem => {
      CommonPage.waitElementTextChangeTo(elem, '1 / 1');
      expect(elem.getText()).toBe('1 / 1');
    });
  });

  it('AldK8S-190:L1: 单击左导航计算/部署--进入deployment列表--单击创建部署--输入合法参数--点击右上角yaml--验证页面输入参数以转换成yaml', () => {
    deploymentPage.getButtonByText('创建部署').click();
    CommonPage.waitProgressbarNotDisplay();
    deploymentPage.fillinBasical(
      namespace,
      'testUItoyaml',
      '3',
      'RollingUpdate',
    );
    deploymentPage.clickAddVolume();
    CommonPage.waitElementDisplay(addVolumePage.cancelButton);

    addVolumePage.addVolume('testvolume', '配置字典', name);
    browser.sleep(100);
    deploymentPage.fillinContainerAdvanced('testchangtest', 'echo hello');
    deploymentPage.fillinVolumeMounts(
      'testvolume',
      'test/config',
      '/test/config',
    );
    deploymentPage.fillincontainer(
      name4,
      'index.alauda.cn/alaudaorg/qaimages:volumetest',
      '1',
    );
    // 切换到YAML创建
    deploymentPage.selectText('YAML');
    CommonPage.waitElementPresent(deploymentPage.yamlCreator.toolbar);
    deploymentPage.yamlCreator.getYamlValue().then(text => {
      expect(text).toContain('namespace: ' + namespace);
      expect(text).toContain('name: ' + 'testUItoyaml');
      expect(text).toContain('replicas: ' + '3');
      // FIXME: disable for now since AUI changes value change behaviour
      // expect(text).toContain('RollingUpdate');
      expect(text).toContain('testchangtest');
      expect(text).toContain('cpu: 50m');
      expect(text).toContain('memory: 50Mi');
      expect(text).toContain('mountPath: /test/config');
      expect(text).toContain('subPath: test/config');
      expect(text).toContain(
        `image: 'index.alauda.cn/alaudaorg/qaimages:volumetest'`,
      );
    });
  });
});
