/**
 * Created by liuwei on 2018/3/15.
 */
import { by, element } from 'protractor';

import { DeploymentPage } from '../../page_objects/computer/deployment.page';
import { CommonApi } from '../../utility/common.api';
import { CommonKubectl } from '../../utility/common.kubectl';
import { CommonMethod } from '../../utility/common.method';
import { CommonPage } from '../../utility/common.page';
import { k8s_type_namespaces } from '../../utility/resource.type.k8s';

describe('L0:部署列表页的创建功能', () => {
  const deploymentPage = new DeploymentPage();
  const namespace = CommonMethod.random_generate_testData();
  const label = CommonMethod.random_generate_testData();
  const name = CommonMethod.random_generate_testData();

  // 根据【名称，命名空间， 类型】唯一确定资源列表的一行
  const rowkey = [name];

  beforeAll(() => {
    deploymentPage.navigateTo();
    // 创建一个Deployment前先创建一个命名空间和一个配置文件
    this.testdata = CommonKubectl.createResource(
      'deploymentprapare.yaml',
      {
        '${NAME}': name,
        '${NAMESPACE}': namespace,
        '${LABEL}': label,
      },
      'l0.deployment-createprepare.e2e-spec',
    );
    // 准备deployment的测试数据
    this.testdata = CommonMethod.generateStringYaml(
      'deploymentcreate.yaml',
      {
        '${NAME}': name,
        '${NAMESPACE}': namespace,
        '${LABEL}': label,
      },
      'l0.deployment-create.e2e-spec',
    );
  });

  afterAll(() => {
    // 删除测试数据
    CommonKubectl.execKubectlCommand(`kubectl delete namespaces ${namespace}`);
  });

  it('L0:AldK8S-102: 单击左导航计算／部署 -- 进入Deployment 列表页--单击创建部署按钮--进入创建页--输入合法yaml, 单击创建按钮--验证创建成功', () => {
    deploymentPage.getButtonByText('创建部署').click();

    // 切换到YAML创建
    deploymentPage.selectText('YAML');

    deploymentPage.yamlCreator.clickToolbarByName('清空');

    deploymentPage.yamlCreator.setYamlValue(this.testdata.get('data'));
    deploymentPage.yamlCreator.clickButtonCreate();

    // 验证创建成功后，有成功的toast 提示
    // expect(deploymentPage.toastsuccess.message.isPresent()).toBeTruthy();
    CommonPage.waitElementPresent(deploymentPage.toastsuccess.message);
    expect(deploymentPage.toastsuccess.getText()).toContain('创建成功');
    // 跳转回列表页
    deploymentPage.navigateTo();
    expect(deploymentPage.getButtonByText('创建').isPresent()).toBeFalsy();

    deploymentPage.resourceTable.getCell('状态', [name]).then(elem => {
      CommonPage.waitElementTextChangeTo(elem, '1 / 1');
      expect(elem.getText()).toBe('1 / 1');
    });
  });
});
