/**
 * Created by liuwei on 2018/3/15.
 */

import { ServicePage } from '../../page_objects/network/service.page';
import { CommonKubectl } from '../../utility/common.kubectl';
import { CommonMethod } from '../../utility/common.method';

describe('L0:服务创建功能', () => {
  const servicePage = new ServicePage();
  const namespace_headless = CommonMethod.random_generate_testData();
  const namespace_clusterIP = CommonMethod.random_generate_testData();
  const namespace_nodePort = CommonMethod.random_generate_testData();
  const namespace_loadBalancer = CommonMethod.random_generate_testData();
  const label = CommonMethod.random_generate_testData();

  const headlessServiceName = CommonMethod.random_generate_testData();
  const clusterIPServiceName = CommonMethod.random_generate_testData();
  const nodePortServiceName = CommonMethod.random_generate_testData();
  const loadBalancerServiceName = CommonMethod.random_generate_testData();
  // 根据【名称，命名空间， 类型】唯一确定资源列表的一行
  const rowkey = [headlessServiceName];

  beforeAll(() => {
    servicePage.navigateTo();

    this.headless = CommonMethod.generateStringYaml(
      'service.yaml',
      {
        '${NAME}': headlessServiceName,
        '${NAMESPACE}': namespace_headless,
        '${LABEL}': label,
        '${SERVICETYPE}': 'ClusterIP',
        '${clusterIP}': 'None',
      },
      'l0.servicecreate.headless',
    );
    CommonMethod.writeyamlfile('temp_headless.yaml', this.headlessYaml);

    this.clusterIp = CommonMethod.generateStringYaml(
      'service.yaml',
      {
        '${NAME}': clusterIPServiceName,
        '${NAMESPACE}': namespace_clusterIP,
        '${LABEL}': label,
        '${SERVICETYPE}': 'ClusterIP',
        '${clusterIP}': '',
      },
      'l0.servicecreate.clusterip',
    );
    CommonMethod.writeyamlfile('temp_cluster.yaml', this.clusterIpYaml);

    this.nodePort = CommonMethod.generateStringYaml(
      'service.yaml',
      {
        '${NAME}': nodePortServiceName,
        '${NAMESPACE}': namespace_nodePort,
        '${LABEL}': label,
        '${SERVICETYPE}': 'NodePort',
        '${clusterIP}': '',
      },
      'l0.servicecreate.nodeport',
    );
    CommonMethod.writeyamlfile('temp_nodePort.yaml', this.nodePortYaml);

    this.loadBalancer = CommonMethod.generateStringYaml(
      'service.yaml',
      {
        '${NAME}': loadBalancerServiceName,
        '${NAMESPACE}': namespace_loadBalancer,
        '${LABEL}': label,
        '${SERVICETYPE}': 'LoadBalancer',
        '${clusterIP}': '',
      },
      'l0.servicecreate.loadbalancer',
    );
    CommonMethod.writeyamlfile('temp_loadBalancer.yaml', this.loadBalancerYaml);
  });

  afterAll(() => {
    // 删除测试数据
    CommonKubectl.deleteResourceByYmal(this.headless.get('name'));
    CommonKubectl.deleteResourceByYmal(this.clusterIp.get('name'));
    CommonKubectl.deleteResourceByYmal(this.nodePort.get('name'));
    CommonKubectl.deleteResourceByYmal(this.loadBalancer.get('name'));
  });

  it('L0:AldK8S-114: 单击左导航网络／服务 -- 进入服务列表页--单击创建服务按钮--验证创建Headless 服务成功', () => {
    servicePage.getButtonByText('创建服务').click();
    servicePage.yamlCreator.setYamlValue(this.headless.get('data'));
    servicePage.yamlCreator.clickButtonCreate();
    // expect(servicePage.toastsuccess.getText()).toContain('创建成功');
    servicePage.namespace.select(namespace_headless);
    expect(servicePage.resourceTable.getRowCount()).toBe(1);
    servicePage.resourceTable.getCell('集群IP', rowkey).then(elem => {
      // headless service 的集群IP 为 None
      expect(elem.getText()).toBe('None');
    });
  });

  it('L0:AldK8S-116: 单击左导航网络／服务 -- 进入服务列表页--单击创建服务按钮--验证创建 NodePort 服务成功', () => {
    servicePage.getButtonByText('创建服务').click();
    servicePage.yamlCreator.setYamlValue(this.nodePort.get('data'));
    servicePage.yamlCreator.clickButtonCreate();
    // expect(servicePage.toastsuccess.getText()).toContain('创建成功');
    servicePage.namespace.select(namespace_nodePort);
    expect(servicePage.resourceTable.getRowCount()).toBe(1);
    servicePage.resourceTable
      .getCell('类型', [nodePortServiceName])
      .then(elem => {
        // NodePort service 的类型 为 NodePort
        expect(elem.getText()).toBe('NodePort');
      });
  });

  it('L0:AldK8S-117: 单击左导航网络／服务 -- 进入服务列表页--单击创建服务按钮--验证创建LoadBalancer 服务成功', () => {
    servicePage.getButtonByText('创建服务').click();
    servicePage.yamlCreator.setYamlValue(this.loadBalancer.get('data'));
    servicePage.yamlCreator.clickButtonCreate();
    // expect(servicePage.toastsuccess.getText()).toContain('创建成功');
    servicePage.namespace.select(namespace_loadBalancer);
    expect(servicePage.resourceTable.getRowCount()).toBe(1);
    servicePage.resourceTable
      .getCell('类型', [loadBalancerServiceName])
      .then(elem => {
        // NodePort service 的类型 为 NodePort
        expect(elem.getText()).toBe('LoadBalancer');
      });
  });

  it('L0:AldK8S-115: 单击左导航网络／服务 -- 进入服务列表页--单击创建服务按钮--验证创建ClusterIP 服务成功', () => {
    servicePage.getButtonByText('创建服务').click();
    servicePage.yamlCreator.setYamlValue(this.clusterIp.get('data'));
    servicePage.yamlCreator.clickButtonCreate();
    // expect(servicePage.toastsuccess.getText()).toContain('创建成功');
    servicePage.namespace.select(namespace_clusterIP);
    expect(servicePage.resourceTable.getRowCount()).toBe(1);
    servicePage.resourceTable
      .getCell('类型', [clusterIPServiceName])
      .then(elem => {
        // NodePort service 的类型 为 NodePort
        expect(elem.getText()).toBe('ClusterIP');
      });
  });
});
