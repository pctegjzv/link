/**
 * Created by liuwei on 2018/3/15.
 */
import { by, element } from 'protractor';

import { JobPage } from '../../page_objects/computer/job.page';
import { CommonApi } from '../../utility/common.api';
import { CommonKubectl } from '../../utility/common.kubectl';
import { CommonMethod } from '../../utility/common.method';
import { CommonPage } from '../../utility/common.page';
import { k8s_type_namespaces } from '../../utility/resource.type.k8s';

describe('L0:任务创建功能', () => {
  const jobPage = new JobPage();
  const name = CommonMethod.random_generate_testData();

  // 根据【名称，命名空间， 类型】唯一确定资源列表的一行
  const rowkey = [name];

  beforeAll(() => {
    jobPage.navigateTo();
    // 准备deployment的测试数据
    this.testdata = CommonMethod.generateStringYaml(
      'job.yaml',
      {
        '${NAME}': name,
        '${parallelismnum}': '1',
        '${completionsnum}': '2',
        '${backoffLimitnum}': '2',
      },
      'l0.job-create.e2e-spec',
    );
  });
  afterAll(() => {
    // 删除测试数据
    CommonApi.deleteResource(name, k8s_type_namespaces, name);
  });
  it('AldK8S-234:L0:单击计算/任务--进入任务列表--点击创建任务--输入正确的yaml--点击创建--验证任务创建成功', () => {
    jobPage.getButtonByText('创建任务').click();
    jobPage.yamlCreator.setYamlValue(this.testdata.get('data'));
    jobPage.yamlCreator.clickButtonCreate();
    // 验证创建成功后，有成功的toast 提示
    // expect(deploymentPage.toastsuccess.message.isPresent()).toBeTruthy();
    expect(jobPage.toastsuccess.getText()).toContain('创建成功');
    // 跳转回列表页
    CommonPage.waitElementPresent(
      jobPage.resourceTable.getCell('名称', [name]),
    );
    jobPage.resourceTable.getCell('状态', [name]).then(elem => {
      CommonPage.waitElementTextChangeTo(elem, '2 / 2');
      expect(elem.getText()).toBe('2 / 2');
    });
  });
});
