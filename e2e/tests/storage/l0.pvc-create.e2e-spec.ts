/**
 * Created by liuwei on 2018/5/24.
 */

import { PvcPage } from '../../page_objects/storage/pvc.page';
import { CommonKubectl } from '../../utility/common.kubectl';
import { CommonMethod } from '../../utility/common.method';

describe('L0:PVC创建功能', () => {
  const pvcPage = new PvcPage();
  const name = CommonMethod.random_generate_testData();
  // 根据【名称，命名空间， 类型】唯一确定资源列表的一行
  const rowkey = [name];

  beforeAll(() => {
    pvcPage.navigateTo();
    this.testdata = CommonMethod.generateStringYaml(
      'pvc.yaml',
      {
        '${NAME}': name,
        '${LABEL}': CommonMethod.random_generate_testData(),
        '${NAMESPACE}': CommonMethod.random_generate_testData(),
      },
      'l0.pvc-create.e2e-spec',
    );
  });

  afterAll(() => {
    // 删除测试数据
    CommonKubectl.deleteResourceByYmal(this.testdata.get('name'));
    CommonKubectl.execKubectlCommand(`kubectl delete pv ${name}`);
    CommonKubectl.execKubectlCommand(`kubectl delete StorageClass ${name}`);
  });

  it('L0:AldK8S-180: 单击左导航存储／持久卷声明 -- 进入持久卷声明的列表页--单击创建持久卷声明--输入yaml 样例，修改名称，单击创建按钮--验证创建成功', () => {
    pvcPage.getButtonByText('创建持久卷声明').click();
    pvcPage.yamlCreator.labelwrite.click();
    pvcPage.yamlCreator.getYamlValue().then((yaml: string) => {
      pvcPage.yamlCreator.clickToolbarByName('清空');
      pvcPage.yamlCreator.setYamlValue(this.testdata.get('data'));
      pvcPage.yamlCreator.clickButtonCreate();
      // expect(pvcPage.toastsuccess.getText()).toContain('创建成功');
    });

    // 验证pv 创建成功
    pvcPage.searchBox.search(name);
    expect(pvcPage.resourceTable.getRowCount()).toBe(1);
    pvcPage.resourceTable.getCell('名称', rowkey).then(cell => {
      expect(cell.$('a[href*=persistentvolumeclaim]').getText()).toBe(name);
    });
  });
});
