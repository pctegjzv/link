/**
 * Created by liuwei on 2018/5/23.
 */
import { PvcPage } from '../../page_objects/storage/pvc.page';
import { CommonKubectl } from '../../utility/common.kubectl';
import { CommonMethod } from '../../utility/common.method';
import { CommonPage } from '../../utility/common.page';

describe('L1:PVC更新功能', () => {
  const pvcPage = new PvcPage();

  const label_newkey = CommonMethod.random_generate_testData();
  const label_newvalue = CommonMethod.random_generate_testData();
  const annotations_newkey = CommonMethod.random_generate_testData();
  const annotations_newvalue = CommonMethod.random_generate_testData();

  const name = CommonMethod.random_generate_testData();
  // 根据【名称，命名空间， 类型】唯一确定资源列表的一行
  const rowkey = [name];

  beforeAll(() => {
    pvcPage.navigateTo();
    this.testdata = CommonKubectl.createResource(
      'pvc.yaml',
      {
        '${NAME}': name,
        '${LABEL}': CommonMethod.random_generate_testData(),
        '${NAMESPACE}': CommonMethod.random_generate_testData(),
      },
      'l1.pvc-update.e2e-spec',
    );
  });

  afterAll(() => {
    // 删除测试数据
    CommonKubectl.deleteResourceByYmal(this.testdata);
    CommonKubectl.execKubectlCommand(`kubectl delete pv ${name}`);
    CommonKubectl.execKubectlCommand(`kubectl delete StorageClass ${name}`);
  });

  it('L1:AldK8S-181: 单击左导航存储／持久卷声明 -- 进入持久卷声明列表页--选择一个持久卷声明，单击操作列，选择更新标签--验证更新标签功能正确', () => {
    pvcPage.resourceTable.clickOperationButtonByRow(rowkey, '更新标签');
    pvcPage.resourcelabel.newValue(label_newkey, label_newvalue);
    pvcPage.resourcelabel.clickUpdate();
    expect(pvcPage.toastsuccess.getText()).toBe('更新成功');
    expect(pvcPage.resourceTable.getCellLabel(rowkey, '名称')).toContain(
      `${label_newkey}: ${label_newvalue}`,
    );
  });

  it('L1:AldK8S-182: 单击左导航存储／持久卷声明 -- 进入持久卷声明列表页--选择一个持久卷声明，单击操作列，选择更新注解--验证更新注解功能正确', () => {
    pvcPage.resourceTable.clickOperationButtonByRow(rowkey, '更新注解');
    pvcPage.resourceAnnotations.newValue(
      annotations_newkey,
      annotations_newvalue,
    );
    pvcPage.resourceAnnotations.clickUpdate();
    expect(pvcPage.toastsuccess.getText()).toBe('更新成功');
  });

  it('L1:AldK8S-183: 单击左导航存储／持久卷声明 -- 进入持久卷声明列表页--选择一个持久卷声明，单击操作列，选择更新--验证更新功能正确', () => {
    pvcPage.resourceTable.clickResourceNameByRow(rowkey);
    pvcPage.operation.select('更新');
    pvcPage.yamlCreator.getYamlValue().then(yaml => {
      pvcPage.yamlCreator.setYamlValue(
        String(yaml).replace('storage: 5Gi', 'storage: 6Gi'),
      );
      pvcPage.yamlCreator.clickButtonCreate();
      expect(pvcPage.toastsuccess.getText()).toBe('更新成功');
    });
  });

  it('L1:AldK8S-184: 单击左导航存储／持久卷声明 -- 进入持久卷声明列表页--选择一个持久卷声明，单击操作列，选择删除--验证删除功能正确', () => {
    pvcPage.resourceTable.clickOperationButtonByRow(rowkey, '删除');
    pvcPage.confirmDialog.clickConfirm();

    expect(pvcPage.toastsuccess.getText()).toBe('删除成功');
    pvcPage.operation.select('删除');
    expect(pvcPage.confirmDialog.confirmTitle.getText()).toBe(
      `确定删除 持久卷声明 ${name} 吗?`,
    );
    pvcPage.confirmDialog.clickConfirm();
    expect(pvcPage.toastsuccess.getText()).toBe('删除成功');
    pvcPage.navigateTo();
    CommonPage.waitProgressbarNotDisplay();
    pvcPage.searchBox.search(name);
    expect(pvcPage.resourceTable.noResult.getText()).toBe('无持久卷声明');
  });
});
