import { browser, element } from 'protractor';

import { AddVolumePage } from '../../page_objects/computer/addvolumes.page';
import { StatefulsetPage } from '../../page_objects/computer/statefulset.page';
import { CommonApi } from '../../utility/common.api';
import { CommonKubectl } from '../../utility/common.kubectl';
import { CommonMethod } from '../../utility/common.method';
import { CommonPage } from '../../utility/common.page';
import { k8s_type_namespaces } from '../../utility/resource.type.k8s';

describe('L0:UI创建有状态副本集', () => {
  const page = new StatefulsetPage();
  const addVolumePage = new AddVolumePage();
  const name = CommonMethod.random_generate_testData();
  const name1 = CommonMethod.random_generate_testData();
  const name2 = CommonMethod.random_generate_testData();
  const name3 = CommonMethod.random_generate_testData();
  // 根据【名称，命名空间， 类型】唯一确定资源列表的一行

  beforeAll(() => {
    page.navigateTo();
    // 创建一个Deployment前先创建一个命名空间和一个配置文件
    this.testdata = CommonKubectl.createResource(
      'deploymentprapare.yaml',
      {
        '${NAME}': name,
        '${NAMESPACE}': name,
        '${LABEL}': name,
      },
      'l0.statusfulsetcreateprepare-spec',
    );
    CommonKubectl.createResource(
      'statusfulsetprepare.yaml',
      {
        '${NAME}': name,
        '${SERVICETYPE}': 'ClusterIP',
      },
      'l0.statusfulsetcreateprepare',
    );
  });
  beforeEach(() => {
    page.navigateTo();
  });
  afterAll(() => {
    // 删除测试数据
    CommonKubectl.execKubectlCommand(`kubectl delete pv ${name}`);
    CommonKubectl.execKubectlCommand(`kubectl delete StorageClass ${name}`);
    CommonApi.deleteResource(name, k8s_type_namespaces, name);
  });

  it('AldK8S-285:L0: 单击左导航计算 / 有状态副本集 -- 进入有状态副本集列表--单击创建有状态副本集--点击右上角表单--输入合法的参数--点击创建--验证创建成功', () => {
    page.getButtonByText('创建有状态副本集').click();
    CommonPage.waitProgressbarNotDisplay();
    // UI创建Deployment
    browser.sleep(4000);
    page.fillinBasical(name, name, name, 'RollingUpdate', '1');
    page.clickCreateButton();
    // 验证创建成功后，有成功的toast 提示
    // expect(page.toastsuccess.message.isPresent()).toBeTruthy();
    expect(page.toastsuccess.getText()).toContain('创建成功');
    // 进入列表页面
    page.navigateTo();
    expect(page.getButtonByText('创建').isPresent()).toBeFalsy();
    // 验证Deployment运行情况
    CommonPage.waitElementPresent(page.resourceTable.getCell('名称', [name]));
    page.resourceTable.getCell('名称', [name]).then(elem => {
      expect(elem.getText()).toContain(name);
    });
  });

  it('AldK8S-286:L1: 单击左导航计算/有状态副本集 -- 进入有状态副本集列表 -- 单击创建有状态副本集 -- 填写命令echo 其他参数正确 -- 点击创建验证命令已执行', () => {
    page.getButtonByText('创建有状态副本集').click();
    CommonPage.waitProgressbarNotDisplay();
    // UI创建Deployment
    page.fillinBasical(name, name1, name, 'RollingUpdate');
    page.fillinContainerAdvanced('', 'echo hello');
    page.clickCreateButton();
    // 验证创建成功后，有成功的toast 提示
    // expect(page.toastsuccess.message.isPresent()).toBeTruthy();
    expect(page.toastsuccess.getText()).toContain('创建成功');
    // 进入列表页面
    page.resourceBasicInfo.resourceInfoTab.clickTabByText('YAML');
    page.yamlCreator.getYamlValue().then(text => {
      expect(text).toContain('- echo hello');
    });
    page.navigateTo();
    expect(page.getButtonByText('创建').isPresent()).toBeFalsy();
    // 验证Deployment运行情况
    page.resourceTable.getCell('状态', [name1]).then(elem => {
      expect(elem.getText()).toBe(`0 / 1`);
    });
  });

  it('AldK8S-287:L1: 单击左导航计算/有状态副本集--进入有状态副本集列表--单击创建有状态副本集--点击右上角表单--创建两个容器输入合法的参数--点击创建--验证创建成功', () => {
    page.getButtonByText('创建有状态副本集').click();
    CommonPage.waitProgressbarNotDisplay();
    // UI创建Deployment
    page.fillinBasical(name, name2, name, 'RollingUpdate');
    page.fillincontainer(name, 'nginx:alpine', '1');
    page.clickAddContaineButton();
    page.fillincontainer(
      name2,
      'index.alauda.cn/alaudaorg/qaimages:volumetest',
      '1',
    );
    page.clickCreateButton();
    // 验证创建成功后，有成功的toast 提示
    // expect(page.toastsuccess.message.isPresent()).toBeTruthy();
    expect(page.toastsuccess.getText()).toContain('创建成功');
    // 进入列表页面

    page.navigateTo();
    expect(page.getButtonByText('创建').isPresent()).toBeFalsy();
    // 验证Deployment运行情况
    CommonPage.waitElementPresent(page.resourceTable._operationList);
    page.resourceTable.getCell('名称', [name2]).then(elem => {
      expect(elem.getText()).toContain(name2);
    });
  });

  it('AldK8S-288:L1: 单击左导航的计算/有状态副本集--进入有状态副本集列表--单击创建有状态副本集--点击右上角表单--填写资源限制其他参数正确--点击创建--验证创建成功', () => {
    page.getButtonByText('创建有状态副本集').click();
    CommonPage.waitProgressbarNotDisplay();
    // UI创建Deployment
    page.fillinBasical(name, name3, name, 'RollingUpdate');
    // delete
    browser.sleep(10000);

    page.fillincontainer(
      name,
      'index.alauda.cn/alaudaorg/qaimages:volumetest',
      '1',
      '1',
    );
    page.clickCreateButton();
    // 验证创建成功后，有成功的toast 提示
    // expect(page.toastsuccess.message.isPresent()).toBeTruthy();
    expect(page.toastsuccess.getText()).toContain('创建成功');
    // 进入列表页面
    page.navigateTo();
    expect(page.getButtonByText('创建').isPresent()).toBeFalsy();
    // 验证Deployment运行情况
    CommonPage.waitElementPresent(page.resourceTable.getCell('名称', [name3]));
    page.resourceTable.getCell('名称', [name3]).then(elem => {
      expect(elem.getText()).toContain(name3);
    });
  });

  it('AldK8S-289:L1: 单击左导航计算/有状态副本集--进入有状态副本集列表--单击创建有状态副本集--输入合法参数--点击右上角yaml--验证页面输入参数以转换成yaml', () => {
    page.getButtonByText('创建有状态副本集').click();
    CommonPage.waitProgressbarNotDisplay();
    page.fillinBasical(name, 'testUItoyaml', name, 'RollingUpdate', '1', '1');
    page.clickAddVolume();
    CommonPage.waitElementDisplay(addVolumePage.cancelButton);

    addVolumePage.addVolume('testvolume', '配置字典', name);
    browser.sleep(100);
    page.fillinContainerAdvanced('testchangtest', 'echo hello');
    page.fillinVolumeMounts('testvolume', 'test/config', '/test/config');
    page.fillincontainer(
      'testUItoyaml',
      'index.alauda.cn/alaudaorg/qaimages:volumetest',
      '1',
    );
    // 切换到YAML创建
    page.selectText('YAML');
    CommonPage.waitElementPresent(page.yamlCreator.toolbar);
    page.yamlCreator.getYamlValue().then(text => {
      expect(text).toContain('namespace: ' + name);
      expect(text).toContain('name: ' + 'testUItoyaml');
      //  expect(text).toContain('RollingUpdate');
      expect(text).toContain('testchangtest');
      expect(text).toContain('cpu: 50m');
      expect(text).toContain('memory: 50Mi');
      expect(text).toContain('mountPath: /test/config');
      expect(text).toContain('subPath: test/config');
      expect(text).toContain('test: linkautotest');
      expect(text).toContain('linkautotest: linkautotest');
      expect(text).toContain(
        `image: 'index.alauda.cn/alaudaorg/qaimages:volumetest'`,
      );
    });
  });
});
