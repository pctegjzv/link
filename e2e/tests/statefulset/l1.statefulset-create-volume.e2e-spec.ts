import { browser, element } from 'protractor';

import { AddVolumePage } from '../../page_objects/computer/addvolumes.page';
import { StatefulsetPage } from '../../page_objects/computer/statefulset.page';
import { CommonApi } from '../../utility/common.api';
import { CommonKubectl } from '../../utility/common.kubectl';
import { CommonMethod } from '../../utility/common.method';
import { CommonPage } from '../../utility/common.page';
import { k8s_type_namespaces } from '../../utility/resource.type.k8s';

describe('L1:有状态副本集创建功能通过UI', () => {
  const page = new StatefulsetPage();
  const addVolumePage = new AddVolumePage();
  const praparename = CommonMethod.random_generate_testData();
  const name1 = CommonMethod.random_generate_testData();
  const name2 = CommonMethod.random_generate_testData();
  const name3 = CommonMethod.random_generate_testData();
  // 根据【名称，命名空间， 类型】唯一确定资源列表的一行

  beforeAll(() => {
    page.navigateTo();
    // 创建前先创建一个命名空间和一个配置文件
    this.testdata = CommonKubectl.createResource(
      'deploymentprapare.yaml',
      {
        '${NAME}': praparename,
        '${NAMESPACE}': praparename,
        '${LABEL}': praparename,
      },
      'l0.statusfulsetcreateprepare-spec',
    );
    CommonKubectl.createResource(
      'statusfulsetprepare.yaml',
      {
        '${NAME}': praparename,
        '${SERVICETYPE}': 'ClusterIP',
      },
      'l0.statusfulsetcreateprepare',
    );
  });
  beforeEach(() => {
    page.navigateTo();
  });
  afterAll(() => {
    // 删除测试数据
    CommonKubectl.execKubectlCommand(`kubectl delete pv ${praparename}`);
    CommonKubectl.execKubectlCommand(
      `kubectl delete StorageClass ${praparename}`,
    );
    CommonApi.deleteResource(praparename, k8s_type_namespaces, praparename);
  });
  it('AldK8S-290:L1: 单击左导航计算/有状态副本集--进入有状态副本集列表--单击创建有状态副本集--添加配置文件--点击创建--验证有状态副本集成功有配置文件', () => {
    page.getButtonByText('创建有状态副本集').click();
    CommonPage.waitProgressbarNotDisplay();
    // UI创建有状态副本集
    page.fillinBasical(praparename, name1, praparename, 'RollingUpdate');
    page.clickAddVolume();

    CommonPage.waitElementDisplay(addVolumePage.cancelButton);

    addVolumePage.addVolume('testvolume', '配置字典', praparename);
    page.fillinVolumeMounts('testvolume', 'test/config', '/test/config');
    browser.sleep(5000);

    page.clickCreateButton();
    // 验证创建成功后，有成功的toast 提示
    // expect(page.toastsuccess.message.isPresent()).toBeTruthy();
    expect(page.toastsuccess.getText()).toContain('创建成功');
    // 进入列表页面

    page.navigateTo();
    expect(page.getButtonByText('创建').isPresent()).toBeFalsy();
    // 验证有状态副本集运行情况
    CommonPage.waitElementPresent(page.resourceTable.getCell('名称', [name1]));
    page.resourceTable.getCell('名称', [name1]).then(elem => {
      expect(elem.getText()).toContain(name1);
    });
  });
  it('AldK8S-291:L1: 点击左导航计算 / 有状态副本集 -- 进入有状态副本集列表 -- 点击创建有状态副本集 -- 点击右上角表单 -- 挂载保密字典其他都正确 -- 验证创建成功保密字典已挂载', () => {
    page.getButtonByText('创建有状态副本集').click();
    CommonPage.waitProgressbarNotDisplay();
    // UI创建有状态副本集
    page.fillinBasical(praparename, name2, praparename, 'RollingUpdate');
    page.clickAddVolume();
    addVolumePage.addVolume('testsecret', '保密字典', praparename);
    CommonPage.waitElementDisplay(addVolumePage.cancelButton);

    page.fillinVolumeMounts('testsecret', 'test/secret', '/test/secret');
    page.clickCreateButton();
    // 验证创建成功后，有成功的toast 提示
    expect(page.toastsuccess.getText()).toContain('创建成功');
    // 进入列表页面
    page.resourceBasicInfo.resourceInfoTab.clickTabByText('YAML');
    page.yamlCreator.getYamlValue().then(yaml => {
      expect(yaml).toContain('secretName: ' + praparename);
      expect(yaml).toContain('name: testsecret');
      expect(yaml).toContain('mountPath: /test/secret');
      expect(yaml).toContain('subPath: test/secret');
    });
    page.navigateTo();
    expect(page.getButtonByText('创建').isPresent()).toBeFalsy();
    // 验证有状态副本集运行情况
    page.resourceTable.getCell('名称', [name2]).then(elem => {
      expect(elem.getText()).toContain(name2);
    });
  });
  it('AldK8S-292:L1: 点击左导航计算 / 有状态副本集 -- 进入有状态副本集列表 -- 点击创建有状态副本集 -- 点击右上角表单 -- 挂载pvc其他都正确 -- 验证创建成功PVC已挂载', () => {
    page.getButtonByText('创建有状态副本集').click();
    CommonPage.waitProgressbarNotDisplay();
    // UI创建有状态副本集
    page.fillinBasical(praparename, name3, praparename, 'RollingUpdate');

    page.clickAddVolume();
    CommonPage.waitElementDisplay(addVolumePage.cancelButton);
    addVolumePage.addVolume('testpvc', '持久卷声明', praparename);
    browser.sleep(100);
    page.fillinVolumeMounts('testpvc', 'test/pvc', '/test/pvc');
    page.clickCreateButton();
    // 验证创建成功后，有成功的toast 提示
    expect(page.toastsuccess.getText()).toContain('创建成功');
    // 进入列表页面
    page.resourceBasicInfo.resourceInfoTab.clickTabByText('YAML');
    page.yamlCreator.getYamlValue().then(yaml => {
      expect(yaml).toContain('claimName: ' + praparename);
      expect(yaml).toContain('name: testpvc');
      expect(yaml).toContain('mountPath: /test/pvc');
      expect(yaml).toContain('subPath: test/pvc');
    });
  });
});
