/**
 * Created by liuwei on 2018/5/10.
 */

import { DaemonsetPage } from '../../page_objects/computer/daemonset.page';
import { CommonKubectl } from '../../utility/common.kubectl';
import { CommonMethod } from '../../utility/common.method';
import { CommonPage } from '../../utility/common.page';

describe('L0:守护进程集列表页创建功能', () => {
  const daemonsetPage = new DaemonsetPage();
  const namespace = CommonMethod.random_generate_testData();
  const label = CommonMethod.random_generate_testData();
  const name = CommonMethod.random_generate_testData();
  const selectorkey = CommonMethod.random_generate_testData();

  beforeAll(() => {
    daemonsetPage.navigateTo();
    // 创建一个Deployment前先创建一个命名空间和一个配置文件
    this.testdata = CommonKubectl.createResource(
      'deploymentprapare.yaml',
      {
        '${NAME}': name,
        '${NAMESPACE}': namespace,
        '${LABEL}': label,
      },
      'l0.daemonset-createprepare.e2e-spec',
    );
    // 创建1个命名空间，命名空间下面创建一个 replicasets.
    this.testdata = CommonMethod.generateStringYaml(
      'daemonsetcreate.yaml',
      {
        '${NAME}': name,
        '${NAMESPACE}': namespace,
        '${LABEL}': label,
        '${SELECTORKEY}': selectorkey,
      },
      'l0.daemonset-create.e2e-spec',
    );

    this.podcount = JSON.parse(
      CommonKubectl.execKubectlCommand(`kubectl get node -o json`),
    ).items.length;
    daemonsetPage.navigateTo();
  });

  afterAll(() => {
    // 删除测试数据
    CommonKubectl.execKubectlCommand(`kubectl delete namespaces ${namespace}`);
  });

  it('L0:AldK8S-150: 单击左导航计算／守护进程集 -- 进入守护进程集列表页--单击创建守护进程集按钮--进入创建页--输入合法yaml, 单击创建按钮--验证创建成功', () => {
    daemonsetPage.getButtonByText('创建守护进程集').click();

    daemonsetPage.selectText('YAML');
    daemonsetPage.yamlCreator.clickToolbarByName('清空');

    // 写入yaml
    daemonsetPage.yamlCreator.setYamlValue(this.testdata.get('data'));
    daemonsetPage.yamlCreator.clickButtonCreate();

    // 验证创建成功后，有成功的toast 提示
    // expect(daemonsetPage.toastsuccess.message.isPresent()).toBeTruthy();

    // expect(daemonsetPage.toastsuccess.getText()).toContain('创建成功');
    expect(daemonsetPage.getButtonByText('创建').isPresent()).toBeFalsy();
    daemonsetPage.navigateTo();

    daemonsetPage.namespace.select(namespace);
    CommonPage.waitElementPresent(
      daemonsetPage.resourceTable.getCell('名称', [name]),
    );
    daemonsetPage.resourceTable.getCell('状态', [name]).then(elem => {
      CommonPage.waitElementTextChangeTo(
        elem,
        `${this.podcount} / ${this.podcount}`,
      );
      expect(elem.getText()).toBe(`${this.podcount} / ${this.podcount}`);
    });
  });
});
