/**
 * Created by liuwei on 2018/5/14.
 */

import { DaemonsetPage } from '../../page_objects/computer/daemonset.page';
import { CommonApi } from '../../utility/common.api';
import { CommonKubectl } from '../../utility/common.kubectl';
import { CommonMethod } from '../../utility/common.method';
import { CommonPage } from '../../utility/common.page';
import { k8s_type_daemonsets } from '../../utility/resource.type.k8s';

describe('L1:守护进程集更新功能', () => {
  const daemonsetPage = new DaemonsetPage();
  const name = CommonMethod.random_generate_testData();
  const namespace = CommonMethod.random_generate_testData();
  const label = CommonMethod.random_generate_testData();
  const selectorkey = CommonMethod.random_generate_testData();

  const label_newkey = CommonMethod.random_generate_testData();
  const label_newvalue = CommonMethod.random_generate_testData();
  const annotations_newkey = CommonMethod.random_generate_testData();
  const annotations_newvalue = CommonMethod.random_generate_testData();

  // 根据【名称，命名空间， 类型】唯一确定资源列表的一行
  const rowkey = [name];

  beforeAll(() => {
    // 创建一个 ReplicaSets
    this.testdata = CommonKubectl.createResource(
      'daemonset.yaml',
      {
        '${NAME}': name,
        '${NAMESPACE}': namespace,
        '${LABEL}': label,
        '${SELECTORKEY}': selectorkey,
      },
      'l1.daemonset-details.e2e-spec',
    );

    this.podcount = JSON.parse(
      CommonKubectl.execKubectlCommand(`kubectl get node -o json`),
    ).items.length;

    daemonsetPage.navigateTo();
    // 选择新创建的namespace
    daemonsetPage.namespace.select(namespace);
  });

  afterAll(() => {
    // 删除测试数据
    CommonKubectl.execKubectlCommand(`kubectl delete namespaces ${namespace}`);
  });

  it('L1:AldK8S-151: 单击左导航计算／守护进程集 -- 进入守护进程集列表页--单击一个副本集名称--进入详情页-- 单击YAML tab--验证YAML 显示正确', () => {
    daemonsetPage.resourceTable.clickResourceNameByRow(rowkey);
    daemonsetPage.resourceBasicInfo.resourceInfoTab.clickTabByText('YAML');
    daemonsetPage.detailPage.yamlEditor.getYamlValue().then(function(textyaml) {
      expect(CommonMethod.parseYaml(textyaml).metadata.name).toBe(name);
    });
  });

  it('L1:AldK8S-152: 单击左导航计算／守护进程集 -- 进入守护进程集列表页--单击一个副本集名称--进入详情页-- 单击配置管理 tab--验证配置管理 显示正确', () => {
    daemonsetPage.resourceBasicInfo.resourceInfoTab.clickTabByText('配置管理');

    // 验证Pod 详情页【配置管理】表格部分表头显示正确
    expect(daemonsetPage.detailPage.configmapList.getHeaderText()).toEqual([
      '名称',
      '值',
    ]);

    daemonsetPage.detailPage.configmapList
      .getCell('值', rowkey)
      .then(function(elem) {
        expect(elem.getText()).toContain(name);
      });
  });

  it('L1:AldK8S-153: 单击左导航计算／守护进程集 -- 进入守护进程集列表页--单击一个副本集名称--进入详情页-- 单击事件 tab--验证事件 显示正确', () => {
    daemonsetPage.resourceBasicInfo.resourceInfoTab.clickTabByText('事件');

    // 验证Pod 详情页【事件】表格部分表头显示正确
    expect(daemonsetPage.detailPage.eventList.getHeaderText()).toEqual([
      '消息',
      '来源',
      '子对象',
      '总数',
      '首次出现时间',
      '最近出现时间',
    ]);

    // 验证事件的数量大于0
    daemonsetPage.detailPage.eventList.getRowCount().then(rowcount => {
      expect(rowcount).toBeGreaterThan(0);
    });
  });

  it('L1:AldK8S-154: 单击左导航计算／守护进程集 -- 进入守护进程集列表页--单击一个副本集名称--进入详情页-- 单击日志 tab--验证日志 显示正确', async () => {
    daemonsetPage.resourceBasicInfo.resourceInfoTab.clickTabByText('日志');
    daemonsetPage.resourceBasicInfo.logviewer.getToolbarButton('查找').click();
    daemonsetPage.resourceBasicInfo.logviewer.waitLogDisplay();
    daemonsetPage.resourceBasicInfo.logviewer.finder_inputbox.input('hehe');
    daemonsetPage.resourceBasicInfo.logviewer.resultfinder
      .getText()
      .then(result => {
        expect(result.includes('无结果')).toBeFalsy();
      });
  });

  it('L1:AldK8S-155: 单击左导航计算／守护进程集 -- 进入守护进程集列表页--单击一个副本集名称--进入详情页--验证显示信息正确', () => {
    daemonsetPage.clickLeftNavByText('守护进程集');
    daemonsetPage.resourceTable.getCell('状态', rowkey).then(elem => {
      CommonPage.waitElementTextChangeTo(
        elem,
        `${this.podcount} / ${this.podcount}`,
      );
    });
    daemonsetPage.resourceTable.clickResourceNameByRow(rowkey);
    const resourceJson = CommonApi.getResourceJSON(
      name,
      k8s_type_daemonsets,
      namespace,
    );

    // 验证名称显示正确
    expect(
      daemonsetPage.resourceBasicInfo.basicInfo
        .getElementByText('名称')
        .getText(),
    ).toBe(resourceJson.metadata.name);

    // 验证命名空间显示正确
    expect(
      daemonsetPage.resourceBasicInfo.basicInfo
        .getElementByText('命名空间')
        .getText(),
    ).toBe(resourceJson.metadata.namespace);

    // 验证标签显示正确
    const rslabes = Object.keys(resourceJson.metadata.labels);
    for (const key of rslabes) {
      expect(
        daemonsetPage.resourceBasicInfo.basicInfo
          .getElementByText('标签')
          .getText(),
      ).toContain(resourceJson.metadata.labels[key]);
    }

    // 验证注解显示正确
    const rsannotations = Object.keys(resourceJson.metadata.annotations);
    for (const key of rsannotations) {
      expect(
        daemonsetPage.resourceBasicInfo.basicInfo
          .getElementByText('注解')
          .getText(),
      ).toContain(resourceJson.metadata.annotations[key]);
    }

    // 验证镜像显示正确
    expect(
      daemonsetPage.resourceBasicInfo.basicInfo
        .getElementByText('镜像')
        .getText(),
    ).toBe(resourceJson.spec.template.spec.containers[0].image);

    // 验证选择器显示正确
    const matchLabelsKey = Object.keys(resourceJson.spec.selector.matchLabels);
    for (const key of matchLabelsKey) {
      expect(
        daemonsetPage.resourceBasicInfo.basicInfo
          .getElementByText('选择器')
          .getText(),
      ).toContain(resourceJson.spec.selector.matchLabels[key]);
    }

    // 验证状态显示正确
    expect(
      daemonsetPage.resourceBasicInfo.basicInfo
        .getElementByText('状态')
        .getText(),
    ).toBe(`${this.podcount} / ${this.podcount}`);

    // 验证创建时间正确
    daemonsetPage.resourceBasicInfo.basicInfo
      .getElementByText('创建时间')
      .getText()
      .then(ctrateTimeStr => {
        expect(new Date(ctrateTimeStr).getTime()).toBe(
          new Date(resourceJson.metadata.creationTimestamp).getTime(),
        );
      });

    expect(daemonsetPage.detailPage.podList.getRowCount()).toBe(this.podcount);

    daemonsetPage.detailPage.podList
      .getColumeTextByName('名称')
      .then(nameList => {
        for (const containersName of nameList) {
          expect(containersName).toContain(name);
        }
      });

    expect(daemonsetPage.detailPage.serviceList.getRowCount()).toBe(1);

    daemonsetPage.detailPage.serviceList
      .getColumeTextByName('名称')
      .then(nameList => {
        for (const containersName of nameList) {
          expect(containersName).toContain(name);
        }
      });
  });

  it('L1:AldK8S-156: 单击左导航计算／守护进程集 -- 进入守护进程集列表页--单击一个副本集名称--进入详情页-- 单击操作----选择更新标签--验证更新正确', () => {
    daemonsetPage.operation.select('更新标签');
    CommonPage.waitElementPresent(daemonsetPage.resourcelabel.buttonUpdate);
    daemonsetPage.resourcelabel.newValue(label_newkey, label_newvalue);
    daemonsetPage.resourcelabel.clickUpdate();
    expect(daemonsetPage.toastsuccess.getText()).toBe('更新成功');
    daemonsetPage.resourceBasicInfo.resourceInfoTab.clickTabByText('基本信息');
    // 验证标签修改正确
    daemonsetPage.resourceBasicInfo.basicInfo
      .getElementByText('标签')
      .getText()
      .then(labelText => {
        expect(labelText).toContain(label_newvalue);
      });
  });

  it('L1:AldK8S-157: 单击左导航计算／守护进程集 -- 进入守护进程集列表页--单击一个副本集名称--进入详情页-- 单击操作----选择更新注解--验证更新正确', () => {
    daemonsetPage.operation.select('更新注解');
    CommonPage.waitElementPresent(
      daemonsetPage.resourceAnnotations.buttonUpdate,
    );
    daemonsetPage.resourceAnnotations.newValue(
      annotations_newkey,
      annotations_newvalue,
    );
    daemonsetPage.resourceAnnotations.clickUpdate();
    expect(daemonsetPage.toastsuccess.getText()).toBe('更新成功');
    daemonsetPage.resourceBasicInfo.resourceInfoTab.clickTabByText('事件');
    daemonsetPage.resourceBasicInfo.resourceInfoTab.clickTabByText('基本信息');

    // 验证注解修改正确
    daemonsetPage.resourceBasicInfo.basicInfo
      .getElementByText('注解')
      .$$('.plain-container__label')
      .getText()
      .then(labelText => {
        let isContain = false;
        for (const iterator of labelText) {
          isContain = iterator.includes(annotations_newvalue) || isContain;
        }
        expect(true).toBeTruthy();
      });
  });

  it('L1:AldK8S-158: 单击左导航计算／守护进程集 -- 进入守护进程集列表页--单击一个副本集名称--进入详情页-- 单击操作----选择更新删除--验证删除正确', () => {
    daemonsetPage.operation.select('删除');
    daemonsetPage.confirmDialog.clickConfirm();
    expect(daemonsetPage.toastsuccess.getText()).toBe('删除成功');

    CommonPage.clickLeftNavByText('守护进程集');
    daemonsetPage.namespace.select(namespace);
    expect(daemonsetPage.resourceTable.noResult.getText()).toBe('无守护进程集');
  });
});
