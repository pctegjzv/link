/**
 * Created by liuwei on 2018/3/15.
 */

import { PodPage } from '../../page_objects/computer/pod.page';
import { CommonApi } from '../../utility/common.api';
import { CommonKubectl } from '../../utility/common.kubectl';
import { CommonMethod } from '../../utility/common.method';
import { CommonPage } from '../../utility/common.page';
import { CommonStatus } from '../../utility/common.status';
import { k8s_type_pods } from '../../utility/resource.type.k8s';

describe('L1:Pod 详情页功能验证', () => {
  const podPage = new PodPage();
  const namespace = CommonMethod.random_generate_testData();
  const label = CommonMethod.random_generate_testData();
  const label_newkey = CommonMethod.random_generate_testData();
  const label_newvalue = CommonMethod.random_generate_testData();
  const annotations_newkey = CommonMethod.random_generate_testData();
  const annotations_newvalue = CommonMethod.random_generate_testData();
  const name = CommonMethod.random_generate_testData();

  // 根据【名称，命名空间， 类型】唯一确定资源列表的一行
  const rowkey = [name];

  beforeAll(() => {
    podPage.navigateTo();
    // 创建一个pod
    this.testdatafilename = CommonKubectl.createResource(
      'pod.yaml',
      {
        '${NAME}': name,
        '${NAMESPACE}': namespace,
        '${LABEL}': label,
      },
      'l1.pod-detailinfo.e2e-spec',
    );
    podPage.namespace.select(namespace);
    podPage.resourceTable.getCell('状态', rowkey).then(elem => {
      CommonPage.waitElementTextChangeTo(elem, '运行中');
    });
  });

  afterAll(() => {
    // 删除测试数据
    CommonKubectl.deleteResourceByYmal(this.testdatafilename);
  });

  it('L1:AldK8S-26: 单击左导航计算／容器组 -- 进入容器组的资源列表页--单击一个容器组名--进入详情页--验证概览页的显示信息正确', () => {
    podPage.resourceTable.clickResourceNameByRow(rowkey);
    CommonPage.waitElementPresent(
      podPage.detailPage.resourceInfoTab.getTabElementByText('基本信息'),
    );

    // 验证面包屑显示正确
    expect(podPage.breadcrumb.getText()).toEqual(`计算/容器组/${name}`);

    podPage.resourceBasicInfo.basicInfo.getAllKeyText().then(function(text) {
      // 验证详情页基本信息部分包含那些字段
      expect(CommonMethod.converNodeListTextToArray(text).sort()).toEqual(
        [
          '名称',
          '命名空间',
          '标签',
          '注解',
          '状态',
          'IP',
          'QoS Class',
          '节点',
          '创建者',
          '创建时间',
        ].sort(),
      );
    });

    // 单击名称进入详情页，验证详情页tab 显示正确 '基本信息', 'YAML', '配置字典', '日志', '事件'
    podPage.detailPage.resourceInfoTab.getTabText().then(tabText => {
      expect(tabText).toEqual(['基本信息', 'YAML', '配置管理', '日志', '事件']);
    });

    // 验证详情页基本信息部分字段值是否正确
    const resourceJson = CommonApi.getResourceJSON(
      name,
      k8s_type_pods,
      namespace,
    );

    // 验证Pod 名称正确
    expect(
      podPage.resourceBasicInfo.basicInfo.getElementByText('名称').getText(),
    ).toBe(resourceJson.metadata.name);

    // 验证Pod 命名空间正确
    expect(
      podPage.resourceBasicInfo.basicInfo
        .getElementByText('命名空间')
        .getText(),
    ).toBe(resourceJson.metadata.namespace);

    // 验证Pod 标签正确
    expect(
      podPage.resourceBasicInfo.basicInfo.getElementByText('标签').getText(),
    ).toContain(resourceJson.metadata.labels.app);

    // 验证Pod 注解正确
    expect(
      podPage.resourceBasicInfo.basicInfo.getElementByText('注解').getText(),
    ).toContain(resourceJson.metadata.annotations.alaudatest);

    // 验证Pod 状态正确
    expect(
      podPage.resourceBasicInfo.basicInfo.getElementByText('状态').getText(),
    ).toBe(CommonStatus.getPodStatus(resourceJson.status.phase));

    // 验证Pod IP正确
    expect(
      podPage.resourceBasicInfo.basicInfo.getElementByText('IP').getText(),
    ).toBe(resourceJson.status.podIP || '-');

    // 验证Pod QoS Class正确
    expect(
      podPage.resourceBasicInfo.basicInfo
        .getElementByText('QoS Class')
        .getText(),
    ).toBe(resourceJson.status.qosClass);

    // 验证Pod 节点正确
    expect(
      podPage.resourceBasicInfo.basicInfo.getElementByText('节点').getText(),
    ).toBe(resourceJson.spec.nodeName || '-');

    // 验证Pod 创建者为空
    expect(
      podPage.resourceBasicInfo.basicInfo.getElementByText('创建者').getText(),
    ).toBe('-');

    // 验证Pod 创建时间正确
    podPage.resourceBasicInfo.basicInfo
      .getElementByText('创建时间')
      .getText()
      .then(ctrateTimeStr => {
        // 这有bug 页面的显示时间与创建时间相差了12个小时
        // expect(new Date(ctrateTimeStr).getTime()).toBe(new Date(resourceJson.metadata.creationTimestamp).getTime());
      });

    // 验证Pod 详情页【容器】表格部分表头显示正确
    expect(podPage.detailPage.container_list.getHeaderText()).toEqual([
      '名称',
      '镜像',
      '环境变量',
      '执行命令',
      '执行参数',
    ]);

    // 验证Pod 详情页【容器】表格部分名名，镜像显示正确
    podPage.detailPage.container_list.getCell('名称', ['nginx']).then(elem => {
      elem.getText().then(text => {
        expect(text).toBe(resourceJson.spec.containers[0].name);
      });
    });

    podPage.detailPage.container_list
      .getCell('镜像', ['index.alauda.cn/alaudaorg/qaimages:volumetest'])
      .then(elem => {
        elem.getText().then(text => {
          expect(text).toBe(resourceJson.spec.containers[0].image);
        });
      });

    // 验证Pod 详情页【现状】部分显示正确
    expect(podPage.detailPage.conditionList.getHeaderText()).toEqual([
      '类型',
      '状态',
      '最后心跳时间',
      '最后更改时间',
      '原因',
      '消息',
    ]);

    // 验证Pod 详情页【PVC】部分显示正确
    expect(podPage.detailPage.pvcList.getHeaderText()).toEqual([
      '名称',
      '状态',
      '关联持久卷',
      '大小',
      '创建时间',
      '',
    ]);
  });

  it('L1:AldK8S-27: 单击左导航计算／容器组 -- 进入容器组的资源列表页--单击一个容器组名--进入详情页--单击YAML tab--验证YAML显示正确', () => {
    podPage.detailPage.resourceInfoTab.clickTabByText('YAML');
    podPage.detailPage.yamlEditor.getYamlValue().then(function(textyaml) {
      expect(CommonMethod.parseYaml(textyaml).metadata.name).toBe(name);
    });
  });

  it('L1:AldK8S-29: 单击左导航计算／容器组 -- 进入容器组的资源列表页--单击一个容器组名--进入详情页--单击配置字典 tab--验证配置管理显示正确', () => {
    podPage.detailPage.resourceInfoTab.clickTabByText('配置管理');
    // 验证Pod 详情页【配置管理】表格部分表头显示正确
    expect(podPage.detailPage.configmapList.getHeaderText()).toEqual([
      '名称',
      '值',
    ]);
    podPage.detailPage.configmapList.getCell('值', rowkey).then(function(elem) {
      expect(elem.getText()).toContain(name);
    });
  });

  it('L1:AldK8S-36: 单击左导航计算／容器组 -- 进入容器组的资源列表页--单击一个容器组名--进入详情页--单击事件tab--验证事件显示正确', () => {
    podPage.detailPage.resourceInfoTab.clickTabByText('事件');

    // 验证Pod 详情页【事件】表格部分表头显示正确
    expect(podPage.detailPage.eventList.getHeaderText()).toEqual([
      '消息',
      '来源',
      '子对象',
      '总数',
      '首次出现时间',
      '最近出现时间',
    ]);

    // 验证事件的数量大于0
    podPage.detailPage.eventList.getRowCount().then(rowcount => {
      expect(rowcount).toBeGreaterThan(0);
    });
  });

  it('L1:AldK8S-37: 单击左导航计算／容器组 -- 进入容器组的资源列表页--单击操作列--选择更新标签--输入合法值--验证更新标签正确', () => {
    podPage.operation.select('更新标签');
    CommonPage.waitElementPresent(podPage.resourcelabel.buttonUpdate);
    podPage.resourcelabel.newValue(label_newkey, label_newvalue);
    podPage.resourcelabel.clickUpdate();
    expect(podPage.toastsuccess.getText()).toBe('更新成功');
    podPage.detailPage.resourceInfoTab.clickTabByText('基本信息');
    // 验证标签修改正确
    podPage.resourceBasicInfo.basicInfo
      .getElementByText('标签')
      .getText()
      .then(function(labelText) {
        expect(labelText).toContain(label_newvalue);
      });
  });

  it('L1:AldK8S-38: 单击左导航计算／容器组 -- 进入容器组的资源列表页--单击一个容器组名--进入详情页--单击操作--选择更新注解--输入合法值--验证更新注解正确', () => {
    podPage.operation.select('更新注解');
    CommonPage.waitElementPresent(podPage.resourceAnnotations.buttonUpdate);
    podPage.resourceAnnotations.newValue(
      annotations_newkey,
      annotations_newvalue,
    );
    podPage.resourceAnnotations.clickUpdate();
    expect(podPage.toastsuccess.getText()).toBe('更新成功');
    podPage.detailPage.resourceInfoTab.clickTabByText('事件');
    podPage.detailPage.resourceInfoTab.clickTabByText('基本信息');
    // 验证注解修改正确
    podPage.resourceBasicInfo.basicInfo
      .getElementByText('注解')
      .getText()
      .then(function(labelText) {
        expect(labelText).toContain(annotations_newvalue);
      });
  });

  it('L1:AldK8S-39: 单击左导航计算／容器组 -- 进入容器组的资源列表页--单击操作列--选择删除标签--验证删除确认功能', () => {
    podPage.operation.select('删除');
    podPage.confirmDialog.clickConfirm();
    expect(podPage.toastsuccess.getText()).toBe('删除成功');

    CommonPage.clickLeftNavByText('容器组');
    podPage.namespace.select(namespace);
    CommonPage.waitElementPresent(podPage.resourceTable.noResult);
    expect(podPage.resourceTable.noResult.getText()).toBe('无容器组');
  });
});
