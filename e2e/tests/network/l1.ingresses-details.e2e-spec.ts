import { browser } from 'protractor';

import { IngressesPage } from '../../page_objects/network/ingresses.page';
import { CommonKubectl } from '../../utility/common.kubectl';
import { CommonMethod } from '../../utility/common.method';
import { CommonPage } from '../../utility/common.page';

describe('L1:访问权详情', () => {
  const ingressPage = new IngressesPage();
  const name = CommonMethod.random_generate_testData();
  const sname = CommonMethod.random_generate_testData();
  // 根据【名称，命名空间， 类型】唯一确定资源列表的一行
  const rowkey = [name];
  const host = name + 'alauda.cn';
  const tslhost = 'tsl' + name + 'alauda.cn';
  beforeAll(() => {
    this.testdata = CommonKubectl.createResource(
      'namespace.yaml',
      {
        '${NAME}': name,
        '${LABEL}': name,
      },
      'l1.ingresses-detailsnamespace.e2e-spec',
    );
    this.testdata = CommonKubectl.createResource(
      'ingress.yaml',
      {
        '${NAME}': name,
        '${TSLHOST}': tslhost,
        '${HOST}': host,
        '${SNAME}': sname,
      },
      'l1.ingresses-details.e2e-spec',
    );
    ingressPage.navigateTo();
  });
  afterAll(() => {
    // 清除测试数据
    CommonKubectl.execKubectlCommand(`kubectl delete namespaces ${name}`);
  });

  it('AldK8S-269:L1:单击左导航网络 / 访问权 -- 点击一个访问权名称 -- 验证详情页面基本信息访问权规则tsl证书', () => {
    // 进入详情页面
    ingressPage.resourceTable.clickResourceNameByRow(rowkey);
    ingressPage.resourceBasicInfo.basicInfo.getAllKeyText().then(text => {
      expect(text).toEqual([
        '名称',
        '命名空间',
        '标签',
        '注解',
        '默认服务',
        '创建时间',
      ]);
    });
    ingressPage.resourceBasicInfo.resourceInfoTab.getTabText().then(text => {
      expect(text).toEqual(['基本信息', 'YAML', '事件']);
    });
    ingressPage.resourceTable.getHeaderText().then(text => {
      expect(text).toEqual(['域名', '规则', '保密字典', '域名']);
    });
    ingressPage.RuleTable.getHeaderText().then(text => {
      expect(text).toEqual(['路径', '服务', '端口']);
    });
    ingressPage.RuleTable.getCell('端口', [8090]).then(elem => {
      elem.getText().then(text => {
        expect(text).toEqual('8090');
      });
    });
  });
  it('AldK8S-264:L1:单击左导航网络/访问权--点击一个访问权名称--点击右上角更新标签--更新访问权标签--验证标签更新成功', () => {
    ingressPage.operation.select('更新标签');
    ingressPage.resourcelabel.newValue('autotestupdate', 'autotestupdatelabel');
    ingressPage.resourcelabel.clickUpdate();
    // 验证标签修改正确
    ingressPage.resourceBasicInfo.basicInfo
      .getElementByText('标签')
      .getText()
      .then(function(labelText) {
        expect(labelText).toContain('autotestupdatelabel');
      });
  });
  it('AldK8S-265:L1:单击左导航网络/访问权--点击一个访问权名称--点击右上角更新注解--更新访问权注解--验证注解更新成功', () => {
    ingressPage.operation.select('更新注解');
    ingressPage.resourcelabel.newValue('autotestupdate', 'autotestupdate');
    ingressPage.resourcelabel.clickUpdate();
    // 验证标签修改正确
    ingressPage.resourceBasicInfo.basicInfo
      .getElementByText('注解')
      .getText()
      .then(function(labelText) {
        expect(labelText).toContain('autotestupdate');
      });
  });
  it('AldK8S-267:L1:单击左导航网络 / 访问权 -- 点击访问权名称 -- 点击标签后的蓝笔-- 添加一个标签 -- 验证标签添加成功', () => {
    ingressPage.resourceBasicInfo.basicButtion.getElementByText('标签').click();
    ingressPage.resourcelabel.newValue(
      'autotestupdate1',
      'autotestupdatelabel1',
    );
    ingressPage.resourcelabel.clickUpdate();
    // 验证标签修改正确
    ingressPage.resourceBasicInfo.basicInfo
      .getElementByText('标签')
      .getText()
      .then(function(labelText) {
        expect(labelText).toContain('autotestupdatelabel1');
      });
  });
  it('AldK8S-268:L1:单击左导航网络 / 访问权 -- 点击访问权名称 -- 点击注解后的蓝笔-- 添加一个注解-- 验证注解添加成功', () => {
    ingressPage.resourceBasicInfo.basicButtion.getElementByText('注解').click();
    ingressPage.resourcelabel.newValue('autotestupdate1', 'autotestupdate1');
    ingressPage.resourcelabel.clickUpdate();
    // 验证标签修改正确
    ingressPage.resourceBasicInfo.basicInfo
      .getElementByText('注解')
      .getText()
      .then(function(labelText) {
        expect(labelText).toContain('autotestupdate1');
      });
  });
  it('AldK8S-263:L1:单击左导航网络 / 访问权 -- 点击一个访问权名称 -- 点击右上角更新 -- 更新访问权 -- 验证更新成功', () => {
    ingressPage.operation.select('更新');
    CommonPage.waitElementPresent(ingressPage.yamlCreator.buttonCancel);
    ingressPage.yamlCreator.getYamlValue().then(yaml => {
      ingressPage.yamlCreator.setYamlValue(
        String(yaml).replace(/resourceVersion: '(.*)'/g, ''),
      );
    });
    ingressPage.yamlCreator.getYamlValue().then(yaml => {
      ingressPage.yamlCreator
        .setYamlValue(
          String(yaml).replace('servicePort: 8090', 'servicePort: 18081'),
        )
        .then(() => {
          ingressPage.yamlUpdateButton.click();
          browser.sleep(1000);
          ingressPage.yamlUpdateButton.isPresent().then(isPresent => {
            if (isPresent) {
              ingressPage.yamlUpdateButton.click();
            }
          });
        });
      // expect(ingressPage.toastsuccess.getText()).toBe('更新成功');
    });
    ingressPage.navigateTo();

    ingressPage.resourceTable.clickResourceNameByRow(rowkey);

    ingressPage.RuleTable.getCell('端口', [18081]).then(elem => {
      elem.getText().then(text => {
        expect(text).toEqual('18081');
      });
    });
  });
  it('AldK8S-266:L1:单击左导航网络/访问权--点击一个访问权名称--点击右上角删除--确认删除--验证删除成功', () => {
    ingressPage.operation.select('删除');
    ingressPage.confirmDialog.clickConfirm();
    CommonPage.waitElementNotPresent(ingressPage.toastsuccess.message);
    ingressPage.searchBox.search(name);
    expect(ingressPage.resourceTable.noResult.getText()).toBe('无访问权');
  });
});
