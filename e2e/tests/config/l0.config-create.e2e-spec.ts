/**
 * Created by liuwei on 2018/4/25.
 */

import { ConfigPage } from '../../page_objects/config/config.page';
import { CommonKubectl } from '../../utility/common.kubectl';
import { CommonMethod } from '../../utility/common.method';

describe('L0:配置字典创建功能', () => {
  const configPage = new ConfigPage();
  const namespace = CommonMethod.random_generate_testData();
  const name = CommonMethod.random_generate_testData();
  // 根据【名称，命名空间， 类型】唯一确定资源列表的一行
  const rowkey = [name];

  beforeAll(() => {
    configPage.navigateTo();
    CommonKubectl.execKubectlCommand(`kubectl create namespace ${namespace}`);
    configPage.namespace.select(namespace);
  });

  afterAll(() => {
    // 删除测试数据
    CommonKubectl.execKubectlCommand(`kubectl delete namespace ${namespace}`);
  });

  it('L0:AldK8S-127: 单击左导航配置／配置字典 -- 进入配置字典列表页--单击创建配置字典--输入yaml 样例，修改名称，命名空间，单击创建按钮--验证创建成功', () => {
    configPage.clickCreateButton();
    configPage.selectText('YAML');
    configPage.yamlCreator.getYamlValue().then((yaml: string) => {
      configPage.yamlCreator.clickToolbarByName('清空');
      const yamlNew = yaml.replace(
        CommonMethod.parseYaml(yaml).metadata.name,
        name,
      );
      configPage.yamlCreator.setYamlValue(
        yamlNew.replace(
          CommonMethod.parseYaml(yamlNew).metadata.namespace,
          namespace,
        ),
      );
      configPage.yamlCreator.clickButtonCreate();
      // expect(configPage.toastsuccess.getText()).toContain('创建成功');
    });
    // 验证configmap 创建成功
    configPage.navigateTo();

    configPage.searchBox.search(name);
    expect(configPage.resourceTable.getRowCount()).toBe(1);
    configPage.resourceTable.getCell('名称', rowkey).then(cell => {
      expect(cell.$('a[href*=config]').getText()).toBe(name);
    });
  });
});
