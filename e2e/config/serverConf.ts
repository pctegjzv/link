/**
 * 管理配置文件
 * Created by liuwei on 2018/3/2.
 */

const conf = {
  TESTDATAPATH: process.cwd() + '/e2e/test_data/',
};

export let ServerConf = conf;
