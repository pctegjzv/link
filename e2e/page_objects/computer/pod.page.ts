/**
 * 其他资源列表的页面类
 * Created by liuwei on 2018/3/1.
 *
 */
import { CommonPage } from '../../utility/common.page';
import { ResourcePage } from '../commonpage/resource.common.page';

export class PodPage extends ResourcePage {
  navigateTo() {
    // super.navigateTo();
    CommonPage.clickLeftNavByText('容器组');
    CommonPage.waitProgressbarNotDisplay();
    this.namespace.select('所有命名空间');
    CommonPage.waitProgressbarNotDisplay();
  }
}
