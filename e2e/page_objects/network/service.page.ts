/**
 * 部署列表的页面类
 * Created by liuwei on 2018/3/19.
 *
 */
import { by } from 'protractor';

import { CommonPage } from '../../utility/common.page';
import { ResourcePage } from '../commonpage/resource.common.page';
import { KeyValueControl } from '../component/key_value.component';
import { TableComponent } from '../component/table.component';

export class ServicePage extends ResourcePage {
  navigateTo() {
    this.clickLeftNavByText('服务');
    CommonPage.waitProgressbarNotDisplay();
    this.namespace.select('所有命名空间');
    CommonPage.waitProgressbarNotDisplay();
  }

  /**
   * 详情页的容器表格信息
   */
  get detailInfo_endpoint() {
    return new TableComponent('alk-endpoint-list');
  }

  /**
   * 更新选择器dialog 页
   */
  get resourceSelector() {
    return new KeyValueControl(
      by.xpath('//div[@class="aui-dialog-title"]'),
      by.tagName('aui-dialog-content'),
      by.tagName('aui-dialog-footer'),
    );
  }
}
