import { AlaudaTable } from '../../element_objects/alauda.table';
import { CommonPage } from '../../utility/common.page';
import { ResourcePage } from '../commonpage/resource.common.page';
import { TableComponent } from '../component/table.component';

export class IngressesPage extends ResourcePage {
  navigateTo() {
    // super.navigateTo();
    CommonPage.clickLeftNavByText('访问权');
    CommonPage.waitProgressbarNotDisplay();
  }

  get RuleTable() {
    return new AlaudaTable('table th', 'table td', 'table tr');
  }
}
