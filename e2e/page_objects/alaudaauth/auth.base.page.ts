/**
 * Created by changdongmei on 2018/3/26.
 */

import { AlaudaBasicInfo } from '../../element_objects/alauda.basicinfo';
import { AlaudaElement } from '../../element_objects/alauda.element';
import { SearchBox } from '../component/search.component';
import { TableComponent } from '../component/table.component';
import { PageBase } from '../page.base';

export class AuthPageBase extends PageBase {
  /**
   * 获取成员列表页面的成员表
   * 点击左导航到角色页面
   */
  navigateTo() {
    super.clickLeftNavByText('角色');
  }
  /**
   * 获列表页面的成员表
   */
  get authTable() {
    return new TableComponent();
  }
  /**
   * 获取搜索框
   */
  get searchBox() {
    return new SearchBox();
  }

  /**
   * 资源详情页的基本信息部分
   */
  get basicInfo(): AlaudaBasicInfo {
    return new AlaudaBasicInfo('基本信息', 'aui-card', '.aui-card__header h2');
  }

  /**
   * 重写父类属性，用于getElementByText 方法定位元素
   */
  get alaudaElement(): AlaudaElement {
    return new AlaudaElement(
      'aui-form-item .aui-form-item',
      '.aui-form-item__label',
      '.aui-form-item__content',
    );
  }
}
