/**
 * 部署列表的页面类
 * Created by liuwei on 2018/3/19.
 *
 */
import { CommonPage } from '../../utility/common.page';
import { ResourcePage } from '../commonpage/resource.common.page';

export class ConfigPage extends ResourcePage {
  navigateTo() {
    this.clickLeftNavByText('配置字典');
    CommonPage.waitProgressbarNotDisplay();
    this.namespace.select('所有命名空间');
    CommonPage.waitProgressbarNotDisplay();
  }

  /**
   * 单击创建配置字典按钮
   */
  clickCreateButton() {
    this.getButtonByText('创建配置字典').click();
    CommonPage.waitElementPresent(this.yamlCreator.buttonCreate);
  }
}
