/**
 * PVC列表的页面类
 * Created by liuwei on 2018/5/23.
 *
 */

import { CommonPage } from '../../utility/common.page';
import { ResourcePage } from '../commonpage/resource.common.page';

export class PvcPage extends ResourcePage {
  navigateTo() {
    CommonPage.clickLeftNavByText('持久卷声明');
    CommonPage.waitProgressbarNotDisplay();
  }
}
