/**
 * Storage Class列表的页面类
 * Created by liuwei on 2018/5/26.
 *
 */

import { CommonPage } from '../../utility/common.page';
import { ResourcePage } from '../commonpage/resource.common.page';

export class StorageClassPage extends ResourcePage {
  navigateTo() {
    CommonPage.clickLeftNavByText('存储类');
    CommonPage.waitProgressbarNotDisplay();
  }
}
