/**
 * 其他资源列表的页面类
 * Created by liuwei on 2018/3/1.
 *
 */
import { by } from 'protractor';

import { CommonPage } from '../../utility/common.page';
import { ResourcePage } from '../commonpage/resource.common.page';
import { YamlComponent } from '../component/yaml.component';

export class OtherResourcePage extends ResourcePage {
  navigateTo() {
    CommonPage.clickLeftNavByText('命名空间相关资源');
    CommonPage.waitProgressbarNotDisplay();
  }

  /**
   * 通过yaml 创建资源的dialog 页面
   */
  get yamlCreator() {
    return new YamlComponent(by.css('aui-dialog .aui-dialog'));
  }
}
