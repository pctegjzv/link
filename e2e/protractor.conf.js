// Protractor configuration file, see link for more information
// https://github.com/angular/protractor/blob/master/lib/config.ts

const { SpecReporter } = require('jasmine-spec-reporter');

// JUnit style XML reports
const { JUnitXmlReporter } = require('jasmine-reporters');
var Recycler = require('./utility/clear.testdata');
var retry = require('protractor-retry').retry;
var HtmlReporter = require('protractor-beautiful-reporter');

exports.config = {
  allScriptsTimeout: 11000,
  specs: ['./**/*.e2e-spec.ts'],
  suites: {
    smoketest: './**/l0.*.e2e-spec.ts',
    sanitytest: ['./**/l0.*.e2e-spec.ts', './**/l1.*.e2e-spec.ts'],
    fulltest: [
      './**/l0.*.e2e-spec.ts',
      './**/l1.*.e2e-spec.ts',
      './**/l2.*.e2e-spec.ts',
      './**/l3.*.e2e-spec.ts',
    ],
  },
  capabilities: {
    browserName: 'chrome',
    shardTestFiles: true,
    maxInstances: 2,
  },
  verboseMultiSessions: true,
  directConnect: true,
  baseUrl: process.env.BASE_URL || 'http://localhost:4200/',
  framework: 'jasmine',
  jasmineNodeOpts: {
    showColors: true,
    defaultTimeoutInterval: 1800000,
    print: function() {},
  },

  onPrepare() {
    console.log(
      '\n\n*********************UI Auto must set Env*******************************',
    );
    console.log(
      `*        BASE_URL: ${process.env.BASE_URL || 'http://localhost:4200/'}`,
    );
    console.log(
      `*           IMAGE: ${process.env.IMAGE ||
        'index.alauda.cn/alaudaorg/qaimages:volumetest'}`,
    );
    console.log(`*      USER_TOKEN: ${process.env.USER_TOKEN}`);
    console.log(
      '************************************************************************\n\n',
    );
    require('ts-node').register({
      project: 'e2e/tsconfig.json',
    });
    jasmine
      .getEnv()
      .addReporter(new SpecReporter({ spec: { displayStacktrace: true } }));
    jasmine.getEnv().addReporter(
      new JUnitXmlReporter({
        savePath: './e2e-reports',
        consolidateAll: true,
        consolidate: true,
      }),
    );

    jasmine.getEnv().addReporter(
      new HtmlReporter({
        baseDirectory: './e2e-reports',
        screenshotsSubfolder: 'images',
        takeScreenShotsOnlyForFailedSpecs: true,
        docTitle: '[LINK] Dashboard UI Auto Test reporter',
      }).getJasmine2Reporter(),
    );

    retry.onPrepare();

    if (Recycler.TestData.isOpenOIDC()) {
      console.log(
        `${browser.baseUrl}/?locale=zh&id_token=${process.env.USER_TOKEN}`,
      );
      browser.get(
        `${browser.baseUrl}/?locale=zh&id_token=${process.env.USER_TOKEN}`,
      );
    } else {
      browser.get(`${browser.baseUrl}/?locale=zh`);
      browser.sleep(1000);
    }

    browser.waitForAngularEnabled(false);
  },

  onCleanUp(results) {
    retry.onCleanUp(results);
  },

  beforeLaunch() {
    //清理资源
    try {
      Recycler.TestData.clear();
    } catch (err) {
      // console.warn(err);
    }
  },

  afterLaunch() {
    //清理资源
    try {
      Recycler.TestData.clear();
    } catch (err) {
      // console.warn(err);
    }
    return retry.afterLaunch(1);
  },
};
