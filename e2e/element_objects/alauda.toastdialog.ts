/**
 * toast 控件
 * Created by liuwei on 2018/4/10.
 */

import { browser, element } from 'protractor';

import { CommonPage } from '../utility/common.page';

export class AlaudaToast {
  private _toast_selector;

  constructor(selector) {
    this._toast_selector = selector;
  }

  get message() {
    return element(this._toast_selector);
  }

  /**
   * 获取toast控件的message文本值
   */
  getText() {
    CommonPage.waitElementPresent(this.message);
    const text = this.message.getText();
    CommonPage.waitElementNotPresent(this.message);
    return text;
  }
}
