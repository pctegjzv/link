/**
 * 面包屑
 * Created by liuwei on 2018/3/15.
 */

import { browser, by, element } from 'protractor';

export class AlaudaBreadCrumb {
  private _breadcrumb_item_selector;

  /**
   * 构造函数
   * @param breadcrumb_item_selector 获得每个面包屑item的selector
   */
  constructor(breadcrumb_item_selector = by.css('alk-breadcrumb')) {
    this._breadcrumb_item_selector = breadcrumb_item_selector;
  }

  /**
   * 获得面包屑的文本
   */
  getText() {
    return element(this._breadcrumb_item_selector)
      .getText()
      .then(res => res.replace(/\s/g, ''));
  }
}
