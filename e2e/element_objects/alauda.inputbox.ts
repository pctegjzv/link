/**
 * 文本框控件
 * Created by liuwei on 2018/2/22.
 */

import { browser, element } from 'protractor';

export class AlaudaInputbox {
  private _inputBox_selector;

  constructor(selector) {
    this._inputBox_selector = selector;
  }

  get inputBox() {
    return element(this._inputBox_selector);
  }

  /**
   * 在文本框中输入一个值
   *
   * @parameter {inputValue} 要输入的值
   */
  input(inputValue) {
    this.inputBox.clear();
    this.inputBox.sendKeys(inputValue);

    return browser.sleep(100);
  }

  /**
   * 获取文本框的值
   */
  getText() {
    return this.inputBox.getText();
  }
}
