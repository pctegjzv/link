/**
 *  单选框控件
 * Created by changdongmei on 2018/4/12.
 */

import { $, $$, browser, by, element } from 'protractor';

export class AlaudaRadio {
  private _radioinputSelector;

  constructor(radio_item_selector = 'aui-radio .aui-radio') {
    this._radioinputSelector = radio_item_selector;
  }
  /**
   * 返回一个 radio
   */
  get radio() {
    return $(this._radioinputSelector);
  }
  /**
   * 获取单选框
   * @parameter {name}  单选框后面的文字， string 类型
   */
  getRadioByName(name) {
    return $$(this._radioinputSelector)
      .filter(elem => {
        return elem
          .$('span')
          .getText()
          .then(text => {
            return text === name;
          });
      })
      .first();
  }
  /**
   * 获取单选框
   * @parameter {name}  单选框后面的文字， string 类型
   */
  getSelectbyName(name) {
    return $$(this._radioinputSelector)
      .filter(elem => {
        return elem.getText().then(text => {
          return text === name;
        });
      })
      .first();
  }

  /**
   * 根据文本返回是否被选中
   * @param name 后的文字
   */
  isChecked() {
    return element(by.css('aui-radio-button .isChecked'));
  }

  check(name) {
    this.getRadioByName(name).click();
  }
}
