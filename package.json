{
  "name": "link",
  "version": "0.5.0",
  "license": "MIT",
  "scripts": {
    "start": "concurrently --kill-others \"yarn start:frontend:hmr\" \"yarn start:backend\"",
    "start:frontend": "ng serve",
    "start:backend": "cd src/backend; make run-dev-no-auth",
    "start:backend-auth": "cd src/backend; make run-dev",
    "start:frontend:hmr": "ng serve --configuration hmr --hmr",
    "start:backend-win": "./scripts/backend-windows.bat",
    "start:prod": "ng build --prod --output-path dist/public/en; yarn build:backend-dev; mv src/backend/dist/backend dist/backend; ./dist/backend /backend --kubeconfig=${HOME}/.kube/config --insecure-port=9999 --enable-anonymous=true",
    "build": "yarn build:frontend; yarn build:backend; mv src/backend/dist/backend dist/backend; cp src/backend/images/Dockerfile dist/Dockerfile",
    "build:frontend": "ng build --prod --base-href /console/ --deploy-url /console/",
    "build:backend": "cd src/backend; make build",
    "build:backend-dev": "cd src/backend; make build-dev",
    "build:frontend-report": "yarn build:frontend && webpack-bundle-analyzer dist/stats.json",
    "test": "yarn jest --coverage",
    "test:watch": "yarn jest --watch",
    "jest": "./node_modules/.bin/jest",
    "lint": "concurrently \"yarn run lint:frontend\" \"yarn run lint:e2e\"",
    "lint:frontend": "tslint -p . -t stylish --fix \"src/frontend/**/*.ts\" && prettier \"src/frontend/**/*.ts\" --write",
    "lint:e2e": "tslint -p . -t stylish --fix \"e2e/**/*.ts\" && prettier \"e2e/**/*.ts\" --write",
    "e2e": "ng e2e --webdriver-update false",
    "e2e:integration": "concurrently --kill-others --success first \"yarn start\" \"wait-on -l http-get://localhost:4200 && yarn e2e:update-webdriver-alternative && yarn e2e:retry\"",
    "e2e:test-only": "ng e2e --webdriver-update false --devServerTarget=''",
    "e2e:image": "ng e2e --webdriver-update false --devServerTarget='' --protractor-config=e2e/protractor.image.conf.js",
    "e2e:update-webdriver": "webdriver-manager update --standalone false --gecko false",
    "e2e:update-webdriver-alternative": "bash ./scripts/webdriver-manager-update.sh",
    "e2e:retry": "bash ./scripts/testRunner.sh",
    "precommit": "lint-staged",
    "// postinstall": "yarn e2e:update-webdriver-alternative",
    "// storybook": "start-storybook -p 9001 -c .storybook"
  },
  "private": true,
  "dependencies": {
    "@angular/animations": "6.1.0",
    "@angular/cdk": "6.4.2",
    "@angular/common": "6.1.0",
    "@angular/compiler": "6.1.0",
    "@angular/core": "6.1.0",
    "@angular/flex-layout": "6.0.0-beta.17",
    "@angular/forms": "^6.1.0",
    "@angular/http": "6.1.0",
    "@angular/material": "6.4.2",
    "@angular/platform-browser": "6.1.0",
    "@angular/platform-browser-dynamic": "6.1.0",
    "@angular/router": "6.1.0",
    "@ngx-translate/core": "^10.0.2",
    "@types/jwt-decode": "^2.2.1",
    "alauda-ui": "^1.5.13",
    "atob": "^2.1.0",
    "copy": "^0.3.1",
    "core-js": "^2.4.1",
    "d3": "^5.1.0",
    "date-fns": "^1.29.0",
    "file-saver": "^1.3.8",
    "jquery": "^3.3.1",
    "js-yaml": "^3.11.0",
    "jwt-decode": "^2.2.0",
    "lodash": "^4.17.4",
    "monaco-editor": "^0.14.3",
    "monaco-languages": "^1.5.1",
    "protractor-beautiful-reporter": "^1.2.3",
    "protractor-retry": "^1.2.0",
    "rxjs": "6.2.2",
    "sockjs-client": "^1.1.4",
    "upath": "1.1.0",
    "web-animations-js": "^2.3.1",
    "xterm": "^3.5.1",
    "zone.js": "^0.8.26"
  },
  "devDependencies": {
    "@angular-devkit/build-angular": "^0.7.2",
    "@angular/cli": "^6.1.2",
    "@angular/compiler-cli": "6.1.0",
    "@angular/language-service": "6.1.0",
    "@angularclass/hmr": "^2.1.3",
    "@types/d3": "^5.0.0",
    "@types/file-saver": "^1.3.0",
    "@types/jasmine": "^2.8.6",
    "@types/jasminewd2": "^2.0.3",
    "@types/jest": "^23.3.1",
    "@types/jquery": "^3.3.5",
    "@types/js-yaml": "^3.11.2",
    "@types/lodash": "^4.14.115",
    "@types/node": "~10.5.4",
    "babel-core": "^6.26.0",
    "codelyzer": "^4.0.1",
    "concurrently": "^3.6.1",
    "husky": "^0.14.3",
    "jasmine-core": "3.1.0",
    "jasmine-reporters": "^2.3.0",
    "jasmine-spec-reporter": "~4.2.1",
    "jest": "^23.5.0",
    "jest-preset-angular": "^6.0.0",
    "jest-sonar-reporter": "^2.0.0",
    "lint-staged": "^7.1.2",
    "prettier": "^1.14.0",
    "protractor": "^5.4.0",
    "ts-node": "^7.0.0",
    "tslint": "^5.11.0",
    "typescript": "2.9.2",
    "wait-on": "^2.1.0",
    "webpack-bundle-analyzer": "^2.13.1"
  },
  "// Storybook": "@storybook/addon-actions @storybook/addon-knobs @storybook/addon-links @storybook/addon-notes @storybook/addon-options @storybook/addons @storybook/angular",
  "jest": {
    "preset": "jest-preset-angular",
    "coverageReporters": [
      "json",
      "lcov",
      "text",
      "html"
    ],
    "coverageDirectory": "<rootDir>/coverage",
    "setupTestFrameworkScriptFile": "<rootDir>/src/frontend/setup-jest.ts",
    "globals": {
      "ts-jest": {
        "tsConfigFile": "./src/frontend/tsconfig.spec.json"
      },
      "__TRANSFORM_HTML__": true
    },
    "moduleNameMapper": {
      "~api/(.*)": "<rootDir>/src/frontend/typings/$1",
      "~app/(.*)": "<rootDir>/src/frontend/app/$1"
    },
    "testResultsProcessor": "jest-sonar-reporter"
  },
  "jestSonar": {
    "reportFile": "ts-test-reports.xml"
  },
  "prettier": {
    "singleQuote": true,
    "trailingComma": "all",
    "semi": true
  },
  "lint-staged": {
    "linters": {
      "*.{json,scss}": [
        "prettier --write",
        "git add"
      ],
      "{src,e2e}/**/*.ts": [
        "tslint -p . -t stylish --fix",
        "prettier --write",
        "git add"
      ]
    },
    "ignore": [
      "src/backend/vendor/**/*"
    ]
  }
}
