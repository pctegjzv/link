package idps

import (
	"errors"
	"fmt"
	"github.com/emicklei/go-restful"
)

const (
	LDAP = "ldap"
	OIDC = "oidc"
)

type IDPCommonConfig struct {
	Type string `json:"type"`
	ID   string `json:"id"`
	Name string `json:"name"`
}

func SetDexConnectors(idpName string, dexConfig map[string]interface{}) (map[string]interface{}, error) {
	var err error

	dexConfig["connectors"] = make([]map[string]interface{}, 1)
	switch idpName {
	case LDAP:
		dexConfig["connectors"].([]map[string]interface{})[0], err = getLDAPConnectors()
	case OIDC:
		dexConfig["connectors"].([]map[string]interface{})[0], err = getOIDCConnectors()
	default:
		return nil, errors.New(fmt.Sprintf("idp type %s is error", idpName))
	}
	return dexConfig, err
}

func ValidateIDPConfig(request *restful.Request, idpName string) (field, kind, badValue string, err error) {
	switch idpName {
	case LDAP:
		data := LDAPConnectors{}
		request.ReadEntity(&data)
		return validateLDAPConfig(data)
	case OIDC:
		data := OIDCConnectors{}
		request.ReadEntity(&data)
		return validateOIDCConfig(data)
	default:
		return "idpName", "idp", idpName, errors.New(fmt.Sprintf("idp type %s is error", idpName))
	}
}
