package other

import (
	"link/src/backend/api"
	clientapi "link/src/backend/client/api"
	"link/src/backend/resource/dataselect"
	"log"
	"strings"
	"sync"

	"github.com/emicklei/go-restful"
	"k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/apis/meta/v1/unstructured"
	"k8s.io/apimachinery/pkg/runtime/schema"
	"k8s.io/client-go/dynamic"
	"k8s.io/client-go/kubernetes"
)

var unListedResource = []string{"Event", "Deployment", "ReplicaSet", "Pod", "ConfigMap",
	"Secret", "Service", "Node", "Role", "RoleBinding", "ClusterRole", "DaemonSet", "LimitRange", "ResourceQuota",
	"ClusterRoleBinding", "StatefulSet", "PersistentVolume", "PersistentVolumeClaim", "StorageClass",
	"Ingress", "NetworkPolicy", "Namespace", "Job", "CronJob"}

func inBlackList(kind string) bool {
	for _, r := range unListedResource {
		if r == kind {
			return true
		}
	}
	return false
}

func GetResourceList(client *dynamic.Client, resource *v1.APIResource, namespace string) ([]*ResourceMeta, error) {
	resourceList, err := client.Resource(resource, namespace).List(v1.ListOptions{})
	if err != nil {
		return nil, err
	}

	rl, _ := resourceList.(*unstructured.UnstructuredList)

	result := []*ResourceMeta{}
	for _, i := range rl.Items {
		resourceMeta := ResourceMeta{
			ObjectMeta: api.ObjectMeta{
				Name:              i.GetName(),
				Namespace:         i.GetNamespace(),
				Labels:            i.GetLabels(),
				Annotations:       i.GetAnnotations(),
				CreationTimestamp: i.GetCreationTimestamp(),
				Uid:               string(i.GetUID()),
			},
			TypeMeta: ResourceTypeMeta{
				Name:       resource.Name,
				Kind:       i.GetKind(),
				ApiVersion: i.GetAPIVersion(),
			},
		}
		resourceMeta.setScope()
		result = append(result, &resourceMeta)
	}
	return result, nil
}

func GetCanListResource(client kubernetes.Interface, listAll bool) ([]v1.APIResource, error) {
	serverResourceList, err := client.Discovery().ServerResources()
	if err != nil {
		return nil, err
	}
	result := []v1.APIResource{}
	for _, rl := range serverResourceList {
		for _, r := range rl.APIResources {
			if canResourceList(r) {
				gv, _ := schema.ParseGroupVersion(rl.GroupVersion)
				r.Version = gv.Version
				r.Group = gv.Group

				if _, ok := KindToName[r.Kind]; !ok {
					KindToName[r.Kind] = KindMeta{Name: r.Name, Namespaced: r.Namespaced}
				}

				if listAll || !inBlackList(r.Kind) {
					result = append(result, r)
				}

			}
		}
	}
	return result, nil
}

func canResourceList(resource v1.APIResource) bool {
	if strings.Contains(resource.Name, "/") {
		return false
	}

	for _, v := range resource.Verbs {
		if v == "list" {
			return true
		}
	}
	return false
}

func GetAllResourceList(cm clientapi.ClientManager, req *restful.Request, dsQuery *dataselect.DataSelectQuery) (*ResourceList, error) {
	k8sClient, err := cm.Client(req)
	if err != nil {
		return nil, err
	}
	resourceTypes, err := GetCanListResource(k8sClient, false)
	if err != nil {
		return nil, err
	}

	namespace := v1.NamespaceAll
	for _, filter := range dsQuery.FilterQuery.FilterByList {
		if filter.Property == dataselect.NamespaceProperty {
			ns, ok := filter.Value.(dataselect.StdComparableString)
			if ok {
				namespace = string(ns)
				break
			}
		}
	}
	var wg sync.WaitGroup
	resourceChannel := make(chan []*ResourceMeta, 1000)
	errorChannel := make(chan error, 1000)
	for _, rt := range resourceTypes {
		wg.Add(1)
		dyClient, err := cm.DynamicClient(req, &schema.GroupVersion{Group: rt.Group, Version: rt.Version})
		if err != nil {
			return nil, err
		}
		go func(resource v1.APIResource) {
			resources, err := GetResourceList(dyClient, &resource, namespace)
			if err != nil {
				log.Printf("get resource %s error", resource.Name)
				errorChannel <- err
			} else {
				resourceChannel <- resources
			}
			wg.Done()
		}(rt)
	}
	wg.Wait()
	close(resourceChannel)
	close(errorChannel)

	result := ResourceList{}
	rawResources := make([]*ResourceMeta, 0, len(resourceChannel))
	for resources := range resourceChannel {
		rawResources = append(rawResources, resources...)
	}

	rawResources = ResourceMetaList(rawResources).unique()

	rCells, filteredTotal := dataselect.GenericDataSelectWithFilter(toCells(rawResources), dsQuery)
	for err := range errorChannel {
		result.Errors = append(result.Errors, err)
	}
	result.ListMeta.TotalItems = filteredTotal
	result.Resources = fromCells(rCells)
	return &result, nil
}
