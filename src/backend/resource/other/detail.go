package other

import (
	"encoding/json"
	"fmt"
	"k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/apis/meta/v1/unstructured"
	"k8s.io/apimachinery/pkg/types"
	"k8s.io/client-go/dynamic"
	"k8s.io/client-go/kubernetes"
	"link/src/backend/api"
	"link/src/backend/resource/common"
	"link/src/backend/resource/dataselect"
	"link/src/backend/resource/event"
)

type OtherResourceDetail struct {
	ObjectMeta api.ObjectMeta             `json:"objectMeta"`
	TypeMeta   ResourceTypeMeta           `json:"typeMeta"`
	Data       *unstructured.Unstructured `json:"data"`
	EventList  common.EventList           `json:"eventList"`
}

func GetResourceDetail(dyclient *dynamic.Client, k8sclient kubernetes.Interface, resource *v1.APIResource, namespace string, name string) (*OtherResourceDetail, error) {
	r, err := dyclient.Resource(resource, namespace).Get(name, v1.GetOptions{})
	if err != nil {
		return nil, err
	}
	result := OtherResourceDetail{
		ObjectMeta: api.ObjectMeta{
			Name:              r.GetName(),
			Namespace:         r.GetNamespace(),
			Labels:            r.GetLabels(),
			Annotations:       r.GetAnnotations(),
			CreationTimestamp: r.GetCreationTimestamp(),
		},
		TypeMeta: ResourceTypeMeta{
			Name:       r.GetName(),
			Kind:       r.GetKind(),
			ApiVersion: r.GetAPIVersion(),
		},
	}

	result.Data = r

	if r.GetUID() != "" {
		events, err := event.GetEventsByUid(k8sclient, string(r.GetUID()))
		if err != nil {
			return nil, err
		}
		eventList := event.CreateEventList(events, dataselect.DefaultDataSelect)
		result.EventList = eventList
	}
	return &result, nil
}

func DeleteResource(client *dynamic.Client, resource *v1.APIResource, namespace string, name string) error {
	propagationPolicy := v1.DeletePropagationBackground
	err := client.Resource(resource, namespace).Delete(name, &v1.DeleteOptions{PropagationPolicy: &propagationPolicy})
	return err
}

func UpdateResource(client *dynamic.Client, resource *v1.APIResource, namespace string, name string, payload *unstructured.Unstructured) error {
	if payload.GetName() != name || payload.GetNamespace() != namespace {
		return fmt.Errorf("can not update name and namespace field")
	}

	_, err := client.Resource(resource, namespace).Update(payload)
	return err
}

func PatchResource(client *dynamic.Client, resource *v1.APIResource, namespace string, name string, field string, payload *FieldPayload) error {
	fieldPayloadString, err := json.Marshal(payload)
	if err != nil {
		return err
	}

	rc := client.Resource(resource, namespace)
	r, err := rc.Get(name, v1.GetOptions{})
	if err != nil {
		return err
	}

	op := "replace"
	if field == "labels" {
		if len(r.GetLabels()) == 0 {
			op = "add"
		}
	} else {
		if field == "annotations" {
			if len(r.GetAnnotations()) == 0 {
				op = "add"
			}
		}
	}

	patchPayloadTemplate :=
		`[{
        "op": "%s",
        "path": "/metadata/%s",
        "value": %s
    }]`
	patchPayload := fmt.Sprintf(patchPayloadTemplate, op, field, fieldPayloadString)
	_, err = rc.Patch(name, types.JSONPatchType, []byte(patchPayload))
	return err
}
