package node

import (
	"fmt"
	"k8s.io/api/policy/v1beta1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/types"
	client "k8s.io/client-go/kubernetes"
	"strings"
)

func UnCordon(client client.Interface, nodeName string) error {
	patchBody := []byte(`{"spec":{"unschedulable":false}}`)
	_, err := client.CoreV1().Nodes().
		Patch(nodeName, types.StrategicMergePatchType, patchBody)
	return err
}

func Cordon(client client.Interface, nodeName string) error {
	patchBody := []byte(`{"spec":{"unschedulable":true}}`)
	_, err := client.CoreV1().Nodes().
		Patch(nodeName, types.StrategicMergePatchType, patchBody)
	return err
}

func Drain(client client.Interface, nodeName string) error {
	patchBody := []byte(`{"spec":{"unschedulable":true}}`)
	_, err := client.CoreV1().Nodes().
		Patch(nodeName, types.StrategicMergePatchType, patchBody)
	if err != nil {
		return err
	}
	pods, err := client.
		CoreV1().Pods("").
		List(metav1.ListOptions{FieldSelector: fmt.Sprintf("spec.nodeName=%s", nodeName)})
	if err != nil {
		return err
	}
	for _, pod := range pods.Items {
		eviction := v1beta1.Eviction{ObjectMeta: metav1.ObjectMeta{Name: pod.Name, Namespace: pod.Namespace}}
		err := client.CoreV1().Pods(pod.Namespace).Evict(&eviction)
		if err != nil {
			return err
		}
	}
	return nil
}

type Taint struct {
	Key    string `json:"key"`
	Value  string `json:"value"`
	Effect string `json:"effect"`
}

func (t Taint) String() string {
	return fmt.Sprintf(`{"effect":"%s","key":"%s","value":"%s"}`, t.Effect, t.Key, t.Value)
}

func Taints(client client.Interface, nodeName string, taints []Taint) error {
	var patchBody string
	if len(taints) == 0 {
		patchBody = `{"spec":{"taints":null}}`
	} else {
		ts := make([]string, 0, len(taints))
		for _, t := range taints {
			ts = append(ts, t.String())
		}
		patchBody = fmt.Sprintf(`{"spec":{"taints":[%s]}}`, strings.Join(ts, ","))
	}

	_, err := client.CoreV1().Nodes().Patch(nodeName, types.StrategicMergePatchType, []byte(patchBody))
	return err
}
