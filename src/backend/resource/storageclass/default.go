package storageclass

import (
	"fmt"
	metaV1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/client-go/kubernetes"
)

func SetDefaul(client kubernetes.Interface, name string) error {
	ss, err := client.StorageV1().StorageClasses().List(metaV1.ListOptions{})
	if err != nil {
		return err
	}

	var found bool
	for _, s := range ss.Items {
		if s.Name == name {
			found = true
			annotations := s.GetAnnotations()
			if annotations == nil {
				annotations = make(map[string]string)
			}
			annotations["storageclass.kubernetes.io/is-default-class"] = "true"
			s.SetAnnotations(annotations)
			_, err := client.StorageV1().StorageClasses().Update(&s)
			if err != nil {
				return err
			}
			break
		}
	}

	if !found {
		return fmt.Errorf("storageclass %s not found", name)
	}

	for _, s := range ss.Items {
		if s.Name != name && s.GetAnnotations()["storageclass.kubernetes.io/is-default-class"] == "true" {
			annotations := s.GetAnnotations()
			annotations["storageclass.kubernetes.io/is-default-class"] = "false"
			_, err := client.StorageV1().StorageClasses().Update(&s)
			if err != nil {
				return err
			}
		}
	}
	return nil
}

func CancelDefault(client kubernetes.Interface, name string) error {
	s, err := client.StorageV1().StorageClasses().Get(name, metaV1.GetOptions{})
	if err != nil {
		return err
	}
	annotations := s.GetAnnotations()
	if annotations == nil {
		annotations = make(map[string]string)
	}
	annotations["storageclass.kubernetes.io/is-default-class"] = "false"
	_, err = client.StorageV1().StorageClasses().Update(s)
	return err
}
