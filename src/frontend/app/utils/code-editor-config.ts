import { CodeEditorActionsConfig } from 'alauda-ui';

export const createActions: CodeEditorActionsConfig = {
  diffMode: false,
  recover: false,
  copy: true,
  find: true,
  format: false,
};

export const viewActions: CodeEditorActionsConfig = {
  diffMode: false,
  clear: false,
  recover: false,
  copy: true,
  find: true,
  format: false,
};

export const updateActions: CodeEditorActionsConfig = {
  diffMode: true,
  clear: true,
  recover: true,
  copy: true,
  find: true,
  format: false,
};
