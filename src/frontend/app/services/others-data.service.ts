import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { OtherResourceDetail, Resource } from '~api/backendapi';

export interface DetailParams {
  apiVersion?: string; // AKA api version
  kind: string;
  namespace?: string;
  name: string;
}

@Injectable()
export class OthersDataService {
  constructor(private http: HttpClient) {}

  // api/v1/others/{group}/{version}/{kind}/{namespace}/{name}
  private generateDetailUrl(params: DetailParams) {
    const apiVersion = params.apiVersion.split('/');
    let group = '_';
    let version = apiVersion[0];
    if (apiVersion.length === 2) {
      group = apiVersion[0];
      version = apiVersion[1];
    }
    return (
      'api/v1/others/' +
      [
        group || '_',
        version || '_',
        params.kind,
        params.namespace || '_',
        params.name,
      ].join('/')
    );
  }

  getResourceDetailParams(resource: Resource) {
    return {
      apiVersion: resource.typeMeta.apiVersion || 'v1',
      kind: resource.typeMeta.kind,
      namespace: resource.objectMeta.namespace,
      name: resource.objectMeta.name,
    };
  }

  getDetail(params: DetailParams) {
    return this.http.get<OtherResourceDetail>(this.generateDetailUrl(params));
  }

  updateDetail(params: DetailParams, data: any) {
    return this.http.put<OtherResourceDetail>(
      this.generateDetailUrl(params),
      data,
    );
  }

  deleteDetail(params: DetailParams) {
    return this.http.delete<OtherResourceDetail>(
      this.generateDetailUrl(params),
    );
  }

  patchField(params: DetailParams, field: string, data: any) {
    return this.http.patch<OtherResourceDetail>(
      this.generateDetailUrl(params) + '/' + field,
      data,
    );
  }
}
