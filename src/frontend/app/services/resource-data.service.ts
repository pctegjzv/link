import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { MessageService, NotificationService } from 'alauda-ui';
import { safeLoadAll } from 'js-yaml';
import { set } from 'lodash';
import {
  OtherResourceCreationResult,
  Resource,
  isApiResult,
} from '~api/backendapi';

import { TranslateService } from '../translate';

import { DetailParams, OthersDataService } from './others-data.service';

@Injectable()
export class ResourceDataService {
  constructor(
    private auiMessageService: MessageService,
    private auiNotificationService: NotificationService,
    private http: HttpClient,
    private othersData: OthersDataService,
    private translate: TranslateService,
  ) {}

  // With namespace: api/v1/{kind}/{namespace}/{name}
  // Withtout namespace: api/v1/{kind}/{name}
  generateDetailUrl(params: DetailParams) {
    let kind: string;
    if (params.kind.endsWith('s')) {
      kind = params.kind.toLocaleLowerCase() + 'es';
    } else if (params.kind.toLocaleLowerCase() === 'networkpolicy') {
      kind = 'networkpolicies';
    } else {
      kind = params.kind.toLocaleLowerCase() + 's';
    }
    const partials = ['api', 'v1', kind, params.name];
    if (params.namespace) {
      partials.splice(3, 0, params.namespace);
    }
    return partials.join('/');
  }

  getResourceDetailParams(resource: Resource) {
    return this.othersData.getResourceDetailParams(resource);
  }

  getDetail<T>(detailParams: DetailParams, queryParams?: any) {
    return this.http.get<T>(this.generateDetailUrl(detailParams), {
      params: queryParams,
    });
  }

  getRawDetail(params: DetailParams) {
    return this.othersData.getDetail(params);
  }

  updateDetail(params: DetailParams, data: any) {
    return this.othersData.updateDetail(params, data);
  }

  deleteDetail(params: DetailParams) {
    return this.othersData.deleteDetail(params);
  }

  patchField(params: DetailParams, field: string, data: any) {
    return this.othersData.patchField(params, field, data);
  }

  updatePromise(params: DetailParams, data: any) {
    return this.updateDetail(params, data)
      .toPromise()
      .then(() => {
        this.auiMessageService.success({
          content: this.translate.get('update_succeeded'),
        });
      })
      .catch((err: HttpErrorResponse) => {
        if (err) {
          this.auiNotificationService.error({
            title: this.translate.get('update_failed'),
            content: err.error,
          });
        }
        throw err;
      });
  }

  /**
   * Utility function to update a value at path.
   * Note, the latest detail will be fetched first to make sure the resource data
   * to updated use the latest value.
   */
  async updateDetailAtPath(
    params: DetailParams,
    path: string,
    dataAtPath: any,
  ) {
    const { data } = await this.othersData.getDetail(params).toPromise();
    set(data, path, dataAtPath);

    return this.updatePromise(params, data);
  }

  create(yamlOrResource: string | any) {
    return new Promise((resolver, reject) => {
      const parseRes = (
        res: OtherResourceCreationResult | HttpErrorResponse | string,
      ) => {
        let hasSuccess = false;
        if (isApiResult(res)) {
          res.create_messages.forEach(({ resource, message, success }) => {
            if (success) {
              this.auiMessageService.success({
                content:
                  this.translate.get('create_succeeded') + ': ' + resource,
              });
            } else {
              this.auiNotificationService.error({
                title: this.translate.get('create_failed'),
                content: message,
              });
            }
          });

          hasSuccess = res.success_resource_count >= 1;
        } else {
          this.auiNotificationService.error({
            content: (res as HttpErrorResponse).error || res,
          });
        }

        if (hasSuccess) {
          resolver(res);
        } else {
          reject(res);
        }
      };

      try {
        if (typeof yamlOrResource === 'string') {
          yamlOrResource = safeLoadAll(yamlOrResource);
        } else if (!Array.isArray(yamlOrResource)) {
          yamlOrResource = [yamlOrResource];
        }
        this.http
          .post('api/v1/others', yamlOrResource)
          .subscribe(parseRes, parseRes);
      } catch (err) {
        parseRes(err);
      }
    });
  }
}
