import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { Inject, NgModule, Optional, SkipSelf } from '@angular/core';
import { IconRegistryService, MonacoProviderService } from 'alauda-ui';
import * as basicIconsUrl from 'alauda-ui/assets/basic-icons.svg';

import { TranslateService } from '../translate';

import { AppConfigService } from './app-config.service';
import { AuthGuardService } from './auth-guard.service';
import { AuthInterceptor } from './auth-interceptor.service';
import { AuthService } from './auth.service';
import { ConfirmBoxService } from './confirm-box.service';
import { ErrorMessageParserService } from './error-message-parser.service';
import { FeatureStateService } from './feature-state.service';
import { GenericHttpInterceptor } from './generic-http-interceptor.service';
import { HistoryService } from './history.service';
import { LogViewerService } from './log-viewer.service';
import { OthersDataService } from './others-data.service';
import { ResourceDataService } from './resource-data.service';
import { ResourceMutatePageDeactivateGuard } from './resource-mutate-page-deactivate-guard.service';
import { TerminalService } from './terminal.service';
import { TimeService } from './time.service';
import { UiStateService } from './ui-state.service';
import { UnitFormatterService } from './unit-formatter.service';
import { YamlSampleService } from './yaml-sample.service';

/**
 * Services in Angular App is static and singleton over the whole system,
 * so we will import them into the root module.
 *
 * This is to replace the Core module, which we believe is somewhat duplicates with the
 * purpose of global service module.
 */
@NgModule({
  providers: [
    AppConfigService,
    FeatureStateService,
    UiStateService,
    TimeService,
    HistoryService,
    AuthGuardService,
    ResourceMutatePageDeactivateGuard,
    ErrorMessageParserService,
    AuthService,
    LogViewerService,
    ConfirmBoxService,
    MonacoProviderService,
    TerminalService,
    UnitFormatterService,
    GenericHttpInterceptor,
    YamlSampleService,
    {
      provide: HTTP_INTERCEPTORS,
      useClass: AuthInterceptor,
      multi: true,
    },
    {
      provide: HTTP_INTERCEPTORS,
      useClass: GenericHttpInterceptor,
      multi: true,
    },
    // Resource APIs
    OthersDataService,
    ResourceDataService,
  ],
})
export class ServicesModule {
  /* Make sure ServicesModule is imported only by one NgModule the RootModule */
  constructor(
    @Inject(ServicesModule)
    @Optional()
    @SkipSelf()
    parentModule: ServicesModule,
    monacoProvider: MonacoProviderService,
    // Inject the following to make sure they are loaded ahead of other components.
    _appConfig: AppConfigService,
    _translate: TranslateService,
    _historyService: HistoryService,
    _iconService: IconRegistryService,
  ) {
    if (parentModule) {
      throw new Error(
        'ServicesModule is already loaded. Import only in AppModule.',
      );
    }

    _appConfig.init();

    setTimeout(() => {
      // Preload monaco.
      monacoProvider.initMonaco();
    }, 3000);

    _iconService.registrySvgSymbolsByUrl(basicIconsUrl);
  }
}
