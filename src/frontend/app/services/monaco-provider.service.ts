import { Injectable } from '@angular/core';
import { MonacoProviderService } from 'alauda-ui';

const CODE_EDITOR_THEME_KEY = 'code-editor-theme';

/**
 * Custom monaco provider to do some customizations.
 */
@Injectable()
export class CustomMonacoProviderService extends MonacoProviderService {
  initMonaco() {
    return super.initMonaco().then(() => {
      return new Promise(res => {
        this.changeTheme(localStorage.getItem(CODE_EDITOR_THEME_KEY));
        res();
      });
    });
  }

  changeTheme(theme: string) {
    super.changeTheme(theme);
    localStorage.setItem(CODE_EDITOR_THEME_KEY, theme);
  }

  async getLanguageFromFilename(filename: string) {
    await this.initMonaco();
    const lang = monaco.languages
      .getLanguages()
      .find(
        _lang =>
          _lang.extensions &&
          _lang.extensions.some(
            pattern => filename && filename.endsWith(pattern),
          ),
      );

    if (lang) {
      return lang.id;
    }
  }
}
