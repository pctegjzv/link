import { PortalModule } from '@angular/cdk/portal';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import {
  LayoutModule as AuiLayoutModule,
  NavModule as AuiNavModule,
} from 'alauda-ui';

import { SharedModule } from '../shared';

import { AccountMenuComponent } from './account-menu/account-menu.component';
import { NavItemHelperDirective } from './active-nav-item-helper.directive';
import { ConsoleRoutingModule } from './console-routing.module';
import { ConsoleComponent } from './console.component';

@NgModule({
  imports: [
    AuiLayoutModule,
    AuiNavModule,
    SharedModule,
    RouterModule,
    PortalModule,
    ConsoleRoutingModule,
  ],
  declarations: [
    ConsoleComponent,
    AccountMenuComponent,
    NavItemHelperDirective,
  ],
})
export class ConsoleModule {}
