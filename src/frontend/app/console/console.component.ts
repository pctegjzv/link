import { animate, style, transition, trigger } from '@angular/animations';
import { TemplatePortal } from '@angular/cdk/portal';
import {
  AfterViewInit,
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  OnDestroy,
  OnInit,
  QueryList,
  TemplateRef,
  ViewChildren,
} from '@angular/core';
import { NavigationEnd, Router } from '@angular/router';
import { DialogRef, DialogService, DialogSize } from 'alauda-ui';
import { remove } from 'lodash';
import { Observable, Subscription } from 'rxjs';
import { debounceTime, filter, map, startWith, tap } from 'rxjs/operators';

import {
  AuthService,
  ResourceDataService,
  TemplateHolderType,
  UiStateService,
} from '../services';
import { createActions } from '../utils/code-editor-config';

import { NavItemHelperDirective } from './active-nav-item-helper.directive';

export interface NavItem {
  label: string;
  path?: string;
  icon?: string;
  routerLink?: string[];
  items?: NavItem[];
  adminOn?: Boolean;
}

const DEFAULT_NAV_CONFIG: NavItem[] = [
  {
    icon: 'basic:bar_chart_s',
    label: 'nav_overview',
    path: '/overview',
    routerLink: ['/overview'],
  },
  {
    icon: 'basic:sitemap_s',
    label: 'nav_compute',
    path: '/compute',
    items: [
      {
        label: 'nav_deployment',
        routerLink: ['/deployment'],
      },
      {
        label: 'nav_daemonset',
        routerLink: ['/daemonset'],
      },
      {
        label: 'nav_statefulset',
        routerLink: ['/statefulset'],
      },
      {
        label: 'nav_replica_set',
        routerLink: ['/replicaset'],
      },
      {
        label: 'nav_cronjob',
        routerLink: ['/cronjob'],
      },
      {
        label: 'nav_job',
        routerLink: ['/job'],
      },
      {
        label: 'nav_pod',
        routerLink: ['/pod'],
      },
    ],
  },
  {
    icon: 'basic:gears_s',
    label: 'nav_config',
    path: '/config',
    items: [
      {
        label: 'nav_configmap',
        routerLink: ['/configmap'],
      },
      {
        label: 'nav_secret',
        routerLink: ['/secret'],
      },
    ],
  },
  {
    icon: 'basic:internet',
    label: 'nav_network',
    path: '/network',
    items: [
      {
        label: 'nav_service',
        routerLink: ['/service'],
      },
      {
        label: 'nav_ingress',
        routerLink: ['/ingress'],
      },
      {
        label: 'nav_networkpolicy',
        routerLink: ['/networkpolicy'],
      },
    ],
  },
  {
    icon: 'basic:storage_s',
    label: 'nav_storage',
    path: '/storage',
    items: [
      {
        label: 'nav_persistentvolumeclaim',
        routerLink: ['/persistentvolumeclaim'],
      },
      {
        label: 'nav_persistentvolume',
        routerLink: ['/persistentvolume'],
      },
      {
        label: 'nav_storageclass',
        routerLink: ['/storageclass'],
      },
    ],
  },
  {
    icon: 'basic:server_s',
    label: 'nav_cluster',
    path: '/cluster',
    items: [
      {
        label: 'nav_node',
        routerLink: ['/node'],
      },
      {
        label: 'nav_namespace',
        routerLink: ['/namespace'],
      },
    ],
  },
  {
    icon: 'basic:other_s',
    label: 'nav_other_resources',
    path: 'others',
    items: [
      {
        label: 'nav_other_resources_namespaced',
        routerLink: ['/others', 'namespaced'],
      },
      {
        label: 'nav_other_resources_clustered',
        routerLink: ['/others', 'clustered'],
      },
    ],
  },
  {
    icon: 'basic:admin_set_s',
    label: 'nav_administrator_config',
    path: 'administrator',
    items: [
      {
        label: 'nav_roles',
        routerLink: ['/administrator', 'roles'],
      },
      {
        label: 'nav_role-bindings',
        routerLink: ['/administrator', 'role-bindings'],
      },
      {
        label: 'nav_users',
        routerLink: ['/administrator', 'users'],
        adminOn: true,
      },
    ],
  },
];

@Component({
  selector: 'alk-console',
  templateUrl: 'console.component.html',
  styleUrls: ['console.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  animations: [
    trigger('zoomInOut', [
      transition('* => *', [
        style({ transform: 'scale(0.99) translateY(5px)', opacity: '0.1' }),
        animate(
          '0.3s ease',
          style({ transform: 'scale(1) translateY(0)', opacity: '1' }),
        ),
      ]),
    ]),
  ],
})
export class ConsoleComponent implements OnInit, AfterViewInit, OnDestroy {
  initialized = false;

  pageHeaderContentTemplate$: Observable<TemplatePortal<any>>;

  // Default nav config
  navConfig$: Observable<NavItem[]>;

  // TODO: For now item expansion is only related to user clicks.
  expandedPath = '';

  editorOptions = {
    language: 'yaml',
  };

  edtiorActions = createActions;

  yamlCreating = false;
  yamlInputValue = '';
  yamlOriginalValue = '';
  openedYamlDialog: DialogRef<any>;
  activatedRouteComponent: any;

  hasPendingRequests = this.uiState.requests$.pipe(
    map(requests => requests.size > 0),
    debounceTime(0),
    tap(() => this.cdr.markForCheck()),
  );

  @ViewChildren(NavItemHelperDirective)
  private navItemHelpers: QueryList<NavItemHelperDirective>;
  private eventsSubscription: Subscription;

  constructor(
    public uiState: UiStateService,
    private dialog: DialogService,
    private cdr: ChangeDetectorRef,
    private resourceData: ResourceDataService,
    private router: Router,
    private auth: AuthService,
  ) {}

  ngOnInit(): void {
    this.navConfig$ = this.auth.isAdmin$.pipe(
      map(isAdmin => {
        return this.processNavItemsWithAdminPremission(
          DEFAULT_NAV_CONFIG,
          isAdmin,
        );
      }),
    );

    this.pageHeaderContentTemplate$ = this.uiState
      .getTemplateHolder(TemplateHolderType.PageHeaderContent)
      // Since template may be triggered in a child component,
      // we must do another change detection here.
      .templatePortal$.pipe(tap(() => this.cdr.markForCheck()));

    this.eventsSubscription = this.router.events
      .pipe(
        filter(event => event instanceof NavigationEnd),
        startWith({}),
        debounceTime(50),
      )
      .subscribe(() => {
        this.uiState.setActiveItemInfo(this.getActiveNavItemInfo());
        this.initialized = true;
        this.cdr.markForCheck();
      });
  }

  ngOnDestroy(): void {
    this.eventsSubscription.unsubscribe();
  }

  ngAfterViewInit(): void {}

  onNavItemClick(path: string) {
    this.expandedPath = this.expandedPath === path ? '' : path;
  }

  processNavItemsWithAdminPremission(items: NavItem[], isAdmin: boolean) {
    remove(items, (item: NavItem) => {
      if (item.adminOn && !isAdmin) {
        return true;
      }
      if (item.items) {
        this.processNavItemsWithAdminPremission(item.items, isAdmin);
      }
      return false;
    });
    return items;
  }

  isPathExpanded(path: string, routerActive: boolean) {
    return this.expandedPath === path || routerActive;
  }

  showYamlCreateDialog(templateRef: TemplateRef<any>) {
    this.yamlOriginalValue = this.yamlInputValue;
    this.openedYamlDialog = this.dialog.open(templateRef, {
      size: DialogSize.Large,
    });
  }

  async createResource(yaml: string) {
    this.yamlCreating = true;
    try {
      await this.resourceData.create(yaml);
      this.openedYamlDialog.close();
      this.openedYamlDialog = null;
    } catch (err) {
      // ...?
    }
    this.yamlCreating = false;
    this.cdr.markForCheck();
  }

  onPageAnimationStart() {
    // A dirty fix to make sure when router component changes, the page keeps at
    // the top.
    document.querySelector('.aui-layout__page').scrollTo(0, 0);
  }

  private getActiveNavItemInfo() {
    const activeNavItemHelpers = this.navItemHelpers.filter(
      item => item.active,
    );

    return activeNavItemHelpers.map(helper => ({
      label: helper.label,
      routerLink: helper.routerLink && (helper.routerLink as any).commands,
    }));
  }
}
