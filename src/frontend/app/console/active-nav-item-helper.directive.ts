import { Directive, Input, Optional, Self, SkipSelf } from '@angular/core';
import { RouterLink } from '@angular/router';
import { NavItemComponent } from 'alauda-ui';

/**
 * This directive is a helper for ease of information collection.
 */
@Directive({
  selector: 'aui-nav-item[alkNavItemHelper]',
})
export class NavItemHelperDirective {
  @Input()
  label: string;

  get active() {
    return this.navItem.active;
  }

  get isRoot() {
    return this.navItem.isRoot;
  }

  constructor(
    @Self() public navItem: NavItemComponent,
    @Optional()
    @SkipSelf()
    public parentNavItem: NavItemComponent,
    @Optional() public routerLink: RouterLink,
  ) {}
}
