import {
  ChangeDetectionStrategy,
  Component,
  ElementRef,
  Injector,
  OnInit,
  ViewChild,
  forwardRef,
} from '@angular/core';
import { NG_VALIDATORS, NG_VALUE_ACCESSOR } from '@angular/forms';
import { CodeEditorActionsConfig, ToastService } from 'alauda-ui';
import { BaseStringMapFormComponent } from '~app/abstract';

import { TranslateService } from '../../../translate';

@Component({
  selector: 'alk-key-value-form-list',
  templateUrl: './template.html',
  styleUrls: ['./styles.scss'],
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => KeyValueFormListComponent),
      multi: true,
    },
    {
      provide: NG_VALIDATORS,
      useExisting: forwardRef(() => KeyValueFormListComponent),
      multi: true,
    },
  ],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class KeyValueFormListComponent extends BaseStringMapFormComponent
  implements OnInit {
  @ViewChild('fileUploadInput')
  fileUploadInputElementRef: ElementRef<HTMLInputElement>;

  editorActions: CodeEditorActionsConfig = {
    diffMode: false,
    clear: true,
    recover: false,
    copy: false,
    find: true,
    format: false,
    fullscreen: true,
    theme: false,
    export: false,
    import: false,
  };

  constructor(
    injector: Injector,
    private toast: ToastService,
    private translate: TranslateService,
  ) {
    super(injector);
  }

  onImport(_event: Event) {
    const length = this.fileUploadInputElementRef.nativeElement.files.length;
    let counter = 0;

    function checkFileIsBinary(content: string) {
      return Array.from(content).some(char => char.charCodeAt(0) > 127);
    }

    Array.from(this.fileUploadInputElementRef.nativeElement.files).forEach(
      file => {
        const reader = new FileReader();
        reader.onloadend = () => {
          if (checkFileIsBinary(reader.result as string)) {
            this.toast.messageError({
              content: this.translate.get('fileupload_binary_unsupported'),
            });
          } else {
            const newRow = this.createNewControl();

            newRow.setValue([file.name, reader.result]);

            this.form.push(newRow);
            this.cdr.markForCheck();
          }
          counter++;
          if (length === counter) {
            this.fileUploadInputElementRef.nativeElement.value = '';
          }
        };

        reader.readAsText(file);
      },
    );
  }

  ngOnInit() {
    super.ngOnInit();
  }

  getTextareaRows(content: string) {
    content = content || '';
    const lineCount = content.split(/\r\n|\r|\n/).length;
    return Math.min(50, Math.max(lineCount, 3));
  }
}
