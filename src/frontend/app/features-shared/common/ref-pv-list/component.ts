import {
  ChangeDetectionStrategy,
  Component,
  Input,
  OnInit,
} from '@angular/core';

import { PersistentVolume, Resource } from '~api/backendapi';

interface ColumnDef {
  name: string;
  label?: string;
}

const ALL_COLUMN_DEFS: ColumnDef[] = [
  {
    name: 'name',
    label: 'name',
  },
  {
    name: 'status',
    label: 'status',
  },
  {
    name: 'claim',
    label: 'related_pvc',
  },
  {
    name: 'capacity',
    label: 'capacity',
  },
  {
    name: 'creationTimestamp',
    label: 'created_at',
  },
  {
    name: 'action',
  },
];

@Component({
  selector: 'alk-ref-pv-list',
  templateUrl: './template.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class RefPersistentVolumeListComponent implements OnInit {
  @Input()
  list: PersistentVolume[];
  @Input()
  kind: string; // Use to give zero state an hint

  columnDefs = ALL_COLUMN_DEFS;
  columns = this.columnDefs.map(columnDef => columnDef.name);

  constructor() {}

  ngOnInit() {}

  getPersistentVolumeClaimResource(claimName: string) {
    if (claimName) {
      const claim: Resource = {
        typeMeta: {
          apiVersion: 'v1',
          kind: 'PersistentVolumeClaim',
        },
        objectMeta: {
          name: claimName.split('/')[1],
          namespace: claimName.split('/')[0],
        },
      };
      return claim;
    } else {
      return undefined;
    }
  }

  getCellValue(item: PersistentVolume, colName: string) {
    switch (colName) {
      case 'status':
        return item.status;
      case 'claim':
        return item.claim;
      case 'capacity':
        return item.capacity.storage;
      case 'name':
      case 'creationTimestamp':
      case 'labels':
        return item.objectMeta[colName];
      case 'kind':
        return item.typeMeta[colName];
      default:
        return (item as any)[colName];
    }
  }
}
