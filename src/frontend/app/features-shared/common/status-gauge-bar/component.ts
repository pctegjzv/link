import {
  ChangeDetectionStrategy,
  Component,
  EventEmitter,
  Input,
  OnChanges,
  OnInit,
  Output,
  SimpleChanges,
} from '@angular/core';
import { GenericStatus, GenericStatusList } from '~api/status';

import { TranslateService } from '../../../translate';

const StatusToGenericStatus: { [key: string]: GenericStatus } = {
  running: GenericStatus.Running,
  pending: GenericStatus.Pending,
  failed: GenericStatus.Error,
  warning: GenericStatus.Warning,
  bound: GenericStatus.Bound,
  lost: GenericStatus.Warning,
  completed: GenericStatus.Success,
};

@Component({
  selector: 'alk-status-gauge-bar',
  templateUrl: './template.html',
  styleUrls: ['./style.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class StatusGaugeBarComponent implements OnInit, OnChanges {
  @Input()
  statusInfo: { [key: string]: number; total?: number } = {};

  @Input()
  labelRender: (key: string) => string;

  @Output()
  statusBarClick = new EventEmitter();

  statusItems: {
    value: number;
    status: GenericStatus;
    key: string;
  }[] = [];

  onStatusBarClick(status: string) {
    this.statusBarClick.emit(status);
  }

  constructor(private translate: TranslateService) {}

  ngOnInit() {
    const defaultLabelRender = (key: string) => this.translate.get(key);
    this.labelRender = this.labelRender || defaultLabelRender;
  }

  ngOnChanges({ statusInfo }: SimpleChanges) {
    if (statusInfo) {
      this.statusItems = this.updateStatuItems();
    }
  }

  trackByFn(index: number) {
    return index;
  }

  private updateStatuItems() {
    return Object.keys(this.statusInfo)
      .filter(key => key !== 'total')
      .map(key => ({
        value: this.statusInfo[key],
        status: StatusToGenericStatus[key],
        key,
      }))
      .sort(
        (a, b) =>
          GenericStatusList.indexOf(a.status) -
          GenericStatusList.indexOf(b.status),
      );
  }
}
