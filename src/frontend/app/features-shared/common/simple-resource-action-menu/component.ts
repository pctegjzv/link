import { Component, Injector, Input } from '@angular/core';
import { Resource } from '~api/backendapi';

import { BaseActionMenuComponent } from '~app/abstract';

@Component({
  selector: 'alk-simple-resource-action-menu',
  templateUrl: './template.html',
  styleUrls: ['./style.scss'],
})
export class SimpleResourceActionMenuComponent extends BaseActionMenuComponent<
  Resource
> {
  @Input()
  actions = ['update', 'update_labels', 'update_annotations', 'delete'];
  constructor(injector: Injector) {
    super(injector);
  }
}
