import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  Input,
} from '@angular/core';
import { StringMap } from '~api/backendapi';

@Component({
  selector: 'alk-update-labels',
  templateUrl: './template.html',
  styleUrls: ['./styles.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class UpdateLabelsComponent {
  updating = false;

  @Input()
  title: string;

  @Input()
  labels: StringMap = {};

  @Input()
  onUpdate: (labels: StringMap) => Promise<any> = async _labels => {};

  constructor(private cdr: ChangeDetectorRef) {}

  async onConfirm() {
    this.updating = true;
    await this.onUpdate(this.labels);
    this.updating = false;
    this.cdr.markForCheck();
  }
}
