import {
  ChangeDetectionStrategy,
  Component,
  Injector,
  OnInit,
  forwardRef,
} from '@angular/core';
import {
  FormArray,
  FormControl,
  NG_VALIDATORS,
  NG_VALUE_ACCESSOR,
} from '@angular/forms';

import { BaseStringMapFormComponent } from '~app/abstract';

@Component({
  selector: 'alk-key-value-form-table',
  templateUrl: './template.html',
  styleUrls: ['./styles.scss'],
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => KeyValueFormTableComponent),
      multi: true,
    },
    {
      provide: NG_VALIDATORS,
      useExisting: forwardRef(() => KeyValueFormTableComponent),
      multi: true,
    },
  ],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class KeyValueFormTableComponent extends BaseStringMapFormComponent
  implements OnInit {
  form: FormArray;
  constructor(injector: Injector) {
    super(injector);
  }

  rowBackgroundColorFn(row: FormControl) {
    if (row.invalid) {
      return '#f9f2f4';
    } else {
      return '';
    }
  }
}
