import { Component, Injector } from '@angular/core';
import { Pod } from '~api/backendapi';

import { BaseActionMenuComponent } from '~app/abstract';

@Component({
  selector: 'alk-pod-action-menu',
  templateUrl: './template.html',
  styleUrls: [],
})
export class PodActionMenuComponent extends BaseActionMenuComponent<Pod> {
  constructor(injector: Injector) {
    super(injector);
  }
}
