import { ChangeDetectionStrategy, Component, Input } from '@angular/core';

@Component({
  selector: 'alk-update-replicas',
  templateUrl: './template.html',
  styleUrls: ['./styles.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class UpdateReplicasComponent {
  @Input()
  current: number;

  @Input()
  desired: number;

  @Input()
  name: string;

  @Input()
  kind: string;
}
