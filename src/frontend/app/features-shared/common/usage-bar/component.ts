import { Component, Input } from '@angular/core';

import { getUsageFractionColor } from '../../../utils/color-generator';

/**
 * Displays a simple usage bar whos color changes upon percentage.
 */
@Component({
  selector: 'alk-usage-bar',
  templateUrl: './template.html',
  styleUrls: ['./styles.scss'],
})
export class UsageBarComponent {
  @Input()
  percentage: number;

  get color() {
    return `${getUsageFractionColor(this.normalizedPercentage)}`;
  }

  get backgroundColor() {
    // 10 is opacity in hex
    return `${getUsageFractionColor(this.normalizedPercentage)}10`;
  }

  get normalizedPercentage() {
    return Math.min(Math.max(this.percentage, 0), 100);
  }

  get displayedPercentage() {
    return this.percentage.toFixed(2);
  }
}
