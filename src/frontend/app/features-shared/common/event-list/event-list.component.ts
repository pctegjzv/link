import {
  ChangeDetectionStrategy,
  Component,
  Input,
  OnInit,
  SimpleChanges,
} from '@angular/core';

import { OnChanges } from '@angular/core/src/metadata/lifecycle_hooks';
import { DetailEvent } from '~api/backendapi';

interface ColumnDef {
  name: string;
  label: string;
}

const ALL_COLUMN_DEFS: ColumnDef[] = [
  {
    name: 'message',
    label: 'message',
  },
  {
    name: 'source',
    label: 'source',
  },
  {
    name: 'sub_object',
    label: 'sub_object',
  },
  {
    name: 'count',
    label: 'count',
  },
  {
    name: 'firstSeen',
    label: 'first_seen',
  },
  {
    name: 'lastSeen',
    label: 'last_seen',
  },
];

@Component({
  selector: 'alk-event-list',
  templateUrl: './event-list.component.html',
  styleUrls: ['./event-list.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class EventListPageComponent implements OnInit, OnChanges {
  @Input()
  events: DetailEvent[];
  normalizedEvents: any[];

  columnDefs = ALL_COLUMN_DEFS;
  columns = [
    'message',
    'source',
    'sub_object',
    'count',
    'firstSeen',
    'lastSeen',
  ];

  constructor() {}

  ngOnInit() {}

  ngOnChanges({ events }: SimpleChanges): void {
    if (events && this.events) {
      this.normalizedEvents = this.events.map(event => this.normalize(event));
    }
  }

  trackByFn(index: number) {
    return index;
  }

  private normalize(item: DetailEvent) {
    return {
      ...item.typeMeta,
      ...item.objectMeta,
      ...item,
      source: `${item.sourceComponent} ${item.sourceHost}`,
      sub_object: item.object,
    };
  }
}
