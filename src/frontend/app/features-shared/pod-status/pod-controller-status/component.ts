import {
  ChangeDetectionStrategy,
  Component,
  Input,
  OnChanges,
  SimpleChanges,
} from '@angular/core';
import { PodControllerInfo, PodInfoItem } from '~api/backendapi';
import { PodStatusEnum } from '~api/raw-k8s';
import { GenericStatus } from '~api/status';

import {
  POD_STATUSES,
  getPodStatusIcon,
  getPodStatusToPod,
} from '../../utils/pod-status';

@Component({
  selector: 'alk-pod-controller-status',
  templateUrl: './template.html',
  styleUrls: ['./style.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class PodControllerStatusComponent implements OnChanges {
  @Input()
  podControllerInfo: PodControllerInfo;

  @Input()
  tooltipPosition = 'start';

  podStatusToPod: { [key: string]: PodInfoItem[] };
  iconStatus: GenericStatus;
  running: number;
  desired: number;
  POD_STATUSES = POD_STATUSES;
  getPodStatusIcon = getPodStatusIcon;

  ngOnChanges({ podControllerInfo }: SimpleChanges) {
    if (podControllerInfo && podControllerInfo.currentValue) {
      this.podStatusToPod = getPodStatusToPod(this.podControllerInfo);
      this.running =
        this.podStatusToPod[PodStatusEnum.Running].length +
        this.podStatusToPod[PodStatusEnum.Completed].length;
      this.desired = this.podControllerInfo.desired;
    }
  }

  get hideTooltip() {
    return (
      !this.podStatusToPod ||
      (this.podControllerInfo.warnings.length === 0 &&
        this.podControllerInfo.pods.length === 0)
    );
  }

  get notYetCreatedPods() {
    return (
      this.podControllerInfo &&
      this.desired - this.podControllerInfo.pods.length
    );
  }
}
