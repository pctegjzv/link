import {
  ChangeDetectionStrategy,
  Component,
  Input,
  OnChanges,
  SimpleChanges,
} from '@angular/core';
import { Pod, PodDetail } from '~api/backendapi';

import { getPodStatusIcon } from '../../utils/pod-status';

@Component({
  selector: 'alk-pod-status',
  templateUrl: './template.html',
  styleUrls: ['./styles.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class PodStatusComponent implements OnChanges {
  @Input()
  pod: Pod | PodDetail;

  status: string;
  tooltipType: string;
  iconStatus: string;
  warnings: string[];

  ngOnChanges({ pod }: SimpleChanges): void {
    if (pod && pod.currentValue) {
      this.iconStatus = getPodStatusIcon(this.pod.podStatus.status);
      this.tooltipType = this.getTooltipType();
      this.status = this.pod.podStatus.status;
      this.warnings = this.pod.warnings.map(warning => warning.message);
    }
  }

  private getTooltipType() {
    return 'default';
  }
}
