import { HttpClient } from '@angular/common/http';
import {
  ChangeDetectionStrategy,
  Component,
  Injector,
  Input,
  OnChanges,
  SimpleChanges,
  forwardRef,
} from '@angular/core';
import { FormArray, NG_VALIDATORS, NG_VALUE_ACCESSOR } from '@angular/forms';
import { Observable } from 'rxjs';
import { map, share } from 'rxjs/operators';
import { SecretList } from '~api/backendapi';
import { PodSpec } from '~api/raw-k8s';
import { BaseResourceFormGroupComponent } from '~app/abstract';

import { PathParam } from '../../utils';

const IMAGE_PULL_SECRET_TYPES = [
  'kubernetes.io/dockercfg',
  'kubernetes.io/dockerconfigjson',
];

@Component({
  selector: 'alk-pod-spec-form',
  templateUrl: './template.html',
  styleUrls: ['./styles.scss'],
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => PodSpecFormComponent),
      multi: true,
    },
    {
      provide: NG_VALIDATORS,
      useExisting: forwardRef(() => PodSpecFormComponent),
      multi: true,
    },
  ],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class PodSpecFormComponent
  extends BaseResourceFormGroupComponent<PodSpec>
  implements OnChanges {
  @Input()
  namespace: string;

  activeContainerIndex = 0;
  secrets$: Observable<{ name: string }[]>;

  constructor(private httpClient: HttpClient, injector: Injector) {
    super(injector);
  }

  ngOnChanges({ namespace }: SimpleChanges) {
    if (namespace) {
      this.secrets$ = this.httpClient
        .get<SecretList>(`api/v1/secrets/${this.namespace || ''}`)
        .pipe(
          map(secretList => {
            const secrets = secretList.secrets
              .filter(secret => IMAGE_PULL_SECRET_TYPES.includes(secret.type))
              .map(secret => ({
                name: secret.objectMeta.name,
              }));

            return secrets;
          }),
          share(),
        );
    }
  }

  createForm() {
    return this.fb.group({
      containers: this.fb.array([]),
      volumes: this.fb.control([]),
      serviceAccountName: this.fb.control(''),
      imagePullSecrets: this.fb.control([]),
    });
  }

  pullSecretTrackFn(secret: { name: string }) {
    return secret.name;
  }

  adaptResourceModel(resource: PodSpec) {
    // Makes sure user will not accidently remove the last container:
    if (resource && !resource.containers) {
      resource = { ...resource, containers: [{ name: '', image: '' }] };
    }
    return resource;
  }

  getDefaultFormModel(): PodSpec {
    return {
      containers: [{ name: '', image: '' }],
      volumes: [] as any,
    };
  }

  addContainer() {
    this.containersForm.push(this.getNewContainerFormControl());
    this.activeContainerIndex = this.containersForm.length - 1;
    this.cdr.markForCheck();
  }

  removeContainer(index: number) {
    this.containersForm.removeAt(index);
  }

  getOnFormArrayResizeFn() {
    return (path: PathParam) => this.getNewContainerFormControl(path);
  }

  getNewContainerFormControl(path?: PathParam) {
    let index = this.containersForm.length;
    if (path) {
      index = +path[path.length - 1];
    }
    return this.fb.control({ name: `container-${index}`, image: '' });
  }

  get containersForm(): FormArray {
    return this.form.get('containers') as FormArray;
  }

  get volumesForm(): FormArray {
    return this.form.get('volumes') as FormArray;
  }

  trackByFn(index: number) {
    return index;
  }
}
