import {
  ChangeDetectionStrategy,
  Component,
  Injector,
  Input,
  forwardRef,
} from '@angular/core';
import { NG_VALIDATORS, NG_VALUE_ACCESSOR } from '@angular/forms';
import { KeyToPath } from '~api/raw-k8s';

import { BaseResourceFormArrayComponent } from '~app/abstract';

@Component({
  selector: 'alk-key-to-path-items-form',
  templateUrl: './template.html',
  styleUrls: ['./styles.scss'],
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => KeyToPathItemsFormComponent),
      multi: true,
    },
    {
      provide: NG_VALIDATORS,
      useExisting: forwardRef(() => KeyToPathItemsFormComponent),
      multi: true,
    },
  ],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class KeyToPathItemsFormComponent extends BaseResourceFormArrayComponent<
  KeyToPath
> {
  @Input()
  availableKeys: string[];

  constructor(injector: Injector) {
    super(injector);
  }

  createForm() {
    return this.fb.array([]);
  }

  getDefaultFormModel(): KeyToPath[] {
    return [
      {
        key: '',
        path: '',
      },
    ];
  }

  getOnFormArrayResizeFn() {
    return () => this.createNewControl();
  }

  private createNewControl() {
    return this.fb.group({
      key: [''],
      path: [''],
    });
  }
}
