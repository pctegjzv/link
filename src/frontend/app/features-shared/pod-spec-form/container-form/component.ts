import {
  ChangeDetectionStrategy,
  Component,
  Injector,
  Input,
  forwardRef,
} from '@angular/core';
import { NG_VALIDATORS, NG_VALUE_ACCESSOR } from '@angular/forms';
import { Container } from '~api/raw-k8s';
import { BaseResourceFormGroupComponent } from '~app/abstract';

import { VolumesFormComponent } from '../volumes-form/component';

const DEFAULT_CONTAINER: Container = {
  name: '',
  image: '',
};

@Component({
  selector: 'alk-container-form',
  templateUrl: './template.html',
  styleUrls: ['./styles.scss'],
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => ContainerFormComponent),
      multi: true,
    },
    {
      provide: NG_VALIDATORS,
      useExisting: forwardRef(() => ContainerFormComponent),
      multi: true,
    },
  ],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ContainerFormComponent extends BaseResourceFormGroupComponent<
  Container
> {
  @Input()
  namespace: string;

  @Input()
  updateMode: boolean;

  @Input()
  volumesForm: VolumesFormComponent;

  constructor(injector: Injector) {
    super(injector);
  }

  createForm() {
    return this.fb.group({
      name: [],
      image: [],
      volumeMounts: [],
      env: [],
      envFrom: [],
      args: [],
      command: [],
      resources: {},
    });
  }

  getDefaultFormModel() {
    return DEFAULT_CONTAINER;
  }
}
