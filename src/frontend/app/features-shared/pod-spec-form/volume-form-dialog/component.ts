import { HttpClient } from '@angular/common/http';
import {
  ChangeDetectionStrategy,
  Component,
  EventEmitter,
  Input,
  OnInit,
  Output,
} from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { DialogRef } from 'alauda-ui';
import { Observable } from 'rxjs';
import {
  distinctUntilChanged,
  map,
  publishReplay,
  refCount,
  startWith,
  switchMap,
  tap,
} from 'rxjs/operators';
import { ConfigMap, Resource, ResourceList, Secret } from '~api/backendapi';

import { setFormByResource, setResourceByForm } from '../../../utils';
import { AVAILABLE_VOLUME_TYPES } from '../volumes';

@Component({
  selector: 'alk-volume-form-dialog',
  templateUrl: './template.html',
  styleUrls: ['./styles.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class VolumeFormDialogComponent implements OnInit {
  @Input()
  namespace: string;
  @Input()
  volume: any = {
    name: 'new-volume',
  };

  @Input()
  title: string;
  @Output()
  volumeChanged = new EventEmitter();

  volumeTypes = AVAILABLE_VOLUME_TYPES;
  form: FormGroup;
  attributeForms: FormGroup;

  pvcNames$: Observable<string[]>;
  configMaps$: Observable<ConfigMap[]>;
  secrets$: Observable<Secret[]>;
  selectedConfigMap$: Observable<ConfigMap>;
  selectedSecret$: Observable<Secret>;

  constructor(
    private fb: FormBuilder,
    private dialogRef: DialogRef<VolumeFormDialogComponent>,
    private httpClient: HttpClient,
  ) {}

  onConfirm() {
    // Construct valid volume data:
    const name = this.form.get('name').value;
    const type = this.form.get('type').value;
    const volume = {
      name: name,

      // Use setResourceByForm to make sure hidden fields are not overriden
      [type]: setResourceByForm(this.form.get('attributes'), this.volume[type]),
    };

    this.dialogRef.close(volume);
  }

  get type() {
    return this.form.get('type').value;
  }

  ngOnInit() {
    const type =
      // Default is persistentVolumeClaim
      this.getVolumeTypeFromVolume(this.volume) || AVAILABLE_VOLUME_TYPES[0];
    const attributes = this.volume[type];
    const configs = this.getFormControlConfigs();

    this.attributeForms = this.fb.group(configs);

    this.form = this.fb.group({
      name: [this.volume.name],
      type: [type],
    });

    if (attributes) {
      setFormByResource(this.attributeForms.get(type), attributes);
    }

    this.setupPvcForm();
    this.setupConfigMapForm();
    this.setupSecretForm();

    // Set up type observable:
    this.form
      .get('type')
      .valueChanges.pipe(
        startWith(null),
        map(() => this.type),
        distinctUntilChanged(),
      )
      .subscribe(_type => {
        if (_type && !this.form.get('name').value) {
          this.form.get('name').setValue(_type);
        }

        this.form.setControl('attributes', this.attributeForms.get(_type));
      });
  }

  private setupPvcForm() {
    const pvcForm = this.attributeForms.get('persistentVolumeClaim');

    this.pvcNames$ = this.getNamespaceResources$('persistentVolumeClaim').pipe(
      map((list: any) =>
        list.items.map((item: Resource) => item.objectMeta.name),
      ),
      tap(pvcNames => {
        if (!pvcForm.value.claimName && pvcNames && pvcNames.length > 0) {
          pvcForm.get('claimName').setValue(pvcNames[0]);
        }
      }),
    );
  }

  private setupConfigMapForm() {
    const configMapForm = this.attributeForms.get('configMap');

    this.configMaps$ = this.getNamespaceResources$('configMap').pipe(
      map((list: any) => list.items),
      tap(configMaps => {
        if (!configMapForm.value.name && configMaps && configMaps.length > 0) {
          configMapForm.get('name').setValue(configMaps[0].objectMeta.name);
        }
      }),
    );

    this.selectedConfigMap$ = configMapForm.get('name').valueChanges.pipe(
      startWith(null),
      map(() => {
        return configMapForm.get('name').value;
      }),
      distinctUntilChanged(),
      switchMap(name => {
        return this.configMaps$.pipe(
          map(
            configMaps =>
              configMaps &&
              configMaps.find(configMap => configMap.objectMeta.name === name),
          ),
        );
      }),
      publishReplay(1),
      refCount(),
    );
  }

  private setupSecretForm() {
    const secretForm = this.attributeForms.get('secret');

    this.secrets$ = this.getNamespaceResources$('secret').pipe(
      map((list: any) => list.secrets),
      tap(secrets => {
        if (!secretForm.value.secretName && secrets && secrets.length > 0) {
          secretForm.get('secretName').setValue(secrets[0].objectMeta.name);
        }
      }),
    );

    this.selectedSecret$ = secretForm.get('secretName').valueChanges.pipe(
      startWith(null),
      map(() => secretForm.get('secretName').value),
      distinctUntilChanged(),
      switchMap(name => {
        return this.secrets$.pipe(
          map(
            secrets =>
              secrets &&
              secrets.find(secret => secret.objectMeta.name === name),
          ),
        );
      }),
      publishReplay(1),
      refCount(),
    );
  }

  private getNamespaceResources$(resourceName: string) {
    return this.httpClient
      .get<ResourceList>(
        `api/v1/${resourceName.toLocaleLowerCase()}s/${this.namespace || ''}`,
      )
      .pipe(
        publishReplay(1),
        refCount(),
      );
  }

  private getVolumeTypeFromVolume(volume: any) {
    return Object.keys(volume).find(key => key !== 'name');
  }

  private getFormControlConfigs() {
    // For now we list all type whether or not they are used.
    return {
      emptyDir: [{}], // No controls yet
      persistentVolumeClaim: this.fb.group({
        claimName: this.fb.control(''),
      }),
      hostPath: this.fb.group({
        path: ['/'],
      }),
      configMap: this.fb.group({
        name: [''],
        items: [[]],
      }),
      secret: this.fb.group({
        secretName: [''],
        items: [[]],
      }),
    };
  }
}
