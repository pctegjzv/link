// Only the volume listed here will be allowed to be edited/added with form
export const AVAILABLE_VOLUME_TYPES = [
  'persistentVolumeClaim',
  'configMap',
  'secret',
  'hostPath',
  'emptyDir',
];
