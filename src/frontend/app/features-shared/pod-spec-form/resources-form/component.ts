import {
  ChangeDetectionStrategy,
  Component,
  Injector,
  forwardRef,
} from '@angular/core';
import { NG_VALIDATORS, NG_VALUE_ACCESSOR } from '@angular/forms';
import { ResourceRequirements } from '~api/raw-k8s';

import { BaseResourceFormGroupComponent } from '~app/abstract';

const AVAILABLE_RESOURCE_TYPES = ['cpu', 'memory'];

@Component({
  selector: 'alk-resources-form',
  templateUrl: './template.html',
  styleUrls: ['./styles.scss'],
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => ResourcesFormComponent),
      multi: true,
    },
    {
      provide: NG_VALIDATORS,
      useExisting: forwardRef(() => ResourcesFormComponent),
      multi: true,
    },
  ],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ResourcesFormComponent extends BaseResourceFormGroupComponent<
  ResourceRequirements
> {
  AVAILABLE_RESOURCE_TYPES = AVAILABLE_RESOURCE_TYPES;
  constructor(injector: Injector) {
    super(injector);
  }

  get limits() {
    return this.form.get('limits');
  }

  get requests() {
    return this.form.get('requests');
  }

  createForm() {
    const configs = AVAILABLE_RESOURCE_TYPES.reduce(
      (accum, type) => ({ ...accum, [type]: [] }),
      {},
    );

    return this.fb.group({
      limits: this.fb.group(configs),
      requests: this.fb.group(configs),
    });
  }

  getDefaultFormModel(): ResourceRequirements {
    return {};
  }
}
