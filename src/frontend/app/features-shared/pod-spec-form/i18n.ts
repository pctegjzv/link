export const en = {
  volume_mounts: 'Volume Mounts',
  related_attrs: 'Attributes',
  add_volume: 'Add Volume',
  add_container: 'Add Container',
  update_volume: 'Update Volume',
  mount_path: 'Mount Path',
  sub_path: 'Sub Path',
  add_mount_path: 'Add Mount Path',
  no_container_name_hint: 'Unnamed',
  mount_path_no_volumes_hint:
    'Please add a volume first before adding volume mount',
  service_account_name: 'Service Account Name',

  hostPath: 'Host Path',
  configMap: 'Config Map',
  emptyDir: 'Empty Directory',
  persistentVolumeClaim: 'PVC',
  secret: 'Secret',
  key_to_path: 'Key to Path Mappings',
  volume_name: 'Volume Name',

  container_resources: 'Container Resources',

  requests: 'Requests',
  limits: 'Limits',
};

export const zh = {
  volume_mounts: '存储卷挂载',
  related_attrs: '相关配置',
  add_volume: '添加存储卷',
  update_volume: '更新存储卷',
  mount_path: '挂载路径',
  sub_path: '子路径',
  add_mount_path: '添加挂载路径',
  service_account_name: '账号凭证',
  no_container_name_hint: '未命名',
  mount_path_no_volumes_hint: '添加挂载路径需要有效的存储卷',
  key_to_path: '引用配置项',
  add_container: '添加容器',
  volume_name: '存储卷名称',

  hostPath: '主机路径',
  configMap: '配置字典',
  secret: '保密字典',
  emptyDir: '空目录',
  persistentVolumeClaim: '持久卷声明',

  container_resources: '容器大小',

  requests: '请求值',
  limits: '限制值',
};

export default {
  en,
  zh,
};
