import { Component, Injector, forwardRef } from '@angular/core';
import { NG_VALIDATORS, NG_VALUE_ACCESSOR } from '@angular/forms';
import { BaseResourceFormArrayComponent } from '~app/abstract';

import { OnFormArrayResizeFn } from '../../../utils';

@Component({
  selector: 'alk-ports-form',
  templateUrl: './template.html',
  styleUrls: ['./styles.scss'],
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => PortsFormComponent),
      multi: true,
    },
    {
      provide: NG_VALIDATORS,
      useExisting: forwardRef(() => PortsFormComponent),
      multi: true,
    },
  ],
})
export class PortsFormComponent extends BaseResourceFormArrayComponent {
  protocols = ['TCP', 'UDP'];

  constructor(injector: Injector) {
    super(injector);
  }

  getOnFormArrayResizeFn(): OnFormArrayResizeFn {
    return () => this.createNewPortControl();
  }

  createNewPortControl() {
    return this.fb.group({
      name: [''],
      containerPort: [''],
      protocol: [this.protocols[0]],
    });
  }
}
