import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';

import { SharedModule } from '../../shared';
import { TranslateService } from '../../translate';

import i18n from './i18n';
import { PodMetricsCardsComponent } from './pod-metrics-cards/component';
import { PodMetricsCellComponent } from './pod-metrics-cell/component';

const EXPORTABLES = [PodMetricsCardsComponent, PodMetricsCellComponent];

const MODULE_PREFIX = 'metrics';

@NgModule({
  declarations: [...EXPORTABLES],
  imports: [CommonModule, SharedModule],
  exports: [...EXPORTABLES],
  providers: [],
})
export class MetricsModule {
  // For lazy loaded modules, you need to manually load the language pack by your self.
  constructor(translate: TranslateService) {
    translate.setTranslations(MODULE_PREFIX, i18n);
  }
}
