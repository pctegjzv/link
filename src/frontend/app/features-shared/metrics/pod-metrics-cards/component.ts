import { Component } from '@angular/core';

import { BasePodMetricsComponent } from '../base-pod-metrics';

/**
 * Simple metrics cards which only display a simple number for each index.
 */
@Component({
  selector: 'alk-pod-metrics-cards',
  templateUrl: './template.html',
  styleUrls: ['./styles.scss'],
})
export class PodMetricsCardsComponent extends BasePodMetricsComponent {}
