import { Component, Inject, OnInit } from '@angular/core';
import { DIALOG_DATA, DialogRef } from 'alauda-ui';
import { EnvVar } from '~api/raw-k8s';

@Component({
  templateUrl: './template.html',
})
export class EnvDialogComponent implements OnInit {
  env: EnvVar[] = [];
  namespace = '';

  constructor(
    @Inject(DIALOG_DATA) public data: { env: EnvVar[]; namespace: string },
    private dialogRef: DialogRef,
  ) {}

  ngOnInit(): void {
    if (this.data) {
      const { env, namespace } = this.data;
      this.env = env;
      this.namespace = namespace;
    }
  }

  onConfirm() {
    this.dialogRef.close(this.env);
  }
}
