import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { SharedModule } from '../../shared';
import { FeatureSharedCommonModule } from '../common/module';
import { MetricsModule } from '../metrics/module';
import { PodStatusModule } from '../pod-status/module';

import { PodListComponent } from './component';

const EXPORTABLES = [PodListComponent];

@NgModule({
  declarations: [...EXPORTABLES],
  imports: [
    CommonModule,
    RouterModule,
    SharedModule,
    FeatureSharedCommonModule,
    MetricsModule,
    PodStatusModule,
  ],
  exports: [...EXPORTABLES],
  providers: [],
})
export class PodListModule {}
