import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { ShellModule } from '../features-shared/shell/module';
import { SharedModule } from '../shared';
import { TranslateService } from '../translate';

import i18n from './i18n';
import { TerminalPageComponent } from './page/component';
import { TerminalRoutingModule } from './routing.module';

const MODULE_PREFIX = 'terminal';

@NgModule({
  imports: [SharedModule, ShellModule, RouterModule, TerminalRoutingModule],
  declarations: [TerminalPageComponent],
})
export class TerminalModule {
  // For lazy loaded modules, you need to manually load the language pack by your self.
  constructor(translate: TranslateService) {
    translate.setTranslations(MODULE_PREFIX, i18n);
  }
}
