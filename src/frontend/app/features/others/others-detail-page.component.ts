import { Component, Injector } from '@angular/core';
import { Observable } from 'rxjs';
import { OtherResourceDetail } from '~api/backendapi';
import { BaseDetailPageComponent } from '~app/abstract';

import { OthersDataService } from '../../services';

@Component({
  templateUrl: './others-detail-page.component.html',
  styleUrls: ['./others-detail-page.component.scss'],
})
export class OthersDetailPageComponent extends BaseDetailPageComponent<
  OtherResourceDetail
> {
  constructor(private othersData: OthersDataService, injector: Injector) {
    super(injector);
  }

  tabs = ['yaml', 'events'];

  fetchData(params: {
    kind: string;
    namespace: string;
    name: string;
    apiVersion: string;
  }): Observable<OtherResourceDetail> {
    return this.othersData.getDetail(params);
  }
}
