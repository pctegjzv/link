import {
  ChangeDetectionStrategy,
  Component,
  Injector,
  OnInit,
} from '@angular/core';
import { Observable } from 'rxjs';
import { first } from 'rxjs/operators';
import { ReplicaSetDetail } from '~api/backendapi';
import { PodSpec } from '~api/raw-k8s';
import { BaseDetailPageComponent } from '~app/abstract';

import { ResourceDataService } from '../../services';

@Component({
  selector: 'alk-replica-set-detail-page',
  templateUrl: './replica-set-detail-page.component.html',
  styleUrls: ['./replica-set-detail-page.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ReplicaSetDetailPageComponent
  extends BaseDetailPageComponent<ReplicaSetDetail>
  implements OnInit {
  constructor(private resourceData: ResourceDataService, injector: Injector) {
    super(injector);
  }

  tabs = ['basic_info', 'yaml', 'config_management', 'logs', 'events'];

  fetchData(params: {
    namespace: string;
    name: string;
  }): Observable<ReplicaSetDetail> {
    return this.resourceData.getDetail({ ...params, kind: 'replicaset' });
  }

  async onPodSpecUpdate(podSpec: PodSpec) {
    const detail = await this.detail$.pipe(first()).toPromise();
    const detailParams = this.resourceData.getResourceDetailParams(detail);

    try {
      await this.resourceData.updateDetailAtPath(
        detailParams,
        'spec.template.spec',
        podSpec,
      );
      this.updated$.next(null);
    } catch (error) {}
  }
}
