import {
  ChangeDetectionStrategy,
  Component,
  Injector,
  OnInit,
} from '@angular/core';
import { Observable } from 'rxjs';
import { Deployment, DeploymentList } from '~api/backendapi';
import {
  BaseResourceListPageComponent,
  ColumnDef,
  FetchParams,
} from '~app/abstract';

import { filterBy, getQuery, pageBy, sortBy } from '../../utils';

const ALL_COLUMN_DEFS: ColumnDef[] = [
  {
    name: 'name',
    label: 'name',
    sortable: true,
  },
  {
    name: 'namespace',
    label: 'namespace',
    sortable: true,
  },
  {
    name: 'pods',
    label: 'status',
  },
  {
    name: 'containerImages',
    label: 'images',
  },
  {
    name: 'metrics',
    label: 'used_resources',
  },
  {
    name: 'creationTimestamp',
    label: 'created_at',
    sortable: true,
    sortStart: 'desc',
  },
  {
    name: 'action',
  },
];

@Component({
  selector: 'alk-deployment-list-page',
  templateUrl: './list-page.component.html',
  styleUrls: ['./list-page.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class DeploymentListPageComponent
  extends BaseResourceListPageComponent<DeploymentList, Deployment>
  implements OnInit {
  map(value: DeploymentList): Deployment[] {
    return value.deployments;
  }

  getColumnDefs(): ColumnDef[] {
    return ALL_COLUMN_DEFS;
  }

  fetchResources(params: FetchParams): Observable<DeploymentList> {
    return this.http.get<DeploymentList>(
      `api/v1/deployments/${params.namespace || ''}`,
      {
        params: getQuery(
          // Currently filter by name means "search"
          filterBy('name', params.search),
          sortBy(params.sort.active, params.sort.direction === 'desc'),
          pageBy(params.pageParams.pageIndex, params.pageParams.pageSize),
        ),
      },
    );
  }

  constructor(injector: Injector) {
    super(injector);
  }
}
