import { ChangeDetectionStrategy, Component, Injector } from '@angular/core';
import { Deployment } from '~api/raw-k8s';

import { BaseResourceMutatePageComponent } from '~app/abstract';

@Component({
  selector: 'alk-deployment-mutate-page',
  templateUrl: './mutate-page.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class DeploymentMutatePageComponent extends BaseResourceMutatePageComponent<
  Deployment
> {
  get kind() {
    return 'Deployment';
  }

  constructor(injector: Injector) {
    super(injector);
  }
}
