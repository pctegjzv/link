import { Component, EventEmitter, Input, Output } from '@angular/core';
import { DeploymentDetail } from '~api/backendapi';

import { AppConfigService, ResourceDataService } from '../../services';

@Component({
  selector: 'alk-deployment-detail-basic-info',
  templateUrl: './detail-basic-info.component.html',
  styleUrls: [],
})
export class DeploymentDetailBasicInfoComponent {
  @Input()
  detail: DeploymentDetail;
  @Output()
  updated = new EventEmitter();

  get podListEndpoint() {
    if (this.detail) {
      return (
        this.resourceData.generateDetailUrl({
          name: this.detail.objectMeta.name,
          namespace: this.detail.objectMeta.namespace,
          kind: 'deployment',
        }) + '/pod'
      );
    } else {
      return '';
    }
  }

  constructor(
    private resourceData: ResourceDataService,
    public appConfig: AppConfigService,
  ) {}
}
