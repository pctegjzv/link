import {
  ChangeDetectionStrategy,
  Component,
  Injector,
  OnInit,
  forwardRef,
} from '@angular/core';
import { NG_VALIDATORS, NG_VALUE_ACCESSOR, Validators } from '@angular/forms';
import { Deployment, DeploymentTypeMeta } from '~api/raw-k8s';

import { BasePodControllerFormComponent } from '~app/abstract';

@Component({
  selector: 'alk-deployment-form',
  templateUrl: './template.html',
  styleUrls: ['./styles.scss'],
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => DeploymentFormComponent),
      multi: true,
    },
    {
      provide: NG_VALIDATORS,
      useExisting: forwardRef(() => DeploymentFormComponent),
      multi: true,
    },
  ],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class DeploymentFormComponent
  extends BasePodControllerFormComponent<Deployment>
  implements OnInit {
  kind = 'Deployment';
  namespaced = true;

  getDefaultFormModel() {
    return DeploymentTypeMeta;
  }

  createSpecForm() {
    const specForm = super.createSpecForm();
    specForm.addControl('strategy', this.fb.control({}));
    specForm.addControl('replicas', this.fb.control(1, [Validators.min(0)]));
    return specForm;
  }

  constructor(injector: Injector) {
    super(injector);
  }
}
