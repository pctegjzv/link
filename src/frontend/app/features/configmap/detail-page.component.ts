import {
  ChangeDetectionStrategy,
  Component,
  Injector,
  OnInit,
} from '@angular/core';
import { Observable } from 'rxjs';
import { ConfigMapDetail } from '~api/backendapi';

import { BaseDetailPageComponent } from '~app/abstract';
import { ResourceDataService } from '~app/services';

@Component({
  selector: 'alk-configmap-detail-page',
  templateUrl: './detail-page.component.html',
  styleUrls: ['./detail-page.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ConfigMapDetailPageComponent
  extends BaseDetailPageComponent<ConfigMapDetail>
  implements OnInit {
  constructor(private resourceData: ResourceDataService, injector: Injector) {
    super(injector);
    this.debouncedUpdate = () => {};
  }

  tabs = ['basic_info', 'yaml'];

  fetchData(params: {
    namespace: string;
    name: string;
  }): Observable<ConfigMapDetail> {
    return this.resourceData.getDetail({ ...params, kind: 'configmap' });
  }
}
