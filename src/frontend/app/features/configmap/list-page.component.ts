import {
  ChangeDetectionStrategy,
  Component,
  Injector,
  OnInit,
} from '@angular/core';
import { Observable } from 'rxjs';
import { ConfigMap, ConfigMapList } from '~api/backendapi';
import {
  BaseResourceListPageComponent,
  ColumnDef,
  FetchParams,
} from '~app/abstract';

import { filterBy, getQuery, pageBy, sortBy } from '../../utils';

const ALL_COLUMN_DEFS: ColumnDef[] = [
  {
    name: 'name',
    label: 'name',
    sortable: true,
  },
  {
    name: 'namespace',
    label: 'namespace',
    sortable: true,
  },
  {
    name: 'size',
    label: 'count_size',
  },
  {
    name: 'creationTimestamp',
    label: 'created_at',
    sortable: true,
    sortStart: 'desc',
  },
  {
    name: 'action',
  },
];

@Component({
  selector: 'alk-configmap-list-page',
  templateUrl: './list-page.component.html',
  styleUrls: ['./list-page.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ConfigMapListPageComponent
  extends BaseResourceListPageComponent<ConfigMapList, ConfigMap>
  implements OnInit {
  map(value: ConfigMapList): ConfigMap[] {
    return value.items;
  }

  getColumnDefs(): ColumnDef[] {
    return ALL_COLUMN_DEFS;
  }

  fetchResources(params: FetchParams): Observable<ConfigMapList> {
    return this.http.get<ConfigMapList>(
      `api/v1/configmaps/${params.namespace || ''}`,
      {
        params: getQuery(
          // Currently filter by name means "search"
          filterBy('name', params.search),
          sortBy(params.sort.active, params.sort.direction === 'desc'),
          pageBy(params.pageParams.pageIndex, params.pageParams.pageSize),
        ),
      },
    );
  }

  constructor(injector: Injector) {
    super(injector);
  }
}
