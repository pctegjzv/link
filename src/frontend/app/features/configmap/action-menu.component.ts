import { Component, Injector } from '@angular/core';
import { ConfigMap } from '~api/backendapi';

import { BaseActionMenuComponent } from '~app/abstract';

@Component({
  selector: 'alk-configmap-action-menu',
  templateUrl: './action-menu.component.html',
  styleUrls: [],
})
export class ConfigMapActionMenuComponent extends BaseActionMenuComponent<
  ConfigMap
> {
  constructor(injector: Injector) {
    super(injector);
  }
}
