import {
  ChangeDetectionStrategy,
  Component,
  Injector,
  OnInit,
  forwardRef,
} from '@angular/core';
import { NG_VALIDATORS, NG_VALUE_ACCESSOR, Validators } from '@angular/forms';
import { ConfigMap, ConfigMapTypeMeta } from '~api/raw-k8s';

import { BaseKubernetesResourceFormComponent } from '~app/abstract';

@Component({
  selector: 'alk-configmap-form',
  templateUrl: './template.html',
  styleUrls: ['./styles.scss'],
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => ConfigMapFormComponent),
      multi: true,
    },
    {
      provide: NG_VALIDATORS,
      useExisting: forwardRef(() => ConfigMapFormComponent),
      multi: true,
    },
  ],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ConfigMapFormComponent
  extends BaseKubernetesResourceFormComponent<ConfigMap>
  implements OnInit {
  kind = 'ConfigMap';

  getDefaultFormModel() {
    return ConfigMapTypeMeta;
  }

  createForm() {
    const metadataForm = this.fb.group({
      name: this.fb.control('', [Validators.required]),
      namespace: this.fb.control('', [Validators.required]),
      labels: this.fb.control({}),
      annotations: this.fb.control({}),
    });
    return this.fb.group({
      metadata: metadataForm,
      data: this.fb.control({}),
    });
  }

  ngOnInit() {
    super.ngOnInit();
  }

  constructor(injector: Injector) {
    super(injector);
  }
}
