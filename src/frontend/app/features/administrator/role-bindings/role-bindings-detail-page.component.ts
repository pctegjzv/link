import { HttpClient } from '@angular/common/http';
import {
  ChangeDetectionStrategy,
  Component,
  Injector,
  OnInit,
} from '@angular/core';
import { Observable } from 'rxjs';
import { Resource, RoleBindingDetail } from '~api/backendapi';
import { BaseDetailPageComponent } from '~app/abstract';

import { getDetailRoutes } from '../../../utils/detail-route-builder';

@Component({
  selector: 'alk-role-bindings-detail-page',
  templateUrl: './role-bindings-detail-page.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class RoleBindingsDetailPageComponent
  extends BaseDetailPageComponent<RoleBindingDetail>
  implements OnInit {
  tabs: string[];

  fetchData(params: {
    namespace: string;
    name: string;
  }): Observable<RoleBindingDetail> {
    const endpoint = params.namespace
      ? `api/v1/rbac/namespaces/${params.namespace || ''}/rolebindings/${
          params.name
        }`
      : `api/v1/rbac/clusterrolebindings/${params.name}`;
    return this.http.get<RoleBindingDetail>(endpoint);
  }

  getDetailRoutes(item: Resource) {
    return getDetailRoutes(item);
  }

  ngOnInit() {
    super.ngOnInit();
  }

  constructor(private http: HttpClient, injector: Injector) {
    super(injector);
  }
}
