import { NgModule } from '@angular/core';
import { FeatureSharedCommonModule } from '~app/features-shared/common/module';

import { SharedModule } from '../../shared/shared.module';
import { TranslateService } from '../../translate';

import { AdministratorRoutingModule } from './administrator-routing.module';
import i18n from './i18n';
import { RoleBindingsCreatePageComponent } from './role-bindings/role-bindings-create-page.component';
import { RoleBindingsDetailBasicInfoComponent } from './role-bindings/role-bindings-detail-basic-info.component';
import { RoleBindingsDetailPageComponent } from './role-bindings/role-bindings-detail-page.component';
import { RoleBindingsListPageComponent } from './role-bindings/role-bindings-list-page.component';
import { RoleBindingsUpdateDialogComponent } from './role-bindings/role-bindings-update-dialog.component';
import { SubjectsEditorComponent } from './role-bindings/subjects-editor.component';
import { RoleActionMenuComponent } from './roles/role-action-menu.component';
import { RoleCreatePageComponent } from './roles/role-create-page.component';
import { RoleDetailBasicInfoComponent } from './roles/role-detail-basic-info.component';
import { RoleDetailPageComponent } from './roles/role-detail-page.component';
import { RolePermissionsListComponent } from './roles/role-permissions-list.component';
import { RoleRuleAddComponent } from './roles/role-rule-add.component';
import { RoleUpdateDialogComponent } from './roles/role-update-dialog.component';
import { RolesListPageComponent } from './roles/roles-list-page.component';
import { UserDetailPageComponent } from './users/user-detail-page.component';
import { UsersListPageComponent } from './users/users-list-page.component';

const MODULE_PREFIX = 'administrator';

@NgModule({
  imports: [
    SharedModule,
    FeatureSharedCommonModule,
    AdministratorRoutingModule,
  ],
  declarations: [
    RolesListPageComponent,
    RoleCreatePageComponent,
    RoleDetailPageComponent,
    RoleDetailBasicInfoComponent,
    RoleActionMenuComponent,
    RoleUpdateDialogComponent,
    RoleBindingsUpdateDialogComponent,
    RolePermissionsListComponent,
    RoleRuleAddComponent,
    RoleBindingsListPageComponent,
    RoleBindingsDetailPageComponent,
    RoleBindingsDetailBasicInfoComponent,
    RoleBindingsCreatePageComponent,
    UsersListPageComponent,
    UserDetailPageComponent,
    SubjectsEditorComponent,
  ],
  // For lazy modules, you need to use entryComponents by template.
  entryComponents: [
    RoleUpdateDialogComponent,
    RoleBindingsUpdateDialogComponent,
  ],
  providers: [],
})
export class AdministratorModule {
  // For lazy loaded modules, you need to manually load the language pack by your self.
  constructor(translate: TranslateService) {
    translate.setTranslations(MODULE_PREFIX, i18n);
  }
}
