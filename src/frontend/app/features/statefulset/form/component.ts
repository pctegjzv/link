import {
  ChangeDetectionStrategy,
  Component,
  Injector,
  OnInit,
  forwardRef,
} from '@angular/core';
import { NG_VALIDATORS, NG_VALUE_ACCESSOR, Validators } from '@angular/forms';
import { Observable } from 'rxjs';
import {
  map,
  publishReplay,
  refCount,
  startWith,
  switchMap,
  tap,
} from 'rxjs/operators';
import { ServiceList } from '~api/backendapi';
import { StatefulSet, StatefulSetTypeMeta } from '~api/raw-k8s';

import { BasePodControllerFormComponent } from '~app/abstract';

@Component({
  selector: 'alk-stateful-set-form',
  templateUrl: './template.html',
  styleUrls: ['./styles.scss'],
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => StatefulSetFormComponent),
      multi: true,
    },
    {
      provide: NG_VALIDATORS,
      useExisting: forwardRef(() => StatefulSetFormComponent),
      multi: true,
    },
  ],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class StatefulSetFormComponent
  extends BasePodControllerFormComponent<StatefulSet>
  implements OnInit {
  services$: Observable<string[]>;

  kind = 'StatefulSet';

  constructor(injector: Injector) {
    super(injector);
  }

  ngOnInit(): void {
    super.ngOnInit();

    const namespaceControl = this.form.get('metadata.namespace');

    this.services$ = namespaceControl.valueChanges.pipe(
      startWith(namespaceControl.value),
      switchMap(namespace =>
        this.http
          .get<ServiceList>(`api/v1/services/${namespace}`)
          .pipe(
            map(({ services }) =>
              services.map(service => service.objectMeta.name),
            ),
          ),
      ),
      publishReplay(1),
      refCount(),
      tap(services => {
        const serviceNameControl = this.form.get('spec.serviceName');
        if (!serviceNameControl.value && services && services.length) {
          serviceNameControl.setValue(services[0]);
        }
      }),
    );
  }

  getDefaultFormModel() {
    return StatefulSetTypeMeta;
  }

  createSpecForm() {
    const specForm = super.createSpecForm();
    specForm.addControl('updateStrategy', this.fb.control({}));
    specForm.addControl('replicas', this.fb.control(1, [Validators.min(0)]));
    specForm.addControl('serviceName', this.fb.control({}));
    return specForm;
  }
}
