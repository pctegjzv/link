export const en = {
  create_statefulset: 'Create Stateful Set',
};

export const zh = {
  create_statefulset: '创建有状态副本集',
};

export default {
  en,
  zh,
};
