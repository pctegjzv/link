export const en = {
  create_persistentvolume: 'Create PV',
  reclaim_policy: 'Reclaim Policy',
};

export const zh = {
  create_persistentvolume: '创建持久卷',
  reclaim_policy: '回收策略',
};

export default {
  en,
  zh,
};
