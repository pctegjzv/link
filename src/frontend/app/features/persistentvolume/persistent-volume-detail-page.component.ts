import { Component, Injector, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { PersistentVolumeDetail } from '~api/backendapi';
import { BaseDetailPageComponent } from '~app/abstract';

import { ResourceDataService } from '../../services';

@Component({
  selector: 'alk-persistent-volume-detail-page',
  templateUrl: './persistent-volume-detail-page.component.html',
  styleUrls: ['./persistent-volume-detail-page.component.scss'],
})
export class PersistentVolumeDetailPageComponent
  extends BaseDetailPageComponent<PersistentVolumeDetail>
  implements OnInit {
  podNames$: Observable<string[]>;
  tabs = ['basic_info', 'yaml'];

  fetchData(params: {
    namespace: string;
    name: string;
  }): Observable<PersistentVolumeDetail> {
    return this.resourceData.getDetail({
      ...params,
      kind: 'PersistentVolume',
    });
  }

  constructor(private resourceData: ResourceDataService, injector: Injector) {
    super(injector);
  }

  ngOnInit() {
    super.ngOnInit();
  }
}
