import { ChangeDetectionStrategy, Component, Injector } from '@angular/core';
import { DaemonSet } from '~api/backendapi';

import { BaseActionMenuComponent } from '~app/abstract';

@Component({
  selector: 'alk-daemon-set-action-menu',
  templateUrl: './action-menu.component.html',
  styleUrls: ['./action-menu.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class DaemonSetActionMenuComponent extends BaseActionMenuComponent<
  DaemonSet
> {
  constructor(injector: Injector) {
    super(injector);
  }
}
