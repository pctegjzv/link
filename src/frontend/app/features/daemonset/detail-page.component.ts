import {
  ChangeDetectionStrategy,
  Component,
  Injector,
  OnInit,
} from '@angular/core';
import { Observable } from 'rxjs';
import { first, map } from 'rxjs/operators';
import { DaemonSetDetail } from '~api/backendapi';
import { PodSpec } from '~api/raw-k8s';
import { BaseDetailPageComponent } from '~app/abstract';

import { ResourceDataService } from '../../services';

@Component({
  selector: 'alk-daemon-set-detail-page',
  templateUrl: './detail-page.component.html',
  styleUrls: ['./detail-page.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class DaemonSetDetailPageComponent
  extends BaseDetailPageComponent<DaemonSetDetail>
  implements OnInit {
  podNames$: Observable<string[]>;
  tabs = ['basic_info', 'yaml', 'config_management', 'logs', 'events'];

  fetchData(params: {
    namespace: string;
    name: string;
  }): Observable<DaemonSetDetail> {
    return this.resourceData.getDetail({ ...params, kind: 'DaemonSet' });
  }

  constructor(private resourceData: ResourceDataService, injector: Injector) {
    super(injector);
  }

  ngOnInit() {
    super.ngOnInit();
    this.podNames$ = this.detail$.pipe(
      map(
        detail =>
          detail.podList &&
          detail.podList.pods &&
          detail.podList.pods.map(pod => pod.objectMeta && pod.objectMeta.name),
      ),
    );
  }

  async onPodSpecUpdate(podSpec: PodSpec) {
    const detail = await this.detail$.pipe(first()).toPromise();
    const detailParams = this.resourceData.getResourceDetailParams(detail);

    try {
      await this.resourceData.updateDetailAtPath(
        detailParams,
        'spec.template.spec',
        podSpec,
      );
      this.updated$.next(null);
    } catch (error) {}
  }
}
