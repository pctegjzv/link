import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { ResourceMutatePageDeactivateGuard } from '../../services';

import { DaemonSetDetailPageComponent } from './detail-page.component';
import { DaemonSetListPageComponent } from './list-page.component';
import { DaemonSetMutatePageComponent } from './mutate-page/component';

// TODO: move somewhere else, like a universal sample provider?
const sample = `apiVersion: apps/v1
kind: DaemonSet
metadata:
  name: daemonset-demo
  namespace: default
spec:
  selector:
    matchLabels:
      name: daemonset-demo
  template:
    metadata:
      labels:
        name: daemonset-demo
    spec:
      containers:
      - name: nginx
        image: nginx:alpine
`;

@NgModule({
  imports: [
    RouterModule.forChild([
      {
        path: '',
        pathMatch: 'full',
        redirectTo: 'list',
      },
      {
        path: 'list',
        component: DaemonSetListPageComponent,
      },
      {
        path: 'detail',
        component: DaemonSetDetailPageComponent,
      },
      {
        path: 'update',
        component: DaemonSetMutatePageComponent,
        canDeactivate: [ResourceMutatePageDeactivateGuard],
      },
      {
        path: 'create',
        component: DaemonSetMutatePageComponent,
        canDeactivate: [ResourceMutatePageDeactivateGuard],
        data: {
          sample: sample,
        },
      },
    ]),
  ],
  exports: [RouterModule],
})
export class DaemonSetRoutingModule {}
