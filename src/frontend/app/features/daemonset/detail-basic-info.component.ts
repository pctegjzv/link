import {
  ChangeDetectionStrategy,
  Component,
  EventEmitter,
  Input,
  Output,
} from '@angular/core';
import { DaemonSetDetail } from '~api/backendapi';

import { AppConfigService, ResourceDataService } from '../../services';

@Component({
  selector: 'alk-daemon-set-detail-basic-info',
  templateUrl: './detail-basic-info.component.html',
  styleUrls: ['./detail-basic-info.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class DaemonSetDetailBasicInfoComponent {
  @Input()
  detail: DaemonSetDetail;
  @Output()
  updated = new EventEmitter();

  get podListEndpoint() {
    if (this.detail) {
      return (
        this.resourceData.generateDetailUrl({
          name: this.detail.objectMeta.name,
          namespace: this.detail.objectMeta.namespace,
          kind: 'DaemonSet',
        }) + '/pod'
      );
    } else {
      return '';
    }
  }
  constructor(
    private resourceData: ResourceDataService,
    public appConfig: AppConfigService,
  ) {}
}
