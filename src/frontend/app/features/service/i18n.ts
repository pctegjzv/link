export const en = {
  create_service: 'Create Service',
  session_affinity: 'Session Affinity',
};

export const zh = {
  create_service: '创建服务',
  session_affinity: '会话亲和性',
};

export default {
  en,
  zh,
};
