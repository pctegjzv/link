import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import {
  SimpleResourceCreatePageComponent,
  SimpleResourceUpdatePageComponent,
} from '~app/features-shared/common';

import { ServiceDetailPageComponent } from './service-detail-page.component';
import { ServiceListPageComponent } from './service-list-page.component';

// TODO: move somewhere else, like a universal sample provider or laoder?
const sample = `kind: Service
apiVersion: v1
metadata:
  name: my-service
  namespace: default
spec:
  selector:
    app: MyApp
  ports:
  - name: http
    protocol: TCP
    port: 80
    targetPort: 12345
  type: NodePort
  externalIPs:
  - 80.11.12.10`;

@NgModule({
  imports: [
    RouterModule.forChild([
      {
        path: '',
        pathMatch: 'full',
        redirectTo: 'list',
      },
      {
        path: 'list',
        component: ServiceListPageComponent,
      },
      {
        path: 'detail',
        component: ServiceDetailPageComponent,
      },
      {
        path: 'update',
        component: SimpleResourceUpdatePageComponent,
      },
      {
        path: 'create',
        component: SimpleResourceCreatePageComponent,
        data: {
          sample: sample,
        },
      },
    ]),
  ],
  exports: [RouterModule],
})
export class ServiceRoutingModule {}
