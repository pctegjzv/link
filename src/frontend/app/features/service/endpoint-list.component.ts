import { Component, Input } from '@angular/core';
import { EndpointResource } from '~api/backendapi';

@Component({
  selector: 'alk-endpoint-list',
  templateUrl: `./endpoint-list.component.html`,
})
export class EndpointListComponent {
  columns = ['host', 'ports', 'node', 'ready'];

  @Input()
  endpoints: EndpointResource[];

  get showZeroState() {
    return !this.endpoints || this.endpoints.length === 0;
  }
}
