import { Component, EventEmitter, Input, Output } from '@angular/core';
import { ServiceDetail } from '~api/backendapi';

import { ResourceDataService } from '../../services';

@Component({
  selector: 'alk-service-detail-basic-info',
  templateUrl: './service-detail-basic-info.component.html',
  styleUrls: [],
})
export class ServiceDetailBasicInfoComponent {
  @Input()
  detail: ServiceDetail;
  @Output()
  updated = new EventEmitter();

  get podListEndpoint() {
    if (this.detail) {
      return (
        this.resourceData.generateDetailUrl({
          name: this.detail.objectMeta.name,
          namespace: this.detail.objectMeta.namespace,
          kind: 'service',
        }) + '/pod'
      );
    } else {
      return '';
    }
  }

  constructor(private resourceData: ResourceDataService) {}
}
