import {
  Component,
  EventEmitter,
  Input,
  OnChanges,
  Output,
  SimpleChanges,
} from '@angular/core';
import { NodeDetail } from '~api/backendapi';

import { AppConfigService, ResourceDataService } from '../../services';

@Component({
  selector: 'alk-node-detail-basic-info',
  templateUrl: './node-detail-basic-info.component.html',
  styleUrls: ['./node-detail-basic-info.component.scss'],
})
export class NodeDetailBasicInfoComponent implements OnChanges {
  addresseList: string[][];
  @Input()
  detail: NodeDetail;
  @Output()
  updated = new EventEmitter();

  ngOnChanges({ detail }: SimpleChanges) {
    if (detail && detail.currentValue) {
      this.addresseList = (this.detail.addresses || []).map(
        ({ type, address }) => [type, address],
      );
    }
  }

  get podListEndpoint() {
    if (this.detail) {
      return (
        this.resourceData.generateDetailUrl({
          name: this.detail.objectMeta.name,
          kind: 'node',
        }) + '/pod'
      );
    } else {
      return '';
    }
  }

  get metricsServerEnabled() {
    return this.appConfig.metricsServerEnabled;
  }

  constructor(
    private resourceData: ResourceDataService,
    private appConfig: AppConfigService,
  ) {}
}
