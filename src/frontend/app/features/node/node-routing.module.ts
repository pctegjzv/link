import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { SimpleResourceUpdatePageComponent } from '~app/features-shared/common';

import { NodeDetailPageComponent } from './node-detail-page.component';
import { NodeListPageComponent } from './node-list-page.component';

@NgModule({
  imports: [
    RouterModule.forChild([
      {
        path: '',
        pathMatch: 'full',
        redirectTo: 'list',
      },
      {
        path: 'list',
        component: NodeListPageComponent,
      },
      {
        path: 'detail',
        component: NodeDetailPageComponent,
      },
      {
        path: 'update',
        component: SimpleResourceUpdatePageComponent,
      },
    ]),
  ],
  exports: [RouterModule],
})
export class NodeRoutingModule {}
