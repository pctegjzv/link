import { Component, Injector, forwardRef } from '@angular/core';
import { NG_VALIDATORS, NG_VALUE_ACCESSOR } from '@angular/forms';

import { BaseResourceFormArrayComponent } from '~app/abstract';

@Component({
  selector: 'alk-node-taint-form',
  templateUrl: './template.html',
  styleUrls: ['./styles.scss'],
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => NodeTaintFormComponent),
      multi: true,
    },
    {
      provide: NG_VALIDATORS,
      useExisting: forwardRef(() => NodeTaintFormComponent),
      multi: true,
    },
  ],
})
export class NodeTaintFormComponent extends BaseResourceFormArrayComponent {
  constructor(injector: Injector) {
    super(injector);
  }

  createForm() {
    return this.fb.array([]);
  }

  getDefaultResource(): any[] {
    return [];
  }

  getOnFormArrayResizeFn() {
    return () => this.createNewControl();
  }

  private createNewControl() {
    return this.fb.group({
      key: [],
      value: [],
      effect: [],
    });
  }
}
