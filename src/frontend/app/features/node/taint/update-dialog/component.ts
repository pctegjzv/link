import { animate, style, transition, trigger } from '@angular/animations';
import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  Input,
} from '@angular/core';
import { NodeTaint } from '~api/backendapi';

@Component({
  selector: 'alk-node-taint-update-dialog',
  templateUrl: './template.html',
  styleUrls: ['./styles.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  animations: [
    trigger('zoomInOut', [
      transition(':enter', [
        style({ transform: 'scale(0.95)', opacity: '0' }),
        animate(200, style({ transform: 'scale(1)', opacity: '1' })),
      ]),
    ]),
  ],
})
export class NodeTaintUpdateDialogComponent {
  updating = false;

  @Input()
  title: string;
  @Input()
  taints: NodeTaint[] = [];
  @Input()
  onUpdate: (taints: NodeTaint[]) => Promise<any> = async _taints => {};

  constructor(private cdr: ChangeDetectorRef) {}

  async onConfirm() {
    this.updating = true;
    await this.onUpdate(this.taints);
    this.updating = false;
    this.cdr.markForCheck();
  }
}
