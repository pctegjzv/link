import {
  Component,
  EventEmitter,
  Input,
  OnChanges,
  Output,
  SimpleChanges,
} from '@angular/core';
import { NodeDetail, NodeTaint } from '~api/backendapi';

@Component({
  selector: 'alk-node-taint-field',
  templateUrl: './template.html',
  styles: [
    `
      alk-node-taint-button {
        margin-left: 4px;
        flex-shrink: 0;
      }
    `,
  ],
})
export class NodeTaintFieldComponent implements OnChanges {
  @Input()
  resource: NodeDetail;
  @Output()
  updated = new EventEmitter();

  taintList: string[][] = [];

  ngOnChanges({ resource }: SimpleChanges) {
    if (resource && resource.currentValue) {
      this.taintList = (this.resource.taints || []).map((taint: NodeTaint) => {
        return taint.value
          ? [`${taint.key}=${taint.value}`, taint.effect]
          : [`${taint.key}=${taint.effect}`];
      });
    }
  }
}
