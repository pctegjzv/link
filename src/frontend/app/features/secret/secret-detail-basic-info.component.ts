import { Component, EventEmitter, Input, Output } from '@angular/core';
import { SecretDetail } from '~api/backendapi';

@Component({
  selector: 'alk-secret-detail-basic-info',
  templateUrl: './secret-detail-basic-info.component.html',
  styleUrls: ['./secret-detail-basic-info.component.scss'],
})
export class SecretDetailBasicInfoComponent {
  @Input()
  detail: SecretDetail;
  @Output()
  updated = new EventEmitter();
}
