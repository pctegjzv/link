export const en = {
  create_secret: 'Create Secret',
};

export const zh = {
  create_secret: '创建保密字典',
};

export default {
  en,
  zh,
};
