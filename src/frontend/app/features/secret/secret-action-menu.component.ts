import { Component, Injector } from '@angular/core';
import { Secret } from '~api/backendapi';

import { BaseActionMenuComponent } from '~app/abstract';

@Component({
  selector: 'alk-secret-action-menu',
  templateUrl: './secret-action-menu.component.html',
  styleUrls: [],
})
export class SecretActionMenuComponent extends BaseActionMenuComponent<Secret> {
  constructor(injector: Injector) {
    super(injector);
  }
}
