import { NgModule } from '@angular/core';
import { FeatureSharedCommonModule } from '~app/features-shared/common/module';

import { SharedModule } from '../../shared/shared.module';
import { TranslateService } from '../../translate';

import { SecretDataViwerComponent } from './data-viewer/component';
import i18n from './i18n';
import { SecretActionMenuComponent } from './secret-action-menu.component';
import { SecretDetailBasicInfoComponent } from './secret-detail-basic-info.component';
import { SecretDetailPageComponent } from './secret-detail-page.component';
import { SecretListPageComponent } from './secret-list-page.component';
import { SecretRoutingModule } from './secret-routing.module';
const MODULE_PREFIX = 'secret';

@NgModule({
  imports: [SharedModule, FeatureSharedCommonModule, SecretRoutingModule],
  declarations: [
    SecretListPageComponent,
    SecretActionMenuComponent,
    SecretDetailPageComponent,
    SecretDetailBasicInfoComponent,
    SecretDataViwerComponent,
  ],
  providers: [],
})
export class SecretModule {
  // For lazy loaded modules, you need to manually load the language pack by your self.
  constructor(translate: TranslateService) {
    translate.setTranslations(MODULE_PREFIX, i18n);
  }
}
