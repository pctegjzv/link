export const en = {
  create_cronjob: 'Create Cron Job',
  schedule: 'Schedule',
  suspend: 'Suspended',
  last_schedule: 'Last Scheduled Time',
  starting_deadline: 'Starting Deadline',
  active_deadline_seconds: 'Active Deadline Seconds',
  backoff_limit: 'Backoff Limit',
  completions: 'Completions',
  parallelism: 'Parallelism',
  concurrency_policy: 'Concurrency Policy',
  job_history_limit: 'Job History Limit',
  active_jobs: 'Active Jobs',
  completed_jobs: 'Completed Jobs',
};

export const zh = {
  create_cronjob: '创建定时任务',
  schedule: '定时规则',
  suspend: '挂起',
  last_schedule: '最近执行时间',
  starting_deadline: '开始时间限制',
  active_deadline_seconds: '超时时间',
  backoff_limit: '重试次数',
  completions: '预计完成数',
  parallelism: '最大并行数',
  concurrency_policy: '并发策略',
  job_history_limit: '任务历史限制',
  completed_jobs: '历史执行任务',
  active_jobs: '运行中的任务',
};

export default {
  en,
  zh,
};
