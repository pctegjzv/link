import { ChangeDetectionStrategy, Component, Injector } from '@angular/core';
import { CronJob, CronJobDetail } from '~api/backendapi';

import { BaseActionMenuComponent } from '~app/abstract';

@Component({
  selector: 'alk-cron-job-action-menu',
  templateUrl: './action-menu.component.html',
  styleUrls: [],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class CronJobActionMenuComponent extends BaseActionMenuComponent<
  CronJob | CronJobDetail
> {
  constructor(injector: Injector) {
    super(injector);
  }
}
