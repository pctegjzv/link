export const en = {
  create_persistentvolumeclaim: 'Create PVC',
};

export const zh = {
  create_persistentvolumeclaim: '创建持久卷声明',
};

export default {
  en,
  zh,
};
