import { ChangeDetectionStrategy, Component, Injector } from '@angular/core';
import { PersistentVolumeClaim } from '~api/backendapi';

import { BaseActionMenuComponent } from '~app/abstract';

@Component({
  selector: 'alk-persistent-volume-claim-action-menu',
  templateUrl: './persistent-volume-claim-action-menu.component.html',
  styleUrls: ['./persistent-volume-claim-action-menu.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class PersistentVolumeClaimActionMenuComponent extends BaseActionMenuComponent<
  PersistentVolumeClaim
> {
  constructor(injector: Injector) {
    super(injector);
  }
}
