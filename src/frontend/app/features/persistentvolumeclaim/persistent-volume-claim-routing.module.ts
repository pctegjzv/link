import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import {
  SimpleResourceCreatePageComponent,
  SimpleResourceUpdatePageComponent,
} from '~app/features-shared/common';

import { PersistentVolumeClaimDetailPageComponent } from './persistent-volume-claim-detail-page.component';
import { PersistentVolumeClaimListPageComponent } from './persistent-volume-claim-list-page.component';

// TODO: move somewhere else, like a universal sample provider?
const sample = `apiVersion: v1
kind: PersistentVolumeClaim
metadata:
  name: myclaim
  namespace: default
spec:
  accessModes:
    - ReadWriteMany
  volumeMode: Filesystem
  resources:
    requests:
      storage: 8Gi
  storageClassName: slow
  selector:
    matchLabels:
      release: "stable"
    matchExpressions:
      - {key: environment, operator: In, values: [dev]}
`;

@NgModule({
  imports: [
    RouterModule.forChild([
      {
        path: '',
        pathMatch: 'full',
        redirectTo: 'list',
      },
      {
        path: 'list',
        component: PersistentVolumeClaimListPageComponent,
      },
      {
        path: 'detail',
        component: PersistentVolumeClaimDetailPageComponent,
      },
      {
        path: 'update',
        component: SimpleResourceUpdatePageComponent,
      },
      {
        path: 'create',
        component: SimpleResourceCreatePageComponent,
        data: {
          sample: sample,
        },
      },
    ]),
  ],
  exports: [RouterModule],
})
export class PersistentVolumeClaimRoutingModule {}
