import {
  Component,
  EventEmitter,
  Input,
  OnChanges,
  Output,
  SimpleChanges,
} from '@angular/core';
import { JobDetail, Resource } from '~api/backendapi';

import { getOwnerResource } from '../../features-shared/utils/owner-ref';
import { AppConfigService, ResourceDataService } from '../../services';

@Component({
  selector: 'alk-job-detail-basic-info',
  templateUrl: './detail-basic-info.component.html',
  styleUrls: ['./detail-basic-info.component.scss'],
})
export class JobDetailBasicInfoComponent implements OnChanges {
  controller: Resource;

  @Input()
  detail: JobDetail;

  @Output()
  updated = new EventEmitter();

  ngOnChanges({ detail }: SimpleChanges) {
    if (detail && detail.currentValue) {
      this.controller = getOwnerResource(this.detail.data);
    }
  }

  get podListEndpoint() {
    if (this.detail) {
      return (
        this.resourceData.generateDetailUrl({
          name: this.detail.objectMeta.name,
          namespace: this.detail.objectMeta.namespace,
          kind: 'job',
        }) + '/pod'
      );
    } else {
      return '';
    }
  }

  constructor(
    private resourceData: ResourceDataService,
    public appConfig: AppConfigService,
  ) {}
}
