import { ChangeDetectionStrategy, Component, Injector } from '@angular/core';
import { Job, JobDetail } from '~api/backendapi';

import { BaseActionMenuComponent } from '~app/abstract';

@Component({
  selector: 'alk-job-action-menu',
  templateUrl: './action-menu.component.html',
  styleUrls: [],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class JobActionMenuComponent extends BaseActionMenuComponent<
  Job | JobDetail
> {
  constructor(injector: Injector) {
    super(injector);
  }
}
