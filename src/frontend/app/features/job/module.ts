import { NgModule } from '@angular/core';

import { FeaturesSharedWorkloadModule } from '../../features-shared/workload-module';
import { SharedModule } from '../../shared/shared.module';
import { TranslateService } from '../../translate';

import { JobDetailPageComponent } from './detail-page.component';
import { JobListPageComponent } from './list-page.component';

import { JobActionMenuComponent } from './action-menu.component';
import { JobDetailBasicInfoComponent } from './detail-basic-info.component';
import i18n from './i18n';
import { JobRoutingModule } from './routing.module';

@NgModule({
  imports: [JobRoutingModule, SharedModule, FeaturesSharedWorkloadModule],
  declarations: [
    JobListPageComponent,
    JobDetailPageComponent,
    JobActionMenuComponent,
    JobDetailBasicInfoComponent,
  ],
  providers: [],
})
export class JobModule {
  // For lazy loaded modules, you need to manually load the language pack by your self.
  constructor(translate: TranslateService) {
    translate.setTranslations('job', i18n);
  }
}
