export const en = {
  create_job: 'Create Job',
  active_deadline_seconds: 'Active Deadline Seconds',
  backoff_limit: 'Backoff Limit',
  completions: 'Completions',
  parallelism: 'Parallelism',
};

export const zh = {
  create_job: '创建任务',
  active_deadline_seconds: '超时时间',
  backoff_limit: '重试次数',
  completions: '预计完成数',
  parallelism: '最大并行数',
};

export default {
  en,
  zh,
};
