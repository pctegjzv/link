import { Component, Injector } from '@angular/core';
import { Ingress } from '~api/backendapi';

import { BaseActionMenuComponent } from '~app/abstract';

@Component({
  selector: 'alk-ingress-action-menu',
  templateUrl: './ingress-action-menu.component.html',
  styleUrls: [],
})
export class IngressActionMenuComponent extends BaseActionMenuComponent<
  Ingress
> {
  constructor(injector: Injector) {
    super(injector);
  }
}
