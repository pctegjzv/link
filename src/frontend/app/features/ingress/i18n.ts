export const en = {
  create_ingress: 'Create Ingress',
  default_backend: 'Default Backend',
  ingress_rules: 'Ingress Rules',
  ingress_tls: 'TLS Certificates',
};

export const zh = {
  create_ingress: '创建访问权',
  default_backend: '默认服务',
  ingress_rules: '访问权规则',
  ingress_tls: 'TLS 证书',
};

export default {
  en,
  zh,
};
