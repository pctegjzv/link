import { Component, EventEmitter, Input, Output } from '@angular/core';
import { IngressDetail } from '~api/backendapi';

@Component({
  selector: 'alk-ingress-detail-basic-info',
  templateUrl: './ingress-detail-basic-info.component.html',
  styleUrls: [],
})
export class IngressDetailBasicInfoComponent {
  @Input()
  detail: IngressDetail;

  @Output()
  updated = new EventEmitter();

  getServiceResource(name: string) {
    return {
      name: name,
      kind: 'Service',
      namespace: this.detail.objectMeta.namespace,
    };
  }

  getDefaultBackend() {
    return this.detail && this.detail.spec.backend;
  }

  constructor() {}
}
