import { ChangeDetectionStrategy, Component, Input } from '@angular/core';
import { HTTPIngressRuleValue } from '~api/raw-k8s';

// 展示单一的Ingress Rule value
@Component({
  selector: 'alk-ingress-rule-value-table',
  templateUrl: './template.html',
  styleUrls: ['./styles.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class IngressRuleValueTableComponent {
  // Data context for rows.
  @Input()
  ruleValue: HTTPIngressRuleValue;

  @Input()
  namespace: string;

  get isEmpty() {
    return (
      !this.ruleValue ||
      !this.ruleValue.paths ||
      this.ruleValue.paths.length === 0
    );
  }

  getServiceResource(name: string) {
    return {
      name: name,
      kind: 'Service',
      namespace: this.namespace,
    };
  }
}
