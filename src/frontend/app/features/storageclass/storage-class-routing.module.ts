import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import {
  SimpleResourceCreatePageComponent,
  SimpleResourceUpdatePageComponent,
} from '~app/features-shared/common';

import { StorageClassDetailPageComponent } from './storage-class-detail-page.component';
import { StorageClassListPageComponent } from './storage-class-list-page.component';

// TODO: move somewhere else, like a universal sample provider?
const sample = `kind: StorageClass
apiVersion: storage.k8s.io/v1
metadata:
  name: standard
provisioner: kubernetes.io/aws-ebs
parameters:
  type: gp2
reclaimPolicy: Retain
mountOptions:
  - debug
`;

@NgModule({
  imports: [
    RouterModule.forChild([
      {
        path: '',
        pathMatch: 'full',
        redirectTo: 'list',
      },
      {
        path: 'list',
        component: StorageClassListPageComponent,
      },
      {
        path: 'detail',
        component: StorageClassDetailPageComponent,
      },
      {
        path: 'update',
        component: SimpleResourceUpdatePageComponent,
      },
      {
        path: 'create',
        component: SimpleResourceCreatePageComponent,
        data: {
          sample: sample,
        },
      },
    ]),
  ],
  exports: [RouterModule],
})
export class StorageClassRoutingModule {}
