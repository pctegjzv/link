import { Component, Input } from '@angular/core';
import { NamespaceEffectiveQuota } from '~api/backendapi';

@Component({
  selector: 'alk-effective-quota-list',
  templateUrl: './template.html',
})
export class EffectiveQuotaListComponent {
  @Input()
  effectiveQuotas: NamespaceEffectiveQuota[];

  columns = ['resourceName', 'hard', 'used', 'scope'];

  splitName(name: string) {
    return name.split('.').reverse();
  }
}
