import { Component, Input, OnChanges } from '@angular/core';
import { ResourceQuotaStatus } from '~api/backendapi';

@Component({
  selector: 'alk-resource-quota-status-list',
  templateUrl: './template.html',
})
export class ResourceQuotaStatusListComponent implements OnChanges {
  @Input()
  statusList: { [resourceName: string]: ResourceQuotaStatus };

  list: [string, ResourceQuotaStatus][];

  columns = ['resourceName', 'hard', 'used'];

  ngOnChanges() {
    this.list = Object.entries(this.statusList || {});
  }

  splitName(name: string) {
    return name.split('.').reverse();
  }
}
