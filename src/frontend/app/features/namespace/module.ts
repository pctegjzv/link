import { NgModule } from '@angular/core';

import { FeaturesSharedWorkloadModule } from '../../features-shared/workload-module';
import { SharedModule } from '../../shared/shared.module';
import { TranslateService } from '../../translate';

import { NamespaceActionMenuComponent } from './action-menu.component';
import { NamespaceCreateDialogComponent } from './create-dialog/component';
import { NamespaceDetailBasicInfoComponent } from './detail-basic-info.component';
import { NamespaceDetailChartsComponent } from './detail-charts.component';
import { NamespaceDetailPageComponent } from './detail-page.component';
import { NamespaceFormComponent } from './form/component';
import i18n from './i18n';
import { LimitRangeFormComponent } from './limit-range/form/component';
import { LimitRangeItemArrayFormComponent } from './limit-range/item-array-form/component';
import { LimitRangeLimitListComponent } from './limit-range/limit-list/component';
import { LimitRangeListItemDialogComponent } from './limit-range/list-item/dialog/component';
import { LimitRangeListItemFormComponent } from './limit-range/list-item/form/component';
import { LimitRangeMutatePageComponent } from './limit-range/mutate-page/mutate-page.component';
import { NamespaceListPageComponent } from './list-page.component';
import { EffectiveQuotaListComponent } from './resource-quota/effective-quota-list/component';
import { ResourceQuotaFormComponent } from './resource-quota/form/component';
import { ResourceQuotaMutatePageComponent } from './resource-quota/mutate-page/mutate-page.component';
import { ResourceQuotaStatusListComponent } from './resource-quota/status-list/component';
import { NamespaceRoutingModule } from './routing.module';
import { NamespaceStatusComponent } from './status.component';
import { NamespaceWorkloadPieComponent } from './workload-pie/component';

const MODULE_PREFIX = 'namespace';

@NgModule({
  imports: [SharedModule, NamespaceRoutingModule, FeaturesSharedWorkloadModule],
  declarations: [
    NamespaceStatusComponent,
    NamespaceListPageComponent,
    NamespaceDetailPageComponent,
    NamespaceActionMenuComponent,
    NamespaceDetailBasicInfoComponent,
    NamespaceDetailChartsComponent,
    NamespaceWorkloadPieComponent,
    LimitRangeFormComponent,
    LimitRangeLimitListComponent,
    LimitRangeMutatePageComponent,
    LimitRangeItemArrayFormComponent,
    LimitRangeListItemFormComponent,
    LimitRangeListItemDialogComponent,
    ResourceQuotaMutatePageComponent,
    ResourceQuotaFormComponent,
    EffectiveQuotaListComponent,
    ResourceQuotaStatusListComponent,
    NamespaceFormComponent,
    NamespaceCreateDialogComponent,
  ],
  entryComponents: [
    NamespaceCreateDialogComponent,
    LimitRangeListItemDialogComponent,
  ],
  providers: [],
})
export class NamespaceModule {
  // For lazy loaded modules, you need to manually load the language pack by your self.
  constructor(translate: TranslateService) {
    translate.setTranslations(MODULE_PREFIX, i18n);
  }
}
