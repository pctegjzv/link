import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { SimpleResourceUpdatePageComponent } from '~app/features-shared/common';

import { ResourceMutatePageDeactivateGuard } from '../../services';

import { NamespaceDetailPageComponent } from './detail-page.component';
import { LimitRangeMutatePageComponent } from './limit-range/mutate-page/mutate-page.component';
import { NamespaceListPageComponent } from './list-page.component';
import { ResourceQuotaMutatePageComponent } from './resource-quota/mutate-page/mutate-page.component';

// TODO: move somewhere else, like a universal sample provider?
const quotaSample = `# You can specify a resource quota like this:
apiVersion: v1
kind: ResourceQuota
metadata:
  name: example-compute-resources
  namespace: example-namespace
spec:
  hard: {}
`;

const limitRangeSample = `# You can specify a resource quota like this:
apiVersion: v1
kind: LimitRange
metadata:
  name: example-mem-limit-range
  namespace: example-namespace
`;

@NgModule({
  imports: [
    RouterModule.forChild([
      {
        path: '',
        pathMatch: 'full',
        redirectTo: 'list',
      },
      {
        path: 'list',
        component: NamespaceListPageComponent,
      },
      {
        path: 'detail',
        component: NamespaceDetailPageComponent,
      },
      {
        path: 'update',
        component: SimpleResourceUpdatePageComponent,
      },
      {
        path: 'resourcequota',
        children: [
          {
            path: 'create',
            component: ResourceQuotaMutatePageComponent,
            canDeactivate: [ResourceMutatePageDeactivateGuard],
            data: { sample: quotaSample },
          },
          {
            path: 'update',
            component: ResourceQuotaMutatePageComponent,
            canDeactivate: [ResourceMutatePageDeactivateGuard],
          },
        ],
      },
      {
        path: 'limitrange',
        children: [
          {
            path: 'create',
            component: LimitRangeMutatePageComponent,
            canDeactivate: [ResourceMutatePageDeactivateGuard],
            data: { sample: limitRangeSample },
          },
          {
            path: 'update',
            component: LimitRangeMutatePageComponent,
            canDeactivate: [ResourceMutatePageDeactivateGuard],
          },
        ],
      },
    ]),
  ],
  exports: [RouterModule],
})
export class NamespaceRoutingModule {}
