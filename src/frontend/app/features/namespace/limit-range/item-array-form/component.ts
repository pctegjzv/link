import {
  ChangeDetectionStrategy,
  Component,
  Injector,
  OnInit,
  forwardRef,
} from '@angular/core';
import { NG_VALIDATORS, NG_VALUE_ACCESSOR } from '@angular/forms';
import { DialogService } from 'alauda-ui';
import { LimitRangeListItem } from '~api/backendapi';
import { LimitRangeItem } from '~api/raw-k8s';
import { BaseResourceFormArrayComponent } from '~app/abstract';

import {
  flattenAndMergeLimitRangeItemList,
  recoverFromLimitRangeListItem,
} from '../limit-range';
import { LimitRangeListItemDialogComponent } from '../list-item/dialog/component';

@Component({
  selector: 'alk-namespace-limit-range-item-array-form',
  templateUrl: './template.html',
  styles: [
    `
      :host {
        flex: 1;
        width: 100%;
      }
    `,
  ],
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => LimitRangeItemArrayFormComponent),
      multi: true,
    },
    {
      provide: NG_VALIDATORS,
      useExisting: forwardRef(() => LimitRangeItemArrayFormComponent),
      multi: true,
    },
  ],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class LimitRangeItemArrayFormComponent
  extends BaseResourceFormArrayComponent<LimitRangeItem, LimitRangeListItem>
  implements OnInit {
  constructor(private dialog: DialogService, injector: Injector) {
    super(injector);
  }

  createForm() {
    return this.fb.array([]);
  }

  getDefaultFormModel() {
    return [] as LimitRangeListItem[];
  }

  adaptResourceModel(lris: LimitRangeItem[]) {
    return flattenAndMergeLimitRangeItemList(lris);
  }

  adaptFormModel(lrlis: LimitRangeListItem[]): LimitRangeItem[] {
    return recoverFromLimitRangeListItem(lrlis);
  }

  async editListItem(index: number) {
    const res: LimitRangeListItem = await this.dialog
      .open(LimitRangeListItemDialogComponent, {
        data: { excludedItems: this.formModel, item: this.formModel[index] },
      })
      .afterClosed()
      .toPromise();
    if (res) {
      this.form.controls[index].setValue(res);
      this.cdr.markForCheck();
    }
  }

  async addListItem() {
    const res: LimitRangeListItem = await this.dialog
      .open(LimitRangeListItemDialogComponent, {
        data: { excludedItems: this.formModel },
      })
      .afterClosed()
      .toPromise();
    if (res) {
      this.form.push(this.fb.control(res));
      this.cdr.markForCheck();
    }
  }
}
