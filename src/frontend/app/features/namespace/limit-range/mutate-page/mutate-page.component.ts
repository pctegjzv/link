import {
  ChangeDetectionStrategy,
  Component,
  Injector,
  OnInit,
} from '@angular/core';
import { Observable } from 'rxjs';
import { map, startWith } from 'rxjs/operators';
import { LimitRange } from '~api/raw-k8s';

import { BaseResourceMutatePageComponent } from '~app/abstract';

@Component({
  selector: 'alk-limit-range-mutate-page',
  templateUrl: './mutate-page.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class LimitRangeMutatePageComponent
  extends BaseResourceMutatePageComponent<LimitRange>
  implements OnInit {
  namespace$: Observable<string>;

  get kind() {
    return 'LimitRange';
  }

  constructor(injector: Injector) {
    super(injector);
  }

  ngOnInit() {
    super.ngOnInit();
    this.namespace$ = this.activatedRoute.params.pipe(
      startWith(this.activatedRoute.snapshot.params),
      map(params => params.namespace || 'default'),
    );
  }

  afterMutation(resource: LimitRange) {
    this.router.navigate([
      '/namespace/detail',
      { name: resource.metadata.namespace },
    ]);
  }
}
