import { Component, Injector } from '@angular/core';
import { Namespace } from '~api/backendapi';

import { BaseActionMenuComponent } from '~app/abstract';

@Component({
  selector: 'alk-namespace-action-menu',
  templateUrl: './action-menu.component.html',
  styleUrls: [],
})
export class NamespaceActionMenuComponent extends BaseActionMenuComponent<
  Namespace
> {
  constructor(injector: Injector) {
    super(injector);
  }

  createLimitRange() {
    this.createSubResource('limitrange');
  }

  createResourceQuota() {
    this.createSubResource('resourcequota');
  }

  createSubResource(type: string) {
    this.router.navigate([
      `/namespace/${type}/create`,
      { namespace: this.resource.objectMeta.name },
    ]);
  }
}
