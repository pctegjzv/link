import {
  ChangeDetectionStrategy,
  Component,
  Injector,
  forwardRef,
} from '@angular/core';
import { NG_VALIDATORS, NG_VALUE_ACCESSOR, Validators } from '@angular/forms';
import { Namespace, NamespaceTypeMeta } from '~api/raw-k8s';
import { BaseKubernetesResourceFormComponent } from '~app/abstract';

@Component({
  selector: 'alk-namespace-form',
  templateUrl: './template.html',
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => NamespaceFormComponent),
      multi: true,
    },
    {
      provide: NG_VALIDATORS,
      useExisting: forwardRef(() => NamespaceFormComponent),
      multi: true,
    },
  ],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class NamespaceFormComponent extends BaseKubernetesResourceFormComponent<
  Namespace
> {
  kind = 'Namespace';

  createForm() {
    const metadata = this.fb.group({
      name: this.fb.control('', [Validators.required]),
      labels: this.fb.control({}),
      annotations: this.fb.control({}),
    });

    return this.fb.group({
      metadata,
    });
  }

  getDefaultFormModel(): Namespace {
    return NamespaceTypeMeta;
  }

  constructor(injector: Injector) {
    super(injector);
  }
}
