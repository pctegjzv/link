import { Component, EventEmitter, Input, Output } from '@angular/core';
import { NetworkPolicyDetail } from '~api/backendapi';

import { ResourceDataService } from '../../services';

@Component({
  selector: 'alk-network-policy-detail-basic-info',
  templateUrl: './network-policy-detail-basic-info.component.html',
  styleUrls: [],
})
export class NetworkPolicyDetailBasicInfoComponent {
  @Input()
  detail: NetworkPolicyDetail;
  @Output()
  updated = new EventEmitter();

  get podListEndpoint() {
    if (this.detail) {
      return (
        this.resourceData.generateDetailUrl({
          name: this.detail.objectMeta.name,
          namespace: this.detail.objectMeta.namespace,
          kind: 'networkpolicy',
        }) + '/pod'
      );
    } else {
      return '';
    }
  }

  constructor(private resourceData: ResourceDataService) {}
}
