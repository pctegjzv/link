import { Component, Injector } from '@angular/core';
import { NetworkPolicy } from '~api/backendapi';

import { BaseActionMenuComponent } from '~app/abstract';

@Component({
  selector: 'alk-network-policy-action-menu',
  templateUrl: './network-policy-action-menu.component.html',
  styleUrls: [],
})
export class NetworkPolicyActionMenuComponent extends BaseActionMenuComponent<
  NetworkPolicy
> {
  constructor(injector: Injector) {
    super(injector);
  }
}
