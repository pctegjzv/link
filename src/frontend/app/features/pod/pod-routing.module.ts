import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { SimpleResourceUpdatePageComponent } from '~app/features-shared/common';

import { PodDetailPageComponent } from './pod-detail-page.component';
import { PodListPageComponent } from './pod-list-page.component';

@NgModule({
  imports: [
    RouterModule.forChild([
      {
        path: '',
        pathMatch: 'full',
        redirectTo: 'list',
      },
      {
        path: 'list',
        component: PodListPageComponent,
      },
      {
        path: 'detail',
        component: PodDetailPageComponent,
      },
      {
        path: 'update',
        component: SimpleResourceUpdatePageComponent,
      },
    ]),
  ],
  exports: [RouterModule],
})
export class PodRoutingModule {}
