import { Component, Input } from '@angular/core';

@Component({
  selector: 'alk-shrink-list',
  templateUrl: './template.html',
  styleUrls: ['./style.scss'],
})
export class ShrinkListComponent {
  @Input()
  list: string[];
}
