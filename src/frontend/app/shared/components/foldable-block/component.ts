import { Component } from '@angular/core';

@Component({
  selector: 'alk-foldable-block',
  templateUrl: './template.html',
  styleUrls: ['./styles.scss'],
})
export class FoldableBlockComponent {
  folded = true;
}
