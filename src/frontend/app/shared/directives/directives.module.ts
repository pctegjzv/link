import { PortalModule } from '@angular/cdk/portal';
import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';

import { CodeEditorEnhancementDirective } from './code-editor-enhancement.directive';
import { PageHeaderContentDirective } from './page-header-content.directive';
import {
  TocContentDirective,
  TocContentsContainerDirective,
  TocLinkDirective,
} from './toc';

const EXPORTABLES = [
  PageHeaderContentDirective,
  TocContentDirective,
  TocContentsContainerDirective,
  TocLinkDirective,
  CodeEditorEnhancementDirective,
];

@NgModule({
  imports: [CommonModule, PortalModule],
  declarations: [...EXPORTABLES],
  exports: [...EXPORTABLES],
})
export class DirectivesModule {}
