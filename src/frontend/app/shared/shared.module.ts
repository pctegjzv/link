import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FlexLayoutModule } from '@angular/flex-layout';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatProgressBarModule } from '@angular/material';
import {
  ButtonModule,
  CardModule,
  CheckboxModule,
  CodeEditorModule,
  DialogModule,
  DropdownModule,
  FormModule,
  IconModule,
  InputModule,
  MessageModule,
  NotificationModule,
  PaginatorModule,
  RadioModule,
  SelectModule,
  SortModule,
  TableModule,
  TabsModule,
  TagModule,
  TooltipModule,
} from 'alauda-ui';

import { TranslateModule } from '../translate';

import { ComponentsModule } from './components';
import { DirectivesModule } from './directives';
import { PipesModule } from './pipes';

const EXPORTABLE_IMPORTS = [
  // Vendor modules:
  CommonModule,
  FormsModule,
  ReactiveFormsModule,
  TranslateModule,

  // Material imports
  FlexLayoutModule,
  MatProgressBarModule,

  // AUI imports
  FormModule,
  IconModule,
  ButtonModule,
  DropdownModule,
  SortModule,
  TableModule,
  CodeEditorModule,
  CardModule,
  TooltipModule,
  DialogModule,
  NotificationModule,
  MessageModule,
  InputModule,
  CheckboxModule,
  RadioModule,
  SelectModule,
  PaginatorModule,
  TabsModule,
  TagModule,

  // App shared modules:
  ComponentsModule,
  PipesModule,
  DirectivesModule,
];

@NgModule({
  imports: [...EXPORTABLE_IMPORTS],
  exports: [...EXPORTABLE_IMPORTS],
  declarations: [],
})
export class SharedModule {}
