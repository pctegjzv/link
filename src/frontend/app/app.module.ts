import { HttpClientModule } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import {
  CodeEditorModule,
  MessageModule,
  MonacoProviderService,
  NotificationModule,
  PaginatorIntl,
  TooltipCopyIntl,
} from 'alauda-ui';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import {
  CustomMonacoProviderService,
  CustomPaginatorIntlService,
  CustomTooltipIntlService,
  ServicesModule,
} from './services';
import { GlobalTranslateModule } from './translate';

const DEFAULT_MONACO_OPTIONS: monaco.editor.IEditorConstructionOptions = {
  fontSize: 12,
  folding: true,
  scrollBeyondLastLine: false,
  minimap: { enabled: false },
  mouseWheelZoom: true,
  scrollbar: {
    vertical: 'visible',
    horizontal: 'visible',
  },
  fixedOverflowWidgets: true,
};

@NgModule({
  declarations: [AppComponent],
  imports: [
    HttpClientModule,
    BrowserModule,
    BrowserAnimationsModule,
    GlobalTranslateModule,
    ServicesModule,

    // AUI NotificationModule, MessageModule need to be imported here since it provides a service
    NotificationModule,
    MessageModule,

    // MonacoProvider must be provided here to overwrite the one defined in CodeEditorModule.
    CodeEditorModule.forRoot({
      baseUrl: 'lib',
      defaultOptions: DEFAULT_MONACO_OPTIONS,
    }),
    // App routing module should stay at the bottom
    AppRoutingModule,
  ],
  providers: [
    {
      provide: MonacoProviderService,
      useClass: CustomMonacoProviderService,
    },
    {
      provide: TooltipCopyIntl,
      useClass: CustomTooltipIntlService,
    },
    {
      provide: PaginatorIntl,
      useClass: CustomPaginatorIntlService,
    },
  ],
  bootstrap: [AppComponent],
})
export class AppModule {}
