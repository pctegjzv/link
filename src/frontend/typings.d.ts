/// <reference path="../../node_modules/monaco-editor/monaco.d.ts" />

/* SystemJS module definition */
declare var module: NodeModule;
interface NodeModule {
  id: string;
  hot: boolean;
}

declare module '*.svg' {
  const content: string;
  export = content;
}

declare let SockJS: any;
